-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2018 at 12:41 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `food_delivery_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_adminlogin`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_adminlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(30) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(120) NOT NULL,
  `currency` varchar(12) NOT NULL,
  `timezone` text NOT NULL,
  `role` varchar(11) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fooddelivery_adminlogin`
--

INSERT INTO `fooddelivery_adminlogin` (`id`, `fullname`, `username`, `password`, `email`, `currency`, `timezone`, `role`, `icon`, `is_delete`) VALUES
(1, 'admin', 'admin', '123', 'admin@gmail.com', 'CNY - Â¥', 'Asia/Kolkata - (GMT+05:30) Kolkata', '1', 'admin_1472278051.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_bookorder`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_bookorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `long` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `total_price` varchar(10) NOT NULL,
  `payment` varchar(52) NOT NULL,
  `created_at` varchar(15) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `accept_date_time` varchar(50) NOT NULL,
  `accept_status` varchar(6) NOT NULL,
  `assign_date_time` varchar(153) NOT NULL,
  `assign_status` varchar(17) NOT NULL,
  `delivered_date_time` varchar(50) NOT NULL,
  `delivery_date_time` varchar(50) NOT NULL,
  `delivery_status` varchar(6) NOT NULL,
  `delivered_status` varchar(6) NOT NULL,
  `is_assigned` varchar(112) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_category`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_category_res`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_category_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=349 ;

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_city`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_delivery_boy`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_delivery_boy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(55) NOT NULL,
  `vehicle_no` varchar(32) NOT NULL,
  `vehicle_type` varchar(25) NOT NULL,
  `lat` int(55) NOT NULL,
  `long` int(55) NOT NULL,
  `attendance` varchar(8) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_food_desc`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_food_desc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `ItemId` int(11) NOT NULL,
  `ItemQty` int(11) NOT NULL,
  `ItemAmt` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_menu`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=157 ;

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_notify_msg`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_notify_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_restaurant`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(500) NOT NULL,
  `open_time` varchar(30) NOT NULL,
  `close_time` varchar(30) NOT NULL,
  `delivery_time` varchar(30) NOT NULL,
  `timestamp` varchar(20) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `lat` varchar(30) NOT NULL,
  `lon` varchar(30) NOT NULL,
  `desc` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `location` varchar(300) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `del_charge` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_res_owner`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_res_owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(120) NOT NULL,
  `res_id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL,
  `timestamp` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_reviews`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `review_text` text NOT NULL,
  `ratting` varchar(10) NOT NULL,
  `created_at` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_server_key`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_server_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `android_key` varchar(255) NOT NULL,
  `ios_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fooddelivery_server_key`
--

INSERT INTO `fooddelivery_server_key` (`id`, `android_key`, `ios_key`) VALUES
(1, 'AAAA1AprDfk:APA91bERxaABQTQT_2P_XNhsPu8_et8v_sE0T_hMb2evODoIBEdqpq_iwKgqYvuvd0GFflTwscNKQxDz0eYZIHSxzFCyHgm1XYr33GHSyRzJgwBhMAg-4XyPJNIQBctb26krewLXlXFG', 'AAAAauNB3eM:APA91bHAix7ZhjCKl8ixdct2gRzQtEw4MIFEVtGh2TP2pRURR0-J1w99_EWjJvobnAu6hTpYzq5kiAi944u034gFMCREeA1WrZuyRzq67ZGFPCfnGu4btHrb9gtPYiyDwRydgwOkUSdK');

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_subcategory`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_submenu`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `price` varchar(10) NOT NULL,
  `desc` text NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=359 ;

-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_tokendata`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_tokendata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_boyid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


-- --------------------------------------------------------

--
-- Table structure for table `fooddelivery_users`
--

CREATE TABLE IF NOT EXISTS `fooddelivery_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(200) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(50) NOT NULL,
  `referal_code` varchar(20) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `login_with` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) DEFAULT NULL,
  `upass` varchar(50) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `uemail` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uname` (`uname`),
  UNIQUE KEY `uemail` (`uemail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
