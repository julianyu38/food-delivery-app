//
//  Review.h
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "HCSStarRatingView.h"
#import "Reachability.h"
#import "UIImageView+RJLoader.h"
#import "UIImageView+WebCache.h"
#import "Login.h"
#import "SCLAlertView.h"

@interface Review : UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITextViewDelegate,UIAlertViewDelegate>
{
    NSMutableData *receivedData, *receivedData2;
    NSURLConnection *connection1, *connection2;
    NSMutableArray *arrReview;
    
    __weak IBOutlet UIView *addReview;
    __weak IBOutlet UITextView *tv_review;
    __weak IBOutlet HCSStarRatingView *postRate;
    __weak IBOutlet UIView *view_loading;
    __weak IBOutlet UILabel *lbl_rating;
    
    NSString *userId;
    NSMutableDictionary *user_details;
    CGPoint touchPoint;
}

@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UILabel *lblYOUREXPERIENCE;
@property (weak, nonatomic) IBOutlet UILabel *lblYOUR_RATTING;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblRatting;
@property (weak, nonatomic) IBOutlet UIButton *btnShowCancel;

@property (strong, nonatomic) IBOutlet UITableView *tbl_review;
@property(nonatomic,retain) NSString *AlreadyLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnAddReview;
//@property (nonatomic, retain) NSMutableDictionary *dicRestDetail;
@property (nonatomic,retain) NSString *lon;
@property (nonatomic,retain) NSString *lat;
@property (strong, nonatomic) IBOutlet UIView *ViewShowReview;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (nonatomic, retain) NSString *restId;

@end
