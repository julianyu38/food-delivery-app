//
//  Review.m
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Review.h"

@interface Review ()
{
    NSString *language;
}

@end

@implementation Review

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    postRate.maximumValue = 5;
    postRate.minimumValue = 0;
    [self set_Language];

    self.tbl_review.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    tv_review.text = NSLocalizedString(@"txtWrite_here", @"");
    [self retriveJSON:[NSString stringWithFormat:@"%@getrestaurant_review.php?res_id=%@",SERVERURL,self.restId] flag:@"1"];
    
    addReview.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [addReview addGestureRecognizer:tap];
    
//    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
//    [_ViewShowReview addGestureRecognizer:tap2];
    
    self.ViewShowReview.hidden = YES;
    [self.btnAddReview.layer setMasksToBounds:YES];
    self.btnAddReview.layer.cornerRadius = self.btnAddReview.frame.size.height / 2;
    self.btnAddReview.backgroundColor = [UIColor darkGrayColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
//    userId = [user_details valueForKey:@"Id"];
}

#pragma mark - SET LANGUAGE

-(void)set_Language
{
    
    _lblReview.text = NSLocalizedString(@"lblReview", @"");
    _lblYOUREXPERIENCE.text = NSLocalizedString(@"lblYOUREXPERIENCE", @"");
    _lblYOUR_RATTING.text = NSLocalizedString(@"lblYOUR_RATTING", @"");
     [_btnCancel setTitle:NSLocalizedString(@"btnCancel", @"") forState:UIControlStateNormal];
     [_btnShowCancel setTitle:NSLocalizedString(@"btnCancel", @"") forState:UIControlStateNormal];
     [_btnSubmit setTitle:NSLocalizedString(@"btnSubmit", @"") forState:UIControlStateNormal];
    _lblRatting.text = NSLocalizedString(@"lblRatting", @"");
    
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        tv_review.textAlignment = UITextAlignmentRight;

    }
    else
    {
        NSLog(@"en");
    }
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    [connection1 cancel];
    connection1 = nil;
    
    [connection2 cancel];
    connection2 = nil;
}

#pragma mark - UIGestureRecognizer Handler Method
-(void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [tv_review resignFirstResponder];
    if (self.view.frame.origin.y != 0)
    {
//        [UIView animateWithDuration:0.3 animations:^{
//            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        }];
    }
}

#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        if([flag isEqualToString:@"2"])
        {
            connection2 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
    }
}

#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData = [[NSMutableData alloc]init];
        [receivedData setLength:0];
    }
    if (connection == connection2)
    {
        receivedData2 = [[NSMutableData alloc]init];
        [receivedData2 setLength:0];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData appendData:data];
    }
    if (connection == connection2)
    {
        [receivedData2 appendData:data];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([json count] > 0)
            {
                if ([[[json objectAtIndex:0]valueForKey:@"status"]isEqualToString:@"Success"]) {
                    arrReview = [[NSMutableArray alloc] init];
                    arrReview = [[json objectAtIndex:0]valueForKey:@"Reviews"];
//
                    [self.tbl_review reloadData];
                }
                
            }
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No reviews for this restaurant" duration:3.0];
                [self.view addSubview:snackbar];

            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    if (connection == connection2)
    {
        if (receivedData2 != nil)
        {
            
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"%@",json);
            
            if([[[json objectAtIndex:0]valueForKey:@"status"]isEqualToString:@"Success"])
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Review Published Successfully. Thank you." duration:3.0];
                [self.view addSubview:snackbar];
                 [self retriveJSON:[NSString stringWithFormat:@"%@getrestaurant_review.php?res_id=%@",SERVERURL,self.restId] flag:@"1"];
                [_tbl_review reloadData];
            }
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:[[json objectAtIndex:0]valueForKey:@"error"] duration:3.0];
                [self.view addSubview:snackbar];
            }

        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    view_loading.hidden = YES;
}

#pragma mark - TextView Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    if (self.view.frame.origin.y == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
//            self.view.frame = CGRectMake(0, self.view.frame.origin.y - (addReview.frame.origin.y + textView.frame.origin.y), self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        if (self.view.frame.origin.y != 0)
        {
//            [UIView animateWithDuration:0.3 animations:^{
//                self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//            }];
        }
    }
    return YES;
}

#pragma mark - Button Actions
//Back
- (IBAction)btn_back_click:(UIButton *)sender
{
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.8f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromBottom;
//    [self.view.window.layer addAnimation:transition forKey:nil];
////    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    if (userId)
    {
        [navigationArray removeObjectAtIndex: navigationArray.count- (1)];  // You can pass your index here
        self.navigationController.viewControllers = navigationArray;
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    Detail *List = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
//    List.restaurantID=_restId;
//    List.lat = _lat;
//    List.lon = _lon;
//    CATransition* transition = [CATransition animation];
//    transition.duration = transitionDuration;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [self.view.window.layer addAnimation:transition forKey:kCATransition];
//    [self.navigationController pushViewController:List animated:YES];
}

//Open Review Dailogue box
-(IBAction)btn_addReview:(UIButton *)sender
{
    CGRect Frame = self.btnAddReview.frame;
    [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.btnAddReview.frame = CGRectMake(self.btnAddReview.frame.origin.x+5, self.btnAddReview.frame.origin.y+5, self.btnAddReview.frame.size.width-10, self.btnAddReview.frame.size.height-10);
    }completion:^(BOOL finished){
        if (finished)
        {
            [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.btnAddReview.frame = CGRectMake(self.btnAddReview.frame.origin.x-10, self.btnAddReview.frame.origin.y-10, self.btnAddReview.frame.size.width+20, self.btnAddReview.frame.size.height+20);
                self.btnAddReview.alpha = 0.0;
            }completion:^(BOOL finished){
                if (finished)
                {
                    [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.btnAddReview.alpha = 1.0;
                        self.btnAddReview.frame = Frame;
                    }completion:^(BOOL finished) {
                        NSArray *userDetails = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
                        if (userDetails)
                        {
                            addReview.center = sender.center;
                            addReview.hidden = NO;
                            addReview.transform = CGAffineTransformMakeScale(0.01, 0.01);
                            [UIView animateWithDuration:0.3f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                                addReview.transform = CGAffineTransformIdentity;
                                addReview.center = self.view.center;
                            } completion:nil];
                        }
                        else
                        {
                            SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                            [alert addButton:@"YES" actionBlock:^{
                                Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
                                login.Res_ID = _restId;
                                login.page_name=@"Review";
                                CATransition* transition = [CATransition animation];
                                transition.duration = transitionDuration;
                                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                                transition.type = kCATransitionPush;
                                transition.subtype = kCATransitionFromLeft;
                                [self.view.window.layer addAnimation:transition forKey:kCATransition];
                                
                                //            [self presentViewController:login animated:YES completion:nil];
                                [self.navigationController pushViewController:login animated:YES];
                            }];
                            [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:@"Warning" subTitle:@"To add review you must login first. Do you want to login now?" closeButtonTitle:@"NO" duration:0.0];
                        }
                    }];
                }
            }];
        }
    }];
}

//Review Dialogue box Button Action
//Submit button
-(IBAction)btn_postReview:(id)sender
{
    [tv_review resignFirstResponder];
    if (tv_review.text.length == 0 || [tv_review.text isEqualToString:NSLocalizedString(@"txtWrite_here", @"")] || postRate.value == 0)
    {
        [self showAlertView:@"Info" message:@"Please write review and give rate" cancelButtonTitle:@"OK"];
        return;
    }
    else
    {
        [self retriveJSON:[NSString stringWithFormat:@"%@postrestaurant_review.php?user_id=%@&res_id=%@&review_text=%@&ratting=%0.0f",SERVERURL,userId,self.restId,tv_review.text,postRate.value] flag:@"2"];
//        [self retriveJSON:[NSString stringWithFormat:@"%@postrestaurant_review.php?user_id=%@&res_id=%@&review_text=%@&ratting=%0.1f",SERVERURL,userId,self.restId,tv_review.text,postRate.value] flag:@"2"];
        
    }
    addReview.hidden = YES;
    tv_review.text = NSLocalizedString(@"txtWrite_here", @"");
    postRate.value = 0;
}

//Show Review Cancel Button
-(IBAction)btn_cancel:(id)sender
{
    self.ViewShowReview.hidden = NO;
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.ViewShowReview.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.ViewShowReview.center = touchPoint;
    } completion:^(BOOL finished)
     {
         if (finished)
         {
             self.ViewShowReview.hidden = YES;
         }
     }];
}

//Add Review View Cancel Button
-(IBAction)btn_reviewCancel:(id)sender
{
    [tv_review resignFirstResponder];
//    addReview.hidden = YES;
    tv_review.text = NSLocalizedString(@"txtWrite_here", @"");
    lbl_rating.text = @"0.0";
    postRate.value = 0;
    if (self.view.frame.origin.y != 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        addReview.transform = CGAffineTransformMakeScale(0.01, 0.01);
        addReview.center = self.btnAddReview.center;
    } completion:^(BOOL finished){
        addReview.hidden = YES;
    }];
}

#pragma mark - HCSStarRatingView Action
- (IBAction)ratingChanged:(HCSStarRatingView *)sender
{
    if (self.view.frame.origin.y == 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
    if (sender.value >= 0 && sender.value <=5)
    {
        lbl_rating.text = [NSString stringWithFormat:@"%.1f",sender.value];
    }
}

#pragma mark - Tableview Datasource and Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrReview count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *dic = [arrReview objectAtIndex:indexPath.row];
    
    UIImageView *userImage = (UIImageView *)[cell.contentView viewWithTag:103];
    [userImage.layer setMasksToBounds:YES];
    userImage.layer.cornerRadius = userImage.frame.size.width / 2;
    userImage.clipsToBounds = YES;
    NSString *userUsl = [dic valueForKey:@"image"];
    
    NSString *LoginType = [dic valueForKey:@"login_with"];
    NSString *userImage1;
    if ([LoginType isEqualToString:@"appuser"]) {
        userImage1 = [NSString stringWithFormat:@"%@%@",IMAGEURL,userUsl];
    }
    else if ([LoginType isEqualToString:@"Facebook"])
    {
        userImage1 = [NSString stringWithFormat:@"%@",userUsl];
    }
    else if([LoginType isEqualToString:@"Google"])
    {
        userImage1 = [NSString stringWithFormat:@"%@",userUsl];
    }
    userImage1 = [userImage1 stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [userImage sd_setImageWithURL:[NSURL URLWithString:userImage1] placeholderImage:nil options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
        [userImage updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [userImage reveal];
    }];
    
    UIImageView *CellImage = (UIImageView *)[cell.contentView viewWithTag:785];
    if ([language isEqualToString:country])
    {
        NSLog(@"ar");
        CellImage.image=[CellImage.image imageFlippedForRightToLeftLayoutDirection];
    }
    else
    {
        NSLog(@"en");
    }
    UILabel *user = (UILabel *)[cell.contentView viewWithTag:101];
    user.text = [NSString stringWithFormat:@"%@ :",[dic valueForKey:@"username"]];
    [user sizeToFit];
//    [self calculateWidthOfUILable:user];

    UILabel *reviewText = (UILabel *)[cell.contentView viewWithTag:102];
    reviewText.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"review_text"]];
    [reviewText sizeToFit];
//    [self calculateHeightOfUILable:reviewText];
    
    UILabel *reviewRate = (UILabel *)[cell.contentView viewWithTag:104];
    reviewRate.text = [NSString stringWithFormat:@"%.1f/5.0", [[dic valueForKey:@"ratting"] floatValue]];
    reviewRate.frame = CGRectMake(user.frame.origin.x + user.frame.size.width, reviewRate.frame.origin.y, reviewRate.frame.size.width, reviewRate.frame.size.height);
    CGPoint centerPoint = user.center;
    centerPoint.x = reviewRate.center.x;
    centerPoint.y = user.center.y;
    reviewRate.center = centerPoint;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.5f];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    [animation setType:@"rippleEffect"];
    [cell.layer addAnimation:animation forKey:nil];
    
    NSMutableDictionary *dic = [arrReview objectAtIndex:indexPath.row];
    UILabel *username = (UILabel *)[self.ViewShowReview viewWithTag:11];
    username.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"username"]];
    
    HCSStarRatingView *rate = (HCSStarRatingView *)[self.ViewShowReview viewWithTag:12];
    rate.value = [[dic valueForKey:@"ratting"] floatValue];
    
    UILabel *rateLabel = (UILabel *)[self.ViewShowReview viewWithTag:13];
    rateLabel.text = [NSString stringWithFormat:@"%.1f/5", [[dic valueForKey:@"ratting"] floatValue]];
    
    UITextView *reviewText = (UITextView *)[self.ViewShowReview viewWithTag:14];
    reviewText.editable = NO;
    reviewText.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"review_text"]];
    
    [self performSelector:@selector(showReview:) withObject:cell afterDelay:0.5f];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Show Review Animated
-(void)showReview:(UITableViewCell *)sender
{
    touchPoint = [sender convertPoint:CGPointZero toView:self.view];
    touchPoint.x = self.tbl_review.center.x;
    touchPoint.y = touchPoint.y + sender.frame.size.height /2;
    
    self.ViewShowReview.center = touchPoint;
    self.ViewShowReview.hidden = NO;
    self.ViewShowReview.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.ViewShowReview.transform = CGAffineTransformIdentity;
        self.ViewShowReview.center = self.view.center;
    } completion:nil];
}
#pragma mark - Calculate Frame
//Calculate UILable Height
-(void)calculateHeightOfUILable:(UILabel *)lbl
{
    CGSize maximumLabelSize = CGSizeMake(lbl.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize:maximumLabelSize lineBreakMode:lbl.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.height = expectedLabelSize.height + 5;
    lbl.frame = newFrame;
}
//Calculate UILable width. Returns
-(void)calculateWidthOfUILable:(UILabel *)lbl
{
    CGSize maximumLabelSize = CGSizeMake(lbl.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize:maximumLabelSize lineBreakMode:lbl.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.width = expectedLabelSize.width + 3;
    lbl.frame = newFrame;
}

@end
