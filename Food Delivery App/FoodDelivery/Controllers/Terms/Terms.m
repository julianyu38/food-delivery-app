//
//  Terms.m
//  FoodDelivery
//
//  Created by Redixbit on 24/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Terms.h"
#import "Constants.h"
#import "List.h"
@interface Terms ()
{
    NSString *language;
}
@end

@implementation Terms

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self set_Language];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"tnc" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
}

- (IBAction)btn_back:(UIButton *)sender
{
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblTitle.text = NSLocalizedString(@"lblTerm", @"");

    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        
    }
    else
    {
        NSLog(@"en");
    }
    
    
}

@end
