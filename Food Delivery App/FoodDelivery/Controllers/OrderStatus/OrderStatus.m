//
//  OrderStatus.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 23/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "OrderStatus.h"
#import "Constants.h"
#import "LanguageManager.h"
#import "Locale.h"
#import "List.h"
#import "ViewOrder.h"
@interface OrderStatus ()
{
    NSString *strOderPlaced,*strOderVarified,*strOderProcessing,*strOderOutOfDelivery,*strOderDelivery;
    NSMutableArray *arrOderStatus;
    NSString *language;
  
    
}

@end

@implementation OrderStatus

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _view_loading.hidden=YES;
    [self setFont];
    [self set_Language];

    
     [self retriveJSON:[NSString stringWithFormat:@"%@order_details.php?order_id=%@",SERVERURL,_orderId] flag:@"1"];
  
    
}

-(void) setFont
{
    _lblOderNo.font = [UIFont fontWithName:RFontRegular size:_lblOderNo.font.pointSize];
    
    _lblRestName.font=[UIFont fontWithName:RFontSemibold size:_lblRestName.font.pointSize];
    _lblRestAddress.font=[UIFont fontWithName:RFontSemibold size:_lblRestAddress.font.pointSize];
    _lblRestMobile.font = [UIFont fontWithName:RFontSemibold size:_lblRestMobile.font.pointSize];
    
    _lblOderNoTitle.font = [UIFont fontWithName:RFontRegular size:_lblOderNoTitle.font.pointSize];
    _lblOderAmount.font = [UIFont fontWithName:RFontRegular size:_lblOderAmount.font.pointSize];
    _lblDeliveryTime.font = [UIFont fontWithName:RFontRegular size:_lblDeliveryTime.font.pointSize];
    _lblOderTime.font = [UIFont fontWithName:RFontRegular size:_lblOderTime.font.pointSize];
    
    _lblOderNoTitleDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderNoTitleDesc.font.pointSize];
    _lblOderAmountDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderAmountDesc.font.pointSize];
    _lblDeliveryTimeDesc.font = [UIFont fontWithName:RFontRegular size:_lblDeliveryTimeDesc.font.pointSize];
    _lblOderTimeDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderTimeDesc.font.pointSize];
    
   
    _lblOderPlaced.font = [UIFont fontWithName:RFontRegular size:_lblOderPlaced.font.pointSize];
    _lblOderVarified.font = [UIFont fontWithName:RFontRegular size:_lblOderVarified.font.pointSize];
    _lblOderProcess.font = [UIFont fontWithName:RFontRegular size:_lblOderProcess.font.pointSize];
    _lblOderOutOfDelivery.font = [UIFont fontWithName:RFontRegular size:_lblOderOutOfDelivery.font.pointSize];
    _lblOderDelivery.font = [UIFont fontWithName:RFontRegular size:_lblOderDelivery.font.pointSize];
    
    _lblOderPlacedDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderPlacedDesc.font.pointSize];
    _lblOderVarifiedDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderVarifiedDesc.font.pointSize];
    _lblOderProcessDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderProcessDesc.font.pointSize];
    _lblOderOutOfDeliveryDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderOutOfDeliveryDesc.font.pointSize];
    _lblOderDeliveryDesc.font = [UIFont fontWithName:RFontRegular size:_lblOderDeliveryDesc.font.pointSize];
    
    

}
-(void)setText
{
    
    _lblOderNo.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"_lblOrderNo", @""),[arrOderStatus valueForKey:@"order_id"]];
    
    _imgRestImage.layer.cornerRadius = 5.0f;
    _imgRestImage.clipsToBounds = YES;
    NSString *ImgPath = [NSString stringWithFormat:@"%@%@",IMAGEURL,[arrOderStatus valueForKey:@"restaurant_image"]];
    [_imgRestImage sd_setImageWithURL:[NSURL URLWithString:ImgPath] placeholderImage:nil options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
        [_imgRestImage updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_imgRestImage reveal];
    }];
    
    _lblRestName.text=[arrOderStatus valueForKey:@"restaurant_name"];
    _lblRestAddress.text =[arrOderStatus valueForKey:@"restaurant_address"];
    _lblRestMobile.text=[arrOderStatus valueForKey:@"restaurant_contact"];
    _lblOderNoTitleDesc.text=[arrOderStatus valueForKey:@"order_id"];
    _lblOderAmountDesc.text=[NSString stringWithFormat:@"$%@",[arrOderStatus valueForKey:@"order_amount"]];
    _lblDeliveryTimeDesc.text=[arrOderStatus valueForKey:@"delivery_time"];
    _lblOderTimeDesc.text=[arrOderStatus valueForKey:@"order_time"];
    
    strOderPlaced=[NSString stringWithFormat:@"%@",@"Activate"];
    strOderVarified=[NSString stringWithFormat:@"%@",[arrOderStatus valueForKey:@"order_verified"]];
    strOderProcessing=[NSString stringWithFormat:@"%@",[arrOderStatus valueForKey:@"order_verified"]];
    strOderOutOfDelivery=[NSString stringWithFormat:@"%@",[arrOderStatus valueForKey:@"delivery_status"]];
    strOderDelivery=[NSString stringWithFormat:@"%@",[arrOderStatus valueForKey:@"delivered_status"]];
    
    //Your Oder has been placed.
    if ([strOderPlaced isEqualToString:@"Deactivate"]) {
        _lblOderPlaced.textColor =Color_light_grey;
        _imgRound1.image=[UIImage imageNamed:@"btn_unfill.png"];
        
    }
    else
    {
        _lblOderPlaced.textColor =[UIColor blackColor];
        _imgRound1.image=[UIImage imageNamed:@"btn_fill.png"];
        _lblOderPlacedDesc.text = [arrOderStatus valueForKey:@"order_time"];
    }
    
    //Oder Has been varified by Request.
    if ([strOderVarified isEqualToString:@"Deactivate"]) {
        _lblOderVarified.textColor =Color_light_grey;
        _imgRound2.image=[UIImage imageNamed:@"btn_unfill.png"];
    }
    else
    {
        _lblOderVarified.textColor =[UIColor blackColor];
        _imgRound2.image=[UIImage imageNamed:@"btn_fill.png"];
        _lblOderVarifiedDesc.text = [arrOderStatus valueForKey:@"order_verified_date"];
    }
    
    //Oder is processing
    if ([strOderProcessing isEqualToString:@"Deactivate"]) {
        _lblOderProcess.textColor =Color_light_grey;
        _imgRound3.image=[UIImage imageNamed:@"btn_unfill.png"];
    }
    else
    {
        _lblOderProcess.textColor =[UIColor blackColor];
        _imgRound3.image=[UIImage imageNamed:@"btn_fill.png"];
        _lblOderProcessDesc.text=[arrOderStatus valueForKey:@"order_verified_date"];
    }
    
    //Oder is out for delivery
    if ([strOderOutOfDelivery isEqualToString:@"Deactivate"]) {
        _lblOderOutOfDelivery.textColor =Color_light_grey;
        _imgRound4.image=[UIImage imageNamed:@"btn_unfill.png"];
    }
    else
    {
        _lblOderOutOfDelivery.textColor =[UIColor blackColor];
        _imgRound4.image=[UIImage imageNamed:@"btn_fill.png"];
        _lblOderOutOfDeliveryDesc.text = [arrOderStatus valueForKey:@"delivery_date_time"];
    }
    
    //Oder is Delivered
    if ([strOderDelivery isEqualToString:@"Deactivate"]) {
        _lblOderDelivery.textColor =Color_light_grey;
        _imgRound5.image=[UIImage imageNamed:@"btn_unfill.png"];
         _viewOrderComplete.hidden=NO;
    }
    else
    {
        _lblOderDelivery.textColor =[UIColor blackColor];
        _imgRound5.image=[UIImage imageNamed:@"btn_fill.png"];
        _lblOderDeliveryDesc.text =[arrOderStatus valueForKey:@"delivered_date_time"];
          _viewOrderComplete.hidden=YES;
    }
  
    
}
#pragma mark - SET LANGUAGE
-(void)set_Language
{
    
    _lblHeaderTitle.text = NSLocalizedString(@"_lblHeaderTitle", @"");
    
    _lblOderNoTitle.text = NSLocalizedString(@"_lblOderNoTitle", @"");
    _lblOderAmount.text = NSLocalizedString(@"_lblOderAmount", @"");
    _lblDeliveryTime.text = NSLocalizedString(@"_lblDeliveryTime", @"");
    _lblOderTime.text = NSLocalizedString(@"_lblOderTime", @"");
    
    _lblOderPlaced.text = NSLocalizedString(@"_lblOderPlaced", @"");
    _lblOderVarified.text = NSLocalizedString(@"_lblOderVarified", @"");
    _lblOderProcess.text = NSLocalizedString(@"_lblOderProcess", @"");
    _lblOderOutOfDelivery.text = NSLocalizedString(@"_lblOderOutOfDelivery", @"");
    _lblOderDelivery.text = NSLocalizedString(@"_lblOderDelivery", @"");
    
     [_btnComplete setTitle:NSLocalizedString(@"btnComplete", @"") forState:UIControlStateNormal];
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        ;
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _imgNext.image=[_imgNext.image imageFlippedForRightToLeftLayoutDirection];
         _lblOderNoTitleDesc.textAlignment = UITextAlignmentLeft;
        _lblOderAmountDesc.textAlignment = UITextAlignmentLeft;
        _lblDeliveryTimeDesc.textAlignment = UITextAlignmentLeft;
        _lblOderTimeDesc.textAlignment = UITextAlignmentLeft;
        
        
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didClickViewOrderButton:(id)sender {
    
    ViewOrder *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewOrder"];
    next.orderId = [arrOderStatus valueForKey:@"order_id"];
    [self.navigationController pushViewController:next animated:YES];
}
- (IBAction)didClickBackButton:(id)sender {
    if ([_strScreen isEqualToString:@"myorder"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
        }
    }
    
}
- (IBAction)didClickCompleteOrderButton:(id)sender {
    if ([strOderOutOfDelivery isEqualToString:@"Deactivate"]) {
        
    }
    else
    {
        NSMutableDictionary *dicStatus = [[NSMutableDictionary alloc] init];
        [dicStatus setValue:_orderId forKey:@"order_id"];
        
        
        NSString *url =[NSString stringWithFormat:@"%@order_complete.php",SERVERURL];
        [self POST_Order:dicStatus webserviceName:url flag:@"2"];
    }
   
}
#pragma mark - POST Methods
-(void)POST_Order:(NSMutableDictionary *)parameters webserviceName:(NSString *)serviceName flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",serviceName]];
        
        NSLog(@"URL : %@",serviceName);
        
        for (__strong NSString *string in parameters.allValues)
        {
            string = [string stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        }
        
        NSLog(@"parameters :: %@",parameters);
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPShouldHandleCookies:NO];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        for (NSString *param in parameters)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //Close off the request with the boundary
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        [request setHTTPBody:body];
        [request setTimeoutInterval:30.0];
        
        
        if ([flag isEqualToString:@"2"])
        {
            _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_connection2 start];
        }
    }
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}
#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData = [[NSMutableData alloc]init];
        [receivedData setLength:0];
    }
    else if (connection == _connection2)
    {
        _receivedData2 = [NSMutableData new];
        [_receivedData2 setLength:0];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData appendData:data];
    }
    else
    {
        [_receivedData2 appendData:data];
    }
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                arrOderStatus = [[NSMutableArray alloc] init];
                arrOderStatus = [[json valueForKey:@"order_details"] objectAtIndex:0];
              [self setText];
               
            }
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No order details" duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    else if (connection == _connection2)
    {
        if (_receivedData2 != nil)
        {
            
            NSError *error;
            NSMutableDictionary *dictData = [NSJSONSerialization JSONObjectWithData:_receivedData2 options:NSJSONReadingMutableContainers error:&error];
            if ([[dictData valueForKey:@"success"]isEqualToString:@"1"]) {
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                     [self retriveJSON:[NSString stringWithFormat:@"%@order_details.php?order_id=%@",SERVERURL,_orderId] flag:@"1"];
                    _viewOrderComplete.hidden =YES;
                }];
                NSLog(@"Response1212 : %@",dictData);
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:nil subTitle:@"Your assigned order is completed." closeButtonTitle:nil duration:0.0];
            }
            
            
        }
    }
   
    _view_loading.hidden = YES;
}
@end
