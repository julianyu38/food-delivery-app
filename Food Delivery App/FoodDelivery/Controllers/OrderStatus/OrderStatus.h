//
//  OrderStatus.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 23/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
#import "Snackbar.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+RJLoader.h"

@interface OrderStatus : UIViewController
{
    NSMutableData *receivedData;
    NSURLConnection *connection1;
    
}
@property (retain, nonatomic) NSURLConnection *connection2;
@property (retain, nonatomic) NSMutableData *receivedData2;
@property (weak, nonatomic) IBOutlet UIView *viewOrderComplete;

//UIButton
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnComplete;

//UILabel
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOderNo;

//UIImageView
@property (weak, nonatomic) IBOutlet UIImageView *imgNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgRestImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgRound1;
@property (weak, nonatomic) IBOutlet UIImageView *imgRound2;
@property (weak, nonatomic) IBOutlet UIImageView *imgRound3;
@property (weak, nonatomic) IBOutlet UIImageView *imgRound4;
@property (weak, nonatomic) IBOutlet UIImageView *imgRound5;


//UIView
@property (weak, nonatomic) IBOutlet UIView *ViewOderDetail;
@property (weak, nonatomic) IBOutlet UIView *view_loading;

//UILabel
@property (weak, nonatomic) IBOutlet UILabel *lblRestName;
@property (weak, nonatomic) IBOutlet UILabel *lblRestAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblRestMobile;

@property (weak, nonatomic) IBOutlet UILabel *lblOderNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOderAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTime;
@property (weak, nonatomic) IBOutlet UILabel *lblOderTime;

@property (weak, nonatomic) IBOutlet UILabel *lblOderNoTitleDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderAmountDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTimeDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderTimeDesc;

@property (weak, nonatomic) IBOutlet UILabel *lblOderPlaced;
@property (weak, nonatomic) IBOutlet UILabel *lblOderVarified;
@property (weak, nonatomic) IBOutlet UILabel *lblOderProcess;
@property (weak, nonatomic) IBOutlet UILabel *lblOderOutOfDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lblOderDelivery;

@property (weak, nonatomic) IBOutlet UILabel *lblOderPlacedDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderVarifiedDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderProcessDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderOutOfDeliveryDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOderDeliveryDesc;


@property (nonatomic, retain) NSString *orderId;
@property (nonatomic, retain) NSString *strScreen;



@end
