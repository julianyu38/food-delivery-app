//
//  Menu.m
//  FoodDelivery
//
//  Created by Redixbit on 31/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Menu.h"

@interface Menu ()
{
    NSString *language;
}
@end

@implementation Menu

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    viewLoading.hidden = NO;
    [self TotalQTY];
    index = 0;
    isShare = NO;
    arrButtons = [[NSMutableArray alloc]init];
    viewCart.alpha = 0.0;
    btnQty.alpha = 0.0;

    UILabel *title = (UILabel *)[self.view viewWithTag:1000];
    title.text = self.restName;
    tbl_menu.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self set_Language];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [viewContent addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideToRightWithGestureRecognizer:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [viewContent addGestureRecognizer:swipeRight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@restaurant_menu.php?res_id=%@",SERVERURL,self.restID];
//    NSString *strUrl = [NSString stringWithFormat:@"%@restaurant_menu.php?res_id=%@",SERVERURL,self.restID];
    [self retriveJSON:strUrl Flag:@"1"];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    
    [self.connection1 cancel];
    self.connection1 = nil;
    
    [self.connection2 cancel];
    self.connection2 = nil;
}

#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblTitle.text = NSLocalizedString(@"restMenuTitle", @"");
    [_btnCheckOut setTitle:NSLocalizedString(@"btnCheckout", @"") forState:UIControlStateNormal];
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
                _imgBAck.image=[_imgBAck.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [btn_cart setImage:[btn_cart.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        for (UIView *view in menuScrollView.subviews)
        {
            if ([view isKindOfClass:[UIView class]])
            {
                view.transform =  CGAffineTransformMakeRotation(M_PI);
            }
        }
//        menuScrollView.transform = CGAffineTransformMakeRotation(M_2_PI);
//        subView.transform =  CGAffineTransformMakeRotation(M_PI);
    }
    else
    {
        NSLog(@"en");
    }
    
    
}

#pragma mark - UIGestureRecognizer Handler Method
-(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    if (index == 0)
    {
        return;
    }
    index = index - 1;
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = index;
    [self buttonPressed:[arrButtons objectAtIndex:index]];
}

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    if (index == [arrMenuCat count]-1)
    {
        return;
    }
    index = index + 1;
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = index;
    [self buttonPressed:[arrButtons objectAtIndex:index]];
}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:message duration:3.0];
    [self.view addSubview:snackbar];
//    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
//    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        if (iPhoneVersion==5) {
            menuFontSize = 12.0f;
        }
        else if(iPhoneVersion == 10)
        {
            menuFontSize = 14.0f;
        }
        else
        {
            menuFontSize = 25.0f;
        }
        NSString *tagTitle = [[buttonArray objectAtIndex:i]valueForKey: @"name"];
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(cWidth, 0.0f, buttonWidth, buttonHeight);
        [button setTitle:tagTitle forState:UIControlStateNormal];
//        button.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        
        button.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:menuFontSize];
        [button layoutIfNeeded];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        
        [menuScrollView addSubview:button];
        [arrButtons addObject:button];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 3, button.frame.size.width, 3)];
        bottomView.backgroundColor = [UIColor whiteColor];
//        bottomView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"select_menu_borderr.png"]];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        
        UIView *sideView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 0.5, button.frame.size.height - 20)];
        sideView.backgroundColor = [UIColor whiteColor];
//        sideView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"border.png"]];
        [button addSubview:sideView];
        if (i == 0)
        {
            button.selected = YES;
            [bottomView setHidden:NO];
        }
        else
        {
            [bottomView setHidden:YES];
        }
        cWidth += buttonWidth;
    }
    menuScrollView.contentSize = CGSizeMake(cWidth, menuScrollView.frame.size.height);
    menuScrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"menubar_bg.png"]];
  
}

#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    for (UIView *subView in menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        if (btn.tag == senderbtn.tag)
        {
            index = btn.tag;
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else
        {
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    float xx = senderbtn.frame.origin.x;
    NSLog(@"Index %d",index);
    [menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, menuScrollView.frame.size.height) animated:YES];
      [self retriveJSON:[NSString stringWithFormat:@"%@restaurant_submenu.php?menucategory_id=%@",SERVERURL,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"] valueForKey:@"id"]objectAtIndex:index]] Flag:@"2"];
    
    [tbl_menu reloadData];
}

#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    if (iPhoneVersion == 5)
    {
        menuSize = 12.0f;
    }
    else if (iPhoneVersion ==10)
    {
        menuSize=15.0f;
    }
    else
    {
        menuSize = 25.0f;
    }
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:menuFontSize]};
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}

#pragma mark - Retrive JSON from Server
-(void)retriveJSON:(NSString *)url Flag:(NSString *)flag
{
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        viewLoading.hidden = NO;
        NSString *strUrl = url;
        NSLog(@"%@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [self.connection1 start];
        }
        else if ([flag isEqualToString:@"2"])
        {
            self.connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [self.connection2 start];
        }
    }
}

#pragma mark - NSURLConnectionDelegate Methods
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    if(connection==self.connection1)
    {
        _receivedData1 = [NSMutableData new];
        [self.receivedData1 setLength:0];
    }
    else if(connection==self.connection2)
    {
        _receivedData2 = [NSMutableData new];
        [self.receivedData2 setLength:0];
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(connection==self.connection1)
    {
        [self.receivedData1 appendData:data];
    }
    else if(connection==self.connection2)
    {
        [self.receivedData2 appendData:data];
    }
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    viewLoading.hidden = YES;
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(connection==self.connection1)
    {
        if (_receivedData1!= nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"Json :: %@",json);
            arrMenuCat = [[NSMutableArray alloc]init];
            if ([json count] > 0)
            {
                arrMenuCat = json;
                [self addButtonsInScrollMenu:[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]];

                [self retriveJSON:[NSString stringWithFormat:@"%@restaurant_submenu.php?menucategory_id=%@",SERVERURL,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"] valueForKey:@"id"]objectAtIndex:0]] Flag:@"2"];
            }
            else
            {
                NSLog(@"Menu List Not Available");
                [self showAlertView:@"Menu" message:@"Menu not ready. Please visit again after some time" cancelButtonTitle:@"OK"];
            }
        }
        else
        {
            NSLog(@"Data nil");
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
//            [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        }
    }
    else if(connection==self.connection2)
    {
        if (_receivedData2!= nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:_receivedData2 options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"Json :: %@",json);
            if ([json count] > 0)
            {
                arrMenuList = [[NSMutableArray alloc]init];
                arrMenuList = json;
                arrMenuList = [[json objectAtIndex:0]valueForKey:@"Menu_List"];
            }
            else
            {
                arrMenuList = nil;
            }
        }
        else
        {
            NSLog(@"Data nil");
            [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        }
    }
    viewLoading.hidden = YES;
    [tbl_menu reloadData];
}

#pragma mark - UITableView Datasource and Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrMenuList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *dic = [arrMenuList objectAtIndex:indexPath.row];
    
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:101];
    [lblName setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]]];
    
    UILabel *lblDesc = (UILabel *)[cell.contentView viewWithTag:102];
    [lblDesc setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"desc"]]];
    
    UILabel *lblPrice = (UILabel *)[cell.contentView viewWithTag:103];
//    [lblPrice setText:[NSString stringWithFormat:@"%@ %.2f",self.restCurrency,[[[arrMenuList valueForKey:@"price"] objectAtIndex:indexPath.row] floatValue]]];
    [lblPrice setText:[NSString stringWithFormat:@"%@%@",_currency,[dic valueForKey:@"price"]]];
    
    UILabel *lblQty = (UILabel *)[cell.contentView viewWithTag:104];
   
    NSString *querynew =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[dic valueForKey:@"id"]];
    
    NSMutableArray *arr_fav = [self cart:querynew];
    if (arr_fav.count > 0)
    {
        lblQty.text = [NSString stringWithFormat:@"%@",[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"]];
    }
    else
    {
        lblQty.text = @"0";
    }
    UIButton *minus = (UIButton *)[cell.contentView viewWithTag:111];
    UIButton *plus = (UIButton *)[cell.contentView viewWithTag:112];
    plus.tag = indexPath.row;
    minus.tag = indexPath.row;
    
    [plus addTarget:self action:@selector(btn_plus_click:) forControlEvents:UIControlEventTouchUpInside];
    [minus addTarget:self action:@selector(btn_minus_click:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UIScrollView Delegate Method
//Detects scolling of UITableView or scrollview
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == menuScrollView)
    {
        return;
    }
    if ([scrollView.panGestureRecognizer translationInView:scrollView.superview].y > 0)
    {
        // handle dragging to the down
//        NSLog(@"Scrolling Down");
        [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.viewNavigation.frame = CGRectMake(self.viewNavigation.frame.origin.x, 0, self.viewNavigation.frame.size.width, self.viewNavigation.frame.size.height);
            
            menuScrollView.frame = CGRectMake(menuScrollView.frame.origin.x, self.viewNavigation.frame.size.height, menuScrollView.frame.size.width, menuScrollView.frame.size.height);
            
            viewContent.frame = CGRectMake(viewContent.frame.origin.x, self.viewNavigation.frame.size.height +menuScrollView.frame.size.height, viewContent.frame.size.width, self.view.frame.size.height - (menuScrollView.frame.size.height + self.viewNavigation.frame.size.height));
            
        } completion:^(BOOL finished){
        }];
    }
    else
    {
        // handle dragging to the up
//        NSLog(@"Scrolling Up");
        
        [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.viewNavigation.frame = CGRectMake(self.viewNavigation.frame.origin.x, -self.viewNavigation.frame.size.height, self.viewNavigation.frame.size.width, self.viewNavigation.frame.size.height);
            
            menuScrollView.frame = CGRectMake(menuScrollView.frame.origin.x, 0, menuScrollView.frame.size.width, menuScrollView.frame.size.height);
            
            viewContent.frame = CGRectMake(viewContent.frame.origin.x, menuScrollView.frame.size.height, viewContent.frame.size.width, self.view.frame.size.height - menuScrollView.frame.size.height);
            
        } completion:^(BOOL finished){
        }];
    }
    if (!isShare)
    {
        return;
    }
    isShare = YES;
    [self btnCart:[[UIButton alloc]init]];
}

#pragma mark - Add and Remove Quntity
- (IBAction)btn_minus_click:(UIButton *)sender
{
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tbl_menu];
    NSIndexPath *clickedButtonIndexPath = [tbl_menu indexPathForRowAtPoint:touchPoint];
    NSInteger row = clickedButtonIndexPath.row;
    NSInteger section = clickedButtonIndexPath.section;
    int qty;
    
    NSString *query =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row]];
    NSMutableArray *arr_fav = [self cart:query];
    if (arr_fav.count > 0)
    {
        qty = [[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"] integerValue];
        qty = qty - 1;
        if (qty <= 0)
        {
            NSString *passingStr = [NSString stringWithFormat:@"delete from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row]];

            if ([[SQLFile new] operationdb:passingStr])
            {
                NSLog(@"Deleted");
            }
            else
            {
                NSLog(@"Not Deleted");
            }
        }
        else
        {
            NSString *passingStr = [NSString stringWithFormat:@"update CartQTY set foodQty = '%d' where restId = %@ AND menuID = %@ AND foodId = %@",qty,self.restID, [[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row]];
            [self updateDatabase:passingStr];
        }
    }
    else
    {
        return;
    }
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [tbl_menu reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self TotalQTY];
    if (isShare)
    {
        return;
    }
    isShare = NO;
    [self btnCart:[[UIButton alloc]init]];
}

- (IBAction)btn_plus_click:(UIButton *)sender
{
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tbl_menu];
    NSIndexPath *clickedButtonIndexPath = [tbl_menu indexPathForRowAtPoint:touchPoint];
    NSInteger row = clickedButtonIndexPath.row;
    NSInteger section = clickedButtonIndexPath.section;
    int qty;
    
    NSString *query =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row]];
    NSMutableArray *arr_fav = [self cart:query];
    if (arr_fav.count > 0)
    {
        qty = [[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"] integerValue];
        qty = qty + 1;
        NSString *passingStr = [NSString stringWithFormat:@"update CartQTY set foodQty = '%d' where restId = %@ AND menuID = %@ AND foodId = %@",qty,self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row]];
        [self updateDatabase:passingStr];
    }
    else
    {
        qty = 1;
        NSString *passingStr = [NSString stringWithFormat:@"insert into CartQTY values('%@','%@','%@','%@','%.2f','%d','%@','%@')",self.restID,[[[[arrMenuCat objectAtIndex:0]valueForKey:@"Menu_Category"]valueForKey:@"id"]objectAtIndex:index],[[arrMenuList valueForKey:@"id"] objectAtIndex:row],[[arrMenuList valueForKey:@"name"] objectAtIndex:row],[[[arrMenuList valueForKey:@"price"] objectAtIndex:row] floatValue],qty,[[arrMenuList valueForKey:@"desc"] objectAtIndex:row],self.restCurrency];
        [self insertDatabase:passingStr];
    }

    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [tbl_menu reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self TotalQTY];
    if (isShare)
    {
        return;
    }
    isShare = NO;
    [self btnCart:[[UIButton alloc]init]];
}

-(void)TotalQTY
{
    NSString *str = [NSString stringWithFormat:@"select * from CartQTY"];
    NSMutableArray *arrFav = [self cart:str];
    NSNumber *sum = [[arrFav valueForKey:@"foodQty"] valueForKeyPath:@"@sum.self"];
    [btnQty setTitle:[NSString stringWithFormat:@"%@ %@",sum,NSLocalizedString(@"btnItem", @"")] forState:UIControlStateNormal];
}

#pragma mark - Database Methods
//insert - delete
-(void)insertDatabase:(NSString *)string
{
    NSString *passingStr = string;
    SQLFile *sql = [SQLFile new];
    if ([sql operationdb:passingStr])
    {
        NSLog(@"Inserted");
    }
    else
    {
        NSLog(@"Not Inserted");
    }
}
//update
-(void)updateDatabase:(NSString *)string
{
    NSString *passingStr = string;
    SQLFile *sql = [SQLFile new];
    if ([sql updatedb:passingStr])
    {
        NSLog(@"Updated");
    }
    else
    {
        NSLog(@"Not Updated");
    }
}
//select
-(NSMutableArray *)cart:(NSString *)str
{
    SQLFile *new =[[SQLFile alloc]init];
    NSMutableArray *arr_fav = [new select_cart:str];
    return arr_fav;
}

#pragma mark - View button Action
//Back
- (IBAction)btnBack:(UIButton *)sender
{
    NSString *string= @"Delete from CartQTY";
    [self insertDatabase:string];
    [self.navigationController popViewControllerAnimated:YES];
//    if ([language isEqualToString:country]) {
//        NSLog(@"ar");
//        Detail *List = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
//        CATransition* transition = [CATransition animation];
//        transition.duration = transitionDuration;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [self.view.window.layer addAnimation:transition forKey:kCATransition];
//        [self.navigationController pushViewController:List animated:YES];
//
//    }
//    else
//    {
//        NSLog(@"en");
//        Detail *List = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
//        CATransition* transition = [CATransition animation];
//        transition.duration = transitionDuration;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromLeft;
//        [self.view.window.layer addAnimation:transition forKey:kCATransition];
//        [self.navigationController pushViewController:List animated:YES];
//    }
}

//Checkout Button / Go to Cart page
-(IBAction)btn_checkout:(id)sender
{
    Cart *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"Cart"];
    cart.restDelTime = self.restDelTime;
    cart.currency = _currency;
    cart.restName = _restName;
    
    [self.navigationController pushViewController:cart animated:YES];
}

//Cart button
- (IBAction)btnCart:(UIButton *)sender
{
    CGFloat duration = 0.5f;
    if (isShare == NO)
    {
        isShare = YES;
        [CATransaction begin];
        [viewCart.layer removeAllAnimations];
        [btn_cart.layer removeAllAnimations];
        
        viewCart.alpha = 1;
        btnQty.alpha = 1;
        viewCart.hidden = NO;
        
        CAShapeLayer *aCircle = [CAShapeLayer layer];
        CGFloat ratio = btn_cart.bounds.size.width / viewCart.bounds.size.width;
        int circleLayerWidth = viewCart.bounds.size.width;
        UIBezierPath *path;
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            
            path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(-viewCart.bounds.size.width, -(viewCart.bounds.size.width * ratio) / 2 + viewCart.bounds.size.height / 2, circleLayerWidth * ratio, circleLayerWidth * ratio)];
        }
        else
        {
            NSLog(@"en");
             path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(viewCart.bounds.size.width, -(viewCart.bounds.size.width * ratio) / 2 + viewCart.bounds.size.height / 2, circleLayerWidth * ratio, circleLayerWidth * ratio)];
        }
       
        
        aCircle.path = path.CGPath;
        aCircle.fillColor = [UIColor blackColor].CGColor;
        [viewCart.layer setMask:aCircle];
        
        CGRect _rect = CGRectMake(0, -viewCart.bounds.size.width / 2 + viewCart.bounds.size.height / 2, circleLayerWidth, circleLayerWidth);
        
        CABasicAnimation *circleLayerAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
        circleLayerAnimation.toValue = (__bridge id)[UIBezierPath bezierPathWithOvalInRect:_rect].CGPath;
        circleLayerAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        circleLayerAnimation.autoreverses = NO;
        circleLayerAnimation.repeatCount = 1;
        circleLayerAnimation.duration = duration;
        circleLayerAnimation.fillMode = kCAFillModeForwards;
        circleLayerAnimation.removedOnCompletion = NO;
        
        [CATransaction setCompletionBlock:^{
            [viewCart.layer setMask:nil];
        }];
        [aCircle addAnimation:circleLayerAnimation forKey:circleLayerAnimation.keyPath];
        [CATransaction commit];
    }
    else
    {
        // hide view toolbar
        CAShapeLayer *aCircle = [CAShapeLayer layer];
        int circleLayerWidth = viewCart.bounds.size.width;
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, -viewCart.bounds.size.width / 2 + viewCart.bounds.size.height / 2, circleLayerWidth, circleLayerWidth)];
        aCircle.path = path.CGPath;
        aCircle.fillColor = [UIColor blackColor].CGColor;
        [viewCart.layer setMask:aCircle];
        
        [CATransaction begin];
        
        CGFloat ratio = btn_cart.bounds.size.width / viewCart.bounds.size.width;
        
        CGRect _rect = CGRectMake(viewCart.bounds.size.width, -(viewCart.bounds.size.width * ratio) / 2 + viewCart.bounds.size.height / 2, circleLayerWidth * ratio, circleLayerWidth * ratio);
        
        CABasicAnimation *circleLayerAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
        circleLayerAnimation.toValue =
        (__bridge id)[UIBezierPath bezierPathWithOvalInRect:_rect].CGPath;
        circleLayerAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        circleLayerAnimation.autoreverses = NO;
        circleLayerAnimation.repeatCount = 1;
        circleLayerAnimation.duration = duration;
        circleLayerAnimation.removedOnCompletion = false;
        circleLayerAnimation.fillMode = kCAFillModeForwards;
        
        [CATransaction setCompletionBlock:^{
            viewCart.hidden = YES;
            btnQty.alpha = 0;
            [self floatButton];
        }];
        
        [aCircle addAnimation:circleLayerAnimation forKey:circleLayerAnimation.keyPath];
        [CATransaction commit];
        isShare = NO;
    }
}

#pragma mark - Floating animation
//Floating Animation of button
-(void)floatButton
{
    CGRect Frame = btn_cart.frame;
    [UIView animateWithDuration:0.0f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        btn_cart.frame = CGRectMake(btn_cart.frame.origin.x+5, btn_cart.frame.origin.y+5, btn_cart.frame.size.width-10, btn_cart.frame.size.height-10);
        btn_cart.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }completion:^(BOOL finished){
        if (finished)
        {
            [UIView animateWithDuration:0.0f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                btn_cart.frame = CGRectMake(btn_cart.frame.origin.x-10, btn_cart.frame.origin.y-10, btn_cart.frame.size.width+20, btn_cart.frame.size.height+20);
                btn_cart.alpha = 0.0;
            }completion:^(BOOL finished){
                if (finished)
                {
                    [UIView animateWithDuration:0.1f animations:^{
                        
                        btn_cart.alpha = 1.0;
                        btn_cart.frame = Frame;
                    }];
                }
            }];
        }
    }];
}

@end
