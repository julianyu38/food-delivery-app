//
//  TableViewCell.m
//  FoodDelivery
//
//  Created by Redixbit on 31/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
