//
//  Menu.h
//  FoodDelivery
//
//  Created by Redixbit on 31/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "SQLFile.h"
#import "Reachability.h"
#import "Cart.h"
#import "Detail.h"
#import "SCLAlertView.h"

@interface Menu : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    IBOutlet UIView *viewLoading;
    IBOutlet UITableView *tbl_menu;
    IBOutlet UIView *viewContent;
    IBOutlet UIView *viewCart;
    IBOutlet UIButton *btnQty;
    IBOutlet UIScrollView *menuScrollView;
    CGFloat menuSize,menuFontSize;
    //index of menu
    int index;
    BOOL isShare;
    IBOutlet UIButton *btn_cart;
    NSMutableArray *arrMenuCat, *arrMenuList,*arrButtons;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;
@property (weak, nonatomic) IBOutlet UIButton *btnItem;
@property (nonatomic,retain) NSString *restID, *restName, *restCurrency, *restDelTime , *currency;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;

@property (retain, nonatomic) NSURLConnection *connection2;
@property (retain, nonatomic) NSMutableData *receivedData2;

@property (nonatomic) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UIView *viewNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgBAck;


@end
