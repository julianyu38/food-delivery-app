//
//  Cart.h
//  FoodDelivery
//
//  Created by Redixbit on 09/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"

#import "SQLFile.h"
#import "Reachability.h"
#import "SCLAlertView.h"

#import "OrderSucess.h"
#import "Login.h"
#import "UserDetails.h"

@interface Cart : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *viewLoading;
    IBOutlet UITableView *tbl_Cart;
    NSMutableArray *arr_cart,*jsonResult, *arrTotalPrice;
    IBOutlet UILabel *lblTotalQty;
    IBOutlet UILabel *lblTotalPrice;
    NSString *description, *userID;
}
@property (nonatomic,retain) NSString *restID, *restName, *restCurrency;

@property (nonatomic, retain) NSString *restDelTime,*currency;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgCart;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckout;

@end
