//
//  Cart.m
//  FoodDelivery
//
//  Created by Redixbit on 09/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Cart.h"

@interface Cart ()
{
      NSString *language;
}
@end

@implementation Cart

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self set_Language];
    
    viewLoading.hidden = NO;
    tbl_Cart.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    arr_cart = [[NSMutableArray alloc]init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSArray *userDetails = [[NSUserDefaults standardUserDefaults] valueForKey:@"userDetails"];
    userID = [userDetails valueForKey:@"Id"];
    [self retriveData];
}





#pragma mark - SET LANGUAGE

-(void)set_Language
{
    
    _lblTitle.text = NSLocalizedString(@"restCart", @"");
    _lblTitleTotal.text = NSLocalizedString(@"lblTotal", @"");
    [_btnCheckout setTitle:NSLocalizedString(@"btnCheckout", @"") forState:UIControlStateNormal];
    
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
                _imgCart.image=[_imgCart.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
     lblTotalPrice.textAlignment = UITextAlignmentLeft;
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
#pragma mark - Button Action
//Back button
- (IBAction)btn_back_click:(UIButton *)sender
{

    [self.navigationController popViewControllerAnimated:YES];
}

//Checkout Button
-(IBAction)btn_checkout:(id)sender
{
    if ([arr_cart count] > 0)
    {
        UserDetails *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserDetails"];
        user.totalPay = (NSDecimalNumber *)lblTotalPrice.text;
        user.restDelTime = self.restDelTime;
        user.restName = self.restName;
       
        
        [self.navigationController pushViewController:user animated:YES];
//        [self presentViewController:user animated:YES completion:nil];
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Can't Proceed with empty cart." duration:3.0f];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Info" message:@"Can't Proceed with empty cart." cancelButtonTitle:@"OK"];
    }
}

#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - GET METHOD / Retrive Data from webservice
-(void)retriveJSON:(NSString *)url
{
    viewLoading.hidden = NO;

    //Checks internet connection
    if (![self isConnected])
    {
        //handle if Internet connection is not found
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        viewLoading.hidden = YES;
    }
    else
    {
        NSString *urlString = url;
        NSLog(@"URL ::: %@",urlString);
        urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [request setTimeoutInterval:30.0f];
        
        self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [self.connection1 start];
    }
}
#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (_receivedData1 != nil)
    {
        jsonResult = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
        if ([[[jsonResult objectAtIndex:0] valueForKey:@"status"] isEqualToString:@"Failed"])
        {
            [self showAlertView:@"Failed" message:@"Please try after some time" cancelButtonTitle:@"OK"];
        }
        else
        {
            NSString *string= @"Delete from CartQTY";
            [self insertDatabase:string];
            arr_cart = nil;
            [self TotalQTY];
            [tbl_Cart reloadData];
            [self showAlertView:@"Order successfull" message:@"Order successfully booked" cancelButtonTitle:@"OK"];
        }
        viewLoading.hidden = YES;
    }
    else
    {
        NSLog(@"Error :: %@",error);
        Snackbar *snackbar ;
        
        snackbar= [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        viewLoading.hidden = YES;
    }
}

#pragma mark - Retrive Data From Database
-(void)retriveData
{
    NSString *str = [NSString stringWithFormat:@"select * from CartQTY"];
    arr_cart = [self cart:str];
//    NSLog(@"Cart :: %@",arr_cart);
    if (arr_cart.count == 0)
    {
//        [self showAlertView:@"Sorry" message:@"There is no data in cart" cancelButtonTitle:@"OK"];
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"There is no data in cart" duration:3.0];
        [self.view addSubview:snackbar];
    }
    [self TotalQTY];
    [tbl_Cart reloadData];
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:0.3];
}

-(void)hideLoading
{
    viewLoading.hidden = YES;
}

-(NSMutableArray *)cart:(NSString *)str
{
    SQLFile *new =[[SQLFile alloc]init];
    NSMutableArray *arr_fav = [new select_cart:str];
    return arr_fav;
}

#pragma mark - Tableview Datasource and Delegate method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (arr_cart == nil)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_cart.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:101];
    [lblName setText:[NSString stringWithFormat:@"%@",[[arr_cart valueForKey:@"foodName"] objectAtIndex:indexPath.row]]];

    UILabel *lblDesc = (UILabel *)[cell.contentView viewWithTag:102];
    [lblDesc setText:[NSString stringWithFormat:@"%@",[[arr_cart valueForKey:@"foodDesc"] objectAtIndex:indexPath.row]]];

    UILabel *lblPrice = (UILabel *)[cell.contentView viewWithTag:103];
    [lblPrice setText:[NSString stringWithFormat:@"%@ %0.2f",[[arr_cart valueForKey:@"foodCurrency"] objectAtIndex:indexPath.row],[[[arr_cart valueForKey:@"foodPrice"] objectAtIndex:indexPath.row] floatValue]]];
    
    UILabel *lblQty = (UILabel *)[cell.contentView viewWithTag:104];
    NSString *querynew =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",[[arr_cart valueForKey:@"restId"] objectAtIndex:indexPath.row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:indexPath.row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:indexPath.row]];
    
    NSMutableArray *arr_fav = [self cart:querynew];
    lblQty.text = [NSString stringWithFormat:@"%@",[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"]];
    
    UIButton *minus = (UIButton *)[cell.contentView viewWithTag:111];
    UIButton *plus = (UIButton *)[cell.contentView viewWithTag:112];
    
    plus.tag = indexPath.row;
    minus.tag = indexPath.row;
    UILabel *lblQtyT = (UILabel *)[cell.contentView viewWithTag:98];
    UILabel *lblItemPrice = (UILabel *)[cell.contentView viewWithTag:89];
    lblQtyT.text = NSLocalizedString(@"lblQty", @"");
    lblItemPrice.text = NSLocalizedString(@"lblItemPrice", @"");
    
    [plus addTarget:self action:@selector(btn_plus_click:) forControlEvents:UIControlEventTouchUpInside];
    [minus addTarget:self action:@selector(btn_minus_click:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Edit Quantity
- (IBAction)btn_minus_click:(UIButton *)sender
{
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tbl_Cart];
    NSIndexPath *clickedButtonIndexPath = [tbl_Cart indexPathForRowAtPoint:touchPoint];
    NSInteger row = clickedButtonIndexPath.row;
    NSInteger section = clickedButtonIndexPath.section;
    int qty;
    
    NSString *query =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",[[arr_cart valueForKey:@"restId"] objectAtIndex:row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:row]];
    NSMutableArray *arr_fav = [self cart:query];
    if (arr_fav.count > 0)
    {
        qty = [[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"] integerValue];
        
        qty = qty - 1;
        if (qty <= 0)
        {
            NSString *query =[NSString stringWithFormat:@"Delete from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",[[arr_cart valueForKey:@"restId"] objectAtIndex:row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:row]];
            if ([[SQLFile new] operationdb:query])
            {
                NSLog(@"Deleted");
                [self retriveData];
            }
            else
            {
                NSLog(@"Not Deleted");
            }
            return;
        }
        else
        {
            NSString *passingStr = [NSString stringWithFormat:@"update CartQTY set foodQty = '%d' where restId = %@ AND menuID = %@ AND foodId = %@",qty,[[arr_cart valueForKey:@"restId"] objectAtIndex:row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:row]];
            [self updateDatabase:passingStr];
        }
    }
    else
    {
        qty = 0;
    }

    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [tbl_Cart reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self TotalQTY];
}

- (IBAction)btn_plus_click:(UIButton *)sender
{
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tbl_Cart];
    NSIndexPath *clickedButtonIndexPath = [tbl_Cart indexPathForRowAtPoint:touchPoint];
    NSInteger row = clickedButtonIndexPath.row;
    NSInteger section = clickedButtonIndexPath.section;
    int qty;
    
    NSString *query =[NSString stringWithFormat:@"select * from CartQTY where restId = %@ AND menuID = %@ AND foodId = %@",[[arr_cart valueForKey:@"restId"] objectAtIndex:row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:row]];
    NSMutableArray *arr_fav = [self cart:query];
    if (arr_fav.count > 0)
    {
        qty = [[[arr_fav objectAtIndex:0] valueForKey:@"foodQty"] integerValue];
        qty = qty + 1;
        
        NSString *passingStr = [NSString stringWithFormat:@"update CartQTY set foodQty = '%d' where restId = %@ AND menuID = %@ AND foodId = %@",qty,[[arr_cart valueForKey:@"restId"] objectAtIndex:row],[[arr_cart valueForKey:@"menuId"] objectAtIndex:row],[[arr_cart valueForKey:@"foodId"] objectAtIndex:row]];
        [self updateDatabase:passingStr];
    }
    else
    {
        qty = 0;
    }

    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [tbl_Cart reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self TotalQTY];
}

-(void)TotalQTY
{
    NSString *str = [NSString stringWithFormat:@"select * from CartQTY"];
    NSMutableArray *arrFav = [self cart:str];
    NSNumber *sum = [[arrFav valueForKey:@"foodQty"] valueForKeyPath:@"@sum.self"];
    [lblTotalQty setText:[NSString stringWithFormat:@"%@",sum]];
    arrTotalPrice = [[NSMutableArray alloc]init];
    for (int i = 0; i<arrFav.count; i++)
    {
        float totalEach = [[[arrFav valueForKey:@"foodQty"] objectAtIndex:i] floatValue] * [[[arr_cart valueForKey:@"foodPrice"] objectAtIndex:i] floatValue];
        [arrTotalPrice addObject:[NSString stringWithFormat:@"%.2f", totalEach]];
    }
    NSNumber *sumPrice = [arrTotalPrice valueForKeyPath:@"@sum.self"];
    if (arrFav.count == 0)
    {
        lblTotalPrice.text = [NSString stringWithFormat:@"0.00"];
        return;
    }
    lblTotalPrice.text = [NSString stringWithFormat:@"%@ %.2f",[[arrFav valueForKey:@"foodCurrency"] objectAtIndex:0],[sumPrice floatValue]];
}

#pragma mark - Database Methods
//insert - delete
-(void)insertDatabase:(NSString *)string
{
    NSString *passingStr = string;
    SQLFile *sql = [SQLFile new];
    if ([sql operationdb:passingStr])
    {
        NSLog(@"Inserted");
    }
    else
    {
        NSLog(@"Not Inserted");
    }
}
//update
-(void)updateDatabase:(NSString *)string
{
    NSString *passingStr = string;
    SQLFile *sql = [SQLFile new];
    if ([sql updatedb:passingStr])
    {
        NSLog(@"Updated");
    }
    else
    {
        NSLog(@"Not Updated");
    }
}

@end
