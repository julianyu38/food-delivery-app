//
//  FoodCategory.h
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UIImageView+RJLoader.h"
#import "UIImageView+WebCache.h"
#import "List.h"
#import "Reachability.h"
#import "SCLAlertView.h"

@interface FoodCategory : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *jsonResult;
    CGFloat headerHeight,fontSize;
    IBOutlet UIView *viewLoading;
    NSString *strCate;
    BOOL isSelected;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITableView *tbl_category;
@property (retain, nonatomic) NSMutableArray *arrCate;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *addLocate;

@end
