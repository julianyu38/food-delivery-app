//
//  FoodCategory.m
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "FoodCategory.h"

@interface FoodCategory ()

{
    NSString *language;
}
@end

@implementation FoodCategory

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self set_Language];
    viewLoading.hidden = NO;
    _tbl_category.dataSource = self;
    _tbl_category.delegate =self;
    self.tbl_category.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    NSString *strUrl = [NSString stringWithFormat:@"%@restaurant_category.php",SERVERURL];
    [self retriveJSON:strUrl];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    
    [self.connection1 cancel];
    self.connection1 = nil;
}

#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblTitle.text=NSLocalizedString(@"lblCuisine", @"");

    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
     [_addLocate setImage:[_addLocate.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
#pragma mark - Reachability
//Checks Internet Connection
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Retrive JSON Data
-(void)retriveJSON:(NSString *)string
{
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        viewLoading.hidden = YES;
    }
    else
    {
        NSString *strUrl = string;
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [self.connection1 start];
    }
}

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (_receivedData1 != nil)
    {
        jsonResult = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"Json :: %@, Error :: %@",jsonResult,error);
        if ([[jsonResult valueForKey:@"status"] isEqualToString:@"Success"])
        {
            
            _arrCate =[jsonResult valueForKey:@"Category_List"];
            [_tbl_category reloadData];
            
        }
        
      
//        [self.tbl_category reloadData];
        viewLoading.hidden = YES;
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
    }
}

#pragma mark - Button Action
//Back
- (IBAction)btn_back_click:(UIButton *)sender
{
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }

}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Tableview Datasource and Delegate Methods

//
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //set heigth for section
    if (iPhoneVersion == 4 || iPhoneVersion == 5)
    {
        headerHeight = 30;
    }
    else if(iPhoneVersion == 10)
    {
        headerHeight = 43;
    }
   
    else
    {
        headerHeight = 80;
    }
    return headerHeight;
}
//
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Creating header view dynamically
    if (iPhoneVersion == 5)
    {
        fontSize = 15.0f;
    }
    else if(iPhoneVersion == 10)
    {
        fontSize = 15.0f;
    }
    else
    {
        fontSize = 30.0f;
    }
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tbl_category.frame.size.width, headerHeight)];
    headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    UILabel *headerTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, self.tbl_category.frame.size.width-30, headerHeight)];
    [headerTitle setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:fontSize]];
    [headerTitle setText:[NSString stringWithFormat:@"%@",[[_arrCate objectAtIndex:section] valueForKey:@"name"]]];
    [headerView addSubview:headerTitle];

    return headerView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
      return _arrCate.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[[_arrCate objectAtIndex:section] valueForKey:@"subcategory"] count];
 
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    NSDictionary *Dic =
     UIImageView *cellimg = (UIImageView *)[cell.contentView viewWithTag:396];
    
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
                cellimg.image=[cellimg.image imageFlippedForRightToLeftLayoutDirection];
     
    }
    else
    {
        NSLog(@"en");
    }
    UILabel *cat_Lable = (UILabel *)[cell.contentView viewWithTag:101];
    [cat_Lable setText:[NSString stringWithFormat:@"%@",[[[[[_arrCate objectAtIndex:indexPath.section] valueForKey:@"subcategory"]objectAtIndex:indexPath.row]valueForKey:@"name"]substringToIndex:1]]];
    
    
    UILabel *cat_name = (UILabel *)[cell.contentView viewWithTag:102];
    [cat_name setText:[NSString stringWithFormat:@"%@",[[[[_arrCate objectAtIndex:indexPath.section] valueForKey:@"subcategory"]objectAtIndex:indexPath.row]valueForKey:@"name"]]];
    
   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.5f];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    [animation setType:@"rippleEffect"];
    [cell.layer addAnimation:animation forKey:NULL];
    
    List *list = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
    list.categoryName = [[[[_arrCate objectAtIndex:indexPath.section] valueForKey:@"subcategory"]objectAtIndex:indexPath.row]valueForKey:@"name"];
    list.strSearch =@"category";
    //[[[[[jsonResult valueForKey:@"Category_List"] objectAtIndex:indexPath.section] valueForKey:@"subcategory"] valueForKey:@"name"] objectAtIndex:indexPath.row];
    [self performSelector:@selector(gotoNextViewController:) withObject:list afterDelay:0.5];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Goto NextViewController
-(void)gotoNextViewController:(UIViewController *)next
{
    CATransition *transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:next animated:YES];
//    [self presentViewController:next animated:YES completion:nil];
}

#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
