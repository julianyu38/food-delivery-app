//
//  Register.m
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Register.h"
#import "List.h"

@interface Register ()
{
    NSString *language;
}
@end

@implementation Register

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    receivedData1 = [NSMutableData new];
    view_loading.frame = self.view.frame;
    view_loading.hidden = YES;
    viewCodes.hidden = YES;
    image_selected = NO;
    
    [self set_Language];
    [self retriveData];
    isChecked = NO;
//    for (ACFloatingTextField *textfield in scrollView.subviews)
    for (ACFloatingTextField *textfield in self.view_scroll.subviews)
    {
        if ([textfield isKindOfClass:[ACFloatingTextField class]])
        {
            textfield.delegate = self;
            [textfield setPlaceHolderTextColor:[UIColor blackColor]];
            textfield.selectedPlaceHolderTextColor = [UIColor blueColor];
            textfield.btmLineColor = [UIColor blackColor];
            textfield.btmLineSelectionColor = [UIColor blueColor];
        }
    }
    
    [imgProfile.layer setMasksToBounds:YES];
    [imgProfile.layer setCornerRadius:imgProfile.frame.size.width / 2];
    self.btn_signup.enabled = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [self.view_scroll addGestureRecognizer:tap];
    self.view_scroll.contentSize = CGSizeMake(self.view_scroll.frame.size.width, self.btn_signup.frame.origin.y + self.btn_signup.frame.size.height + 10);
    [self enableSignUp];
//    [self registerForKeyboardNotifications];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    
    [connection cancel];
    connection = nil;
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    
    _lblRegister.text = NSLocalizedString(@"lblRegister", @"");
    _lblAgree.text = NSLocalizedString(@"lblAgree", @"");
    _lblDesc.text = NSLocalizedString(@"lblDesc", @"");
    _lblSelectCountry.text = NSLocalizedString(@"lblSelectCountry", @"");
    
    _txt_username.placeholder=NSLocalizedString(@"txt_username", @"");
    _txt_phone.placeholder=NSLocalizedString(@"txt_phone", @"");
    _txt_password.placeholder=NSLocalizedString(@"txt_password", @"");
    _txt_referalcode.placeholder=NSLocalizedString(@"txt_referalcode", @"");
    _txt_email.placeholder=NSLocalizedString(@"txt_email", @"");
    _txt_code.placeholder=NSLocalizedString(@"txt_code", @"");
    
    [_btn_signup setTitle:NSLocalizedString(@"btn_signup", @"") forState:UIControlStateNormal];
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBcak setImage:[_btnBcak.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _txt_phone.textAlignment = UITextAlignmentRight;
        _txt_code.textAlignment = UITextAlignmentRight;
        _txt_username.textAlignment = UITextAlignmentRight;
        _txt_email.textAlignment = UITextAlignmentRight;
        _txt_referalcode.textAlignment = UITextAlignmentRight;
        _txt_password.textAlignment = UITextAlignmentRight;

    }
    else
    {
        NSLog(@"en");
    }
    
    
}

#pragma mark - Back button
- (IBAction)btn_back_click:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didClickHideSelectCountryView:(id)sender
{
    viewCodes.hidden=YES;
    
}
#pragma mark - UIGestureRecognizer Handler Method
-(void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (_txt_phone.text.length>0)
    {
       
        [self hasNoError:_txt_phone];

    }
    else
    {
         [self hasError:_txt_phone];
        
    }
    [self.txt_email resignFirstResponder];
    [self.txt_password resignFirstResponder];
    [self.txt_phone resignFirstResponder];
    [self.txt_referalcode resignFirstResponder];
    [self.txt_username resignFirstResponder];
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}



#pragma mark - UITextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [(ACFloatingTextField *)textField textFieldDidBeginEditing];
    
    if (textField.tag == 6)
    {
        [textField resignFirstResponder];
        viewCodes.hidden = NO;
    }
    else
    {
        [UIView animateWithDuration:0.75 animations:^{
            self.view_scroll.contentOffset = CGPointMake(self.view_scroll.frame.origin.x, activeField.frame.origin.y - activeField.frame.origin.y / 2);//activeField.frame.size.height);
        }];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    [(ACFloatingTextField *)textField textFieldDidEndEditing];
    [UIView animateWithDuration:0.75 animations:^{
        self.view_scroll.contentOffset = CGPointZero;
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [activeField resignFirstResponder];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(ACFloatingTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 1)
    {
        NSCharacterSet *exepted = [NSCharacterSet characterSetWithCharactersInString:NUMBERS];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:[exepted invertedSet]] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField.tag == 2)
    {
        NSCharacterSet *exepted = [NSCharacterSet characterSetWithCharactersInString:ALPHABETS];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:[exepted invertedSet]] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    [self enableSignUp];
    return YES;
}

#pragma mark - TextField Action
- (IBAction)textFieldChanged:(ACFloatingTextField *)textField
{
    if (textField.tag==1)
    {
        if (![self validatePhoneWithString:self.txt_phone.text])
        {
            [self hasError:textField];

        }
        else
        {
              [self hasNoError:textField];
           
        }
    }
    if (textField.tag==2)
    {
        if (![self validateNameWithString:self.txt_username.text])
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
    if (textField.tag==3)
    {
        if (![self validateEmailWithString:self.txt_email.text])
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
    if (textField.tag == 4)
    {
        if(textField.text.length > 2 && [self.txt_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0)
        {
            [self hasNoError:textField];
        }
        else
        {
            [self hasError:textField];
        }
    }
    if (textField.tag == 5)
    {
        if(textField.text.length > 0)
        {
            [self hasNoError:textField];
        }
        else
        {
            [self hasError:textField];
        }
    }
    if (textField.tag == 6)
    {
        if(textField.text.length > 0)
        {
            [self hasNoError:textField];
        }
        else
        {
            [self hasError:textField];
        }
    }
    [self enableSignUp];
}

#pragma mark - Validations and Error Check
-(BOOL)validateNameWithString:(NSString*)fullName
{
    NSString *nameRegex = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nameRegex];
    return [nameTest evaluateWithObject:fullName];
}

- (BOOL)validatePhoneWithString:(NSString*)phone
{
    NSString *phoneRegex = @"[0-9]{10}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//Checks for UITextField Entered Value
-(void)hasError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
    [textfield setBtmLineColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
    [textfield setBtmLineSelectionColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
}

-(void)hasNoError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0]];
    textfield.btmLineColor = [UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0];
    textfield.btmLineSelectionColor = [UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0];
}

-(void)enableSignUp
{
    if ([self validateEmailWithString:self.txt_email.text] && [self validateNameWithString:self.txt_username.text] && [self validatePhoneWithString:self.txt_phone.text] && self.txt_password.text.length > 2 && isChecked == YES && self.txt_code.text.length>0 && image_selected == YES)
    {
        [self.btn_signup setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        self.btn_signup.enabled = YES;
    }
    else
    {
        [self.btn_signup setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.btn_signup.enabled = NO;
    }
}

#pragma mark - Retrive data from Database
-(void)retriveData
{
    SQLFile *new = [SQLFile new];
    NSString *str = [NSString stringWithFormat:@"select * from Country"];
    arrCodes = [[NSMutableArray alloc]init];
    arrCodes = [new select_codes:str];
//    NSLog(@"Codes : %@",arrCodes);
}

#pragma mark - Tableview Datasource and Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (arrCodes == nil)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrCodes count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *lblSort = (UILabel *)[cell.contentView viewWithTag:401];
    [lblSort setText:[NSString stringWithFormat:@"%@",[[arrCodes valueForKey:@"iso"] objectAtIndex:indexPath.row]]];
    UILabel *lblFull = (UILabel *)[cell.contentView viewWithTag:402];
    [lblFull setText:[NSString stringWithFormat:@"%@",[[arrCodes valueForKey:@"name"] objectAtIndex:indexPath.row]]];
    UILabel *lblCode = (UILabel *)[cell.contentView viewWithTag:403];
    [lblCode setText:[NSString stringWithFormat:@"+%@",[[arrCodes valueForKey:@"phonecode"] objectAtIndex:indexPath.row]]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self.txt_code resignFirst2Responder];
    self.txt_code.text = [NSString stringWithFormat:@"+%@",[[arrCodes valueForKey:@"phonecode"] objectAtIndex:indexPath.row]];
    [self hasNoError:self.txt_code];
    viewCodes.hidden = YES;
}

#pragma mark - Checkbox button
- (IBAction)btn_check_action:(UIButton *)sender
{
    if (isChecked == NO)
    {
        [self.btn_check setImage:[UIImage imageNamed:@"check_btn.png"] forState:UIControlStateNormal];
        isChecked = YES;
    }
    else
    {
        [self.btn_check setImage:[UIImage imageNamed:@"uncheck_btn.png"] forState:UIControlStateNormal];
        isChecked = NO;
    }
    [self enableSignUp];
}

#pragma mark - Select Profile
-(IBAction)btn_selectImage:(id)sender
{
    [self.view endEditing:YES];
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

-(IBAction)btn_deleteImage:(id)sender
{
    [self.view endEditing:YES];
    imgProfile.image = [UIImage imageNamed:@"userIcon.png"];
    image_selected = NO;
    [self enableSignUp];
}

#pragma mark - Image Picker Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *choosenImage = info[UIImagePickerControllerOriginalImage];
    imagePath = [[info objectForKey:UIImagePickerControllerReferenceURL] absoluteString];
    
    imgProfile.image = choosenImage;
    image_selected = YES;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self enableSignUp];
}
- (IBAction)btn_TCS_action:(UIButton *)sender
{
    
}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Sign Up
- (IBAction)btn_signUp:(id)sender
{
    [self.view endEditing:YES];
    
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        [self.view addSubview:view_loading];
        [self registerUser];
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        [request setTimeoutInterval:30.0f];
//        //Set Params
//        [request setHTTPShouldHandleCookies:NO];
//        [request setHTTPMethod:@"GET"];
//
//        //Create boundary, it can be anything
//        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
//        
//        // set Content-Type in HTTP header
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data;boundary=%@",boundary];
//        [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
//        
//        //postbody
//        NSMutableData*body=[NSMutableData data];
//        NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
//        [parameters setValue:self.txt_username.text forKey:@"UserName"];
//        [parameters setValue:self.txt_email.text forKey:@"UserEmail"];
//        [parameters setValue:[NSString stringWithFormat:@"%@-%@",self.txt_code.text,self.txt_phone.text] forKey:@"UserContact"];
//        [parameters setValue:self.txt_password.text forKey:@"UserPwd"];
//        [parameters setValue:imagePath forKey:@"imagepath"];
//        NSLog(@"PARAM : %@",parameters);
//        // add params (all params are strings)
//        for (NSString *param in parameters)
//        {
//            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"%@\r\n",[parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
//        }
//                               
////       NSString *FileParamConstant = @"imagepath";
////       NSData *imageData = UIImageJPEGRepresentation(imgProfile.image, 1);
////       //Assuming data is not nil we add this to the multipart form
////       if (imageData)
////       {
////           [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
////           [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
////           [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
////           [body appendData:imageData];
////           [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
////       }
//       
//       //Close off the request with the boundary
////       [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//       
//       // setting the body of the post to the request
//       [request setHTTPBody:body];
//       
//       // set URL
//       [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@add-user?UserName=%@&UserEmail=%@&UserContact=%@-%@&UserPwd=%@&imagepath=%@",SERVERURL,_txt_username.text,_txt_email.text,_txt_code.text,_txt_phone.text,_txt_password.text,imagePath]]];
//       
//        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        [connection start];
    }
}

-(void)registerUser
{
    view_loading.hidden = NO;
    NSError *error;
//
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@add-user1",SERVERURL]];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
//    
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    
//    [request setHTTPMethod:@"POST"];
//    
//    NSString *byteArray = [UIImagePNGRepresentation(imgProfile.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    
//    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
//    [parameters setValue:self.txt_username.text forKey:@"UserName"];
//    [parameters setValue:self.txt_email.text forKey:@"UserEmail"];
//    [parameters setValue:[NSString stringWithFormat:@"%@-%@",self.txt_code.text,self.txt_phone.text] forKey:@"UserContact"];
//    [parameters setValue:self.txt_password.text forKey:@"UserPwd"];
//    [parameters setValue:byteArray forKey:@"Base64DataUrl"];
//    
//    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
//    [request setHTTPBody:postData];
//    
//    
//    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//        NSLog(@"Json :: %@, Error :: %@",json,error);
//    }];
//    
//    [postDataTask resume];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
    [parameters setValue:self.txt_username.text forKey:@"fullname"];
    [parameters setValue:self.txt_email.text forKey:@"email"];
     [parameters setValue:[NSString stringWithFormat:@"%@%@",self.txt_code.text,self.txt_phone.text] forKey:@"phone_no"];
    [parameters setValue:self.txt_referalcode.text forKey:@"referral_code"];
    [parameters setValue:self.txt_password.text forKey:@"password"];
//    [parameters setValue:encodedString forKey:@"Base64DataUrl"];
    
    NSLog(@"PARAM : %@",parameters);
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *FileParamConstant = @"file";
    
    NSData *imageData = UIImageJPEGRepresentation(imgProfile.image, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@userregister.php?",SERVERURL]]];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}
#pragma mark - NSURLConnection Delegate Method

-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    view_loading.hidden = YES;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (receivedData1 != nil)
    {
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
       
        NSLog(@"%@",json);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"id"]objectAtIndex:0]);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"fullname"]objectAtIndex:0]);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"email"]objectAtIndex:0]);
        NSLog(@"Json :: %@, Error :: %@",json,error);
        if ([[[json valueForKey:@"status"] objectAtIndex:0] isEqualToString:@"Success"])
        {
            _txt_username.text = @"";
            _txt_email.text = @"";
            _txt_code.text = @"";
            _txt_phone.text = @"";
            _txt_password.text = @"";
            imagePath = @"";
       
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"fullname"]objectAtIndex:0] forKey:@"Name"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"id"]objectAtIndex:0] forKey:@"User_id"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"email"]objectAtIndex:0] forKey:@"Mail"];
                 [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"image"]objectAtIndex:0] forKey:@"Profile"];
                 [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"phone_no"]objectAtIndex:0] forKey:@"PhoneNo"];
                   [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"referal_code"]objectAtIndex:0] forKey:@"ReferalCode"];
            [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"login_with"]objectAtIndex:0] forKey:@"LoginType"];
                
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                   
//                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:[NSString stringWithFormat:@"Success"] subTitle:@"You have registered successfully." closeButtonTitle:nil duration:0.0];
          
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
            
       
        else
        {
            [self showAlertView:[NSString stringWithFormat:@"Failed"] message:[NSString stringWithFormat:@"%@",[[json valueForKey:@"error"] objectAtIndex:0]] cancelButtonTitle:@"OK"];
//            SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
//            [alert addButton:@"OK" actionBlock:^{
//            }];
//            [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:[NSString stringWithFormat:@"info"] subTitle:[json valueForKey:@"sms"] closeButtonTitle:nil duration:0.0];
        }
        view_loading.hidden = YES;
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
    }
}

@end
