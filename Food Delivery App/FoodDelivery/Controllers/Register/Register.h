//
//  Register.h
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "LocateMe.h"
#import "Constants.h"
#import "Reachability.h"
#import "SQLFile.h"
#import "SCLAlertView.h"

@interface Register : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    BOOL isChecked;
    NSString *imagePath;
    IBOutlet UIView *viewCodes;
    IBOutlet UITableView *tblCodes;
    IBOutlet UIImageView *imgProfile;
    NSMutableArray *arrCodes;
    BOOL image_selected;
    IBOutlet UIView *view_loading;
    NSMutableData *receivedData1;
    NSURLConnection *connection;
//    IBOutlet UIScrollView *scrollView;
    UITextField *activeField;
}
@property(nonatomic,retain)NSString *page_name1;
@property (weak, nonatomic) IBOutlet UIButton *btnBcak;
@property(nonatomic,retain)NSString *Rest_ID;
@property (strong, nonatomic) IBOutlet UIScrollView *view_scroll;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_username;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_phone;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_password;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_referalcode;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_email;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_code;
@property (strong, nonatomic) IBOutlet UIButton *btn_signup;
@property (strong, nonatomic) IBOutlet UIButton *btn_check;

- (IBAction)btn_signUp:(id)sender;
- (IBAction)textFieldChanged:(ACFloatingTextField *)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;


@end
