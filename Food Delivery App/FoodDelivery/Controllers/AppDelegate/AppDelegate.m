//
//  AppDelegate.m
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "AppDelegate.h"
#import "DeliveryBoy.h"
@import Firebase;

@interface AppDelegate ()<FIRMessagingDelegate>
{
    
    NSString *InstanceID;
       NSString *userID;
       NSString *DeliveryBoyuserID;
}
@property (nonatomic, strong) NSString *strUUID;
@property (nonatomic, strong) NSString *strDeviceToken;

@end

@implementation AppDelegate
NSString *const kGCMMessageIDKey = @"gcm.message_id";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    //
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1)
    {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
        
#pragma clang diagnostic pop
    }
    else
    {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
        {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        }
        else
        {
            // iOS 10 or later
            
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].remoteMessageDelegate = self;
            
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
   [FIRApp configure];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [application setStatusBarHidden:YES];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"MyOrder"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"MyOrder"];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"Radius"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"100" forKey:@"Radius"];

    }
    NSMutableDictionary *myDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];

    

    UIViewController *Slider;
     UIViewController *DeliveryBoy;
    if (myDictionary != nil)
    {
        if (iPhoneVersion == 5)
        {
            
            DeliveryBoy = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DeliveryBoy"];
        }
        else if (iPhoneVersion == 10)
        {
            DeliveryBoy = [[UIStoryboard storyboardWithName:@"iPhoneX" bundle:nil] instantiateViewControllerWithIdentifier:@"DeliveryBoy"];
            
        }
        else
        {
            DeliveryBoy = [[UIStoryboard storyboardWithName:@"MainiPad" bundle:nil] instantiateViewControllerWithIdentifier:@"DeliveryBoy"];
        }
        
        navigation = [[UINavigationController alloc] initWithRootViewController:DeliveryBoy];
        navigation.navigationBarHidden = YES;
        self.window.rootViewController =navigation;
    }
    else
    {
        if (iPhoneVersion == 5)
        {
           
            Slider = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"List"];
        }
        else if (iPhoneVersion == 10)
        {
            Slider = [[UIStoryboard storyboardWithName:@"iPhoneX" bundle:nil] instantiateViewControllerWithIdentifier:@"List"];

        }
        else
        {
            Slider = [[UIStoryboard storyboardWithName:@"MainiPad" bundle:nil] instantiateViewControllerWithIdentifier:@"List"];
        }
        
        navigation = [[UINavigationController alloc] initWithRootViewController:Slider];
        navigation.navigationBarHidden = YES;
        self.window.rootViewController =navigation;
    }
    [self CopyandCheckdb];
    
    [GIDSignIn sharedInstance].clientID = GClientID;
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [locationManager requestAlwaysAuthorization];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation] | [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    if (userInfo[kGCMMessageIDKey])
    {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    NSLog(@"userInfo=>%@", userInfo);
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
    NSLog(@"deviceToken1 = %@",deviceToken);
    
}
- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"instanceId_notification=>%@",[notification object]);
    InstanceID = [NSString stringWithFormat:@"%@",[notification object]];
    [[NSUserDefaults standardUserDefaults] setObject:InstanceID forKey:@"firebaseId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (InstanceID !=nil) {
         [self GetData];
    }
    
    [self connectToFcm];
}
- (void)connectToFcm {
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            
            
            NSLog(@"InstanceID_connectToFcm = %@", InstanceID);
           
           
    
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [self sendDeviceInfo];
                    NSLog(@"instanceId_tokenRefreshNotification22=>%@",[[FIRInstanceID instanceID] token]);
                
                    
                });
            });
            
            
        }
    }];}
// Handle incoming notification messages while app is in the foreground.

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    
    // Print message ID.
    NSLog(@"userNotificationCenter: willPresentNotification: withCompletionHandler:");
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey])
    {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
        
    }
    
    //     Print full message.
    NSLog(@"%@", userInfo);
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIFICATION" object:nil];
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}
#pragma mark - Retirve Data From Webservice
-(void)GetData
{
 
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
    NSMutableDictionary *myDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];
    DeliveryBoyuserID = [[myDictionary valueForKey:@"id"]objectAtIndex:0];
    
    NSString *StringURL ;
    if (myDictionary != nil)
    {
        StringURL= [NSString stringWithFormat:@"%@token.php?token=%@&type=Iphonw&user_id=null&delivery_boyid=%@",SERVERURL,InstanceID,DeliveryBoyuserID];
        NSLog(@"URL :: %@",StringURL);
    }
    else
    {
        StringURL= [NSString stringWithFormat:@"%@token.php?token=%@&type=Iphone&user_id=%@&delivery_boyid=null",SERVERURL,InstanceID,userID];
        NSLog(@"URL :: %@",StringURL);
    }
        StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
        
        _connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [_connection1 start];
    
}
/*-(void)POST_Order
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
    NSMutableData *body = [NSMutableData data];
    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];
        DeliveryBoyuserID = [[myDictionary valueForKey:@"id"]objectAtIndex:0];
    if (myDictionary != nil)
    {
        
        [parameters setValue:InstanceID forKey:@"token"];
        
        [parameters setValue:@"Iphone" forKey:@"type"];
        [parameters setValue:@"null" forKey:@"user_id"];
        [parameters setValue:DeliveryBoyuserID forKey:@"delivery_boyid"];
    }
    else
    {
        NSLog(@"TOKEN123 : %@",InstanceID);
        [parameters setValue:InstanceID forKey:@"token"];
        
        [parameters setValue:@"Iphone" forKey:@"type"];
        [parameters setValue:userID forKey:@"user_id"];
        [parameters setValue:@"null" forKey:@"delivery_boyid"];
    }
    
    NSLog(@"PARAM : %@",parameters);
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //Assuming data is not nil we add this to the multipart form
    
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@token.php",SERVERURL]]];
    
    _connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection1 start];
    
    
}
*/

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == _connection1)
    {
        NSError *error;
        if (_receivedData1 != nil)
        {
            
            NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"JSON Result notification:: %@",jsonResult);
            
        }
        else
        {
            
        }
        
    }
}
#pragma mark - Copy and Check Database
-(void)CopyandCheckdb
{
    NSArray *dirpath = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *docdir =[dirpath objectAtIndex:0];
    self.strdbpath =[docdir stringByAppendingString :@"FoodData.sqlite"];
    NSLog(@"dbpath =%@",self.strdbpath);
    BOOL success;
    NSFileManager *fm =[NSFileManager defaultManager];
    success =[fm fileExistsAtPath:self.strdbpath];
    
    if (success)
    {
        NSLog(@"Already Present");
    }
    else
    {
        NSError *err;
        NSString *resource =[[NSBundle mainBundle]pathForResource:@"FoodData" ofType:@"sqlite"];
        success =[fm copyItemAtPath:resource toPath:self.strdbpath error:&err];
        if (success)
        {
            NSLog(@"Successfully Created");
        }
        else
        {
            NSLog(@"Error = %@",err);
        }
    }
}

@end
