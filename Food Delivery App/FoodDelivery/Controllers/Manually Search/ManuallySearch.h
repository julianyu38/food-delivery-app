//
//  ManuallySearch.h
//  FoodDeliverySystem
//
//  Created by R on 09/07/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "List.h"
#import "LocateMe.h"
#import "Reachability.h"
#import <CoreMotion/CoreMotion.h>

@interface ManuallySearch : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *view_loading;
    CMMotionManager *motionManager;
    NSMutableArray *searchdata, *arr;
}

@property (strong, nonatomic) IBOutlet UITableView *tbl_SearchData;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;

- (IBAction)btn_search:(UIButton *)sender;
- (IBAction)btn_back_click:(UIButton *)sender;
- (IBAction)textFieldChange:(id)sender;


@end
