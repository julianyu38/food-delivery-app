//
//  ManuallySearch.m
//  FoodDeliverySystem
//
//  Created by R on 09/07/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "ManuallySearch.h"

@interface ManuallySearch ()
@end

@implementation ManuallySearch


#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    view_loading.hidden = YES;
    self.tbl_SearchData.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    motionManager = [[CMMotionManager alloc] init];
    motionManager.deviceMotionUpdateInterval = 1.00/60.00;
    [motionManager startDeviceMotionUpdates];
    CMDeviceMotion *currentDeviceMotion = motionManager.deviceMotion;
    NSLog(@"%@",currentDeviceMotion);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.tbl_SearchData.hidden = YES;
    [self.txt_search resignFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    
    [self.connection1 cancel];
    self.connection1 = nil;
}
#pragma mark - UITextField Delegate Method
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Retrive Data
-(void)retriveData
{
    view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        searchdata = [[NSMutableArray alloc]init];
        NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&key=%@",self.txt_search.text,API_KEY];
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [self.connection1 start];
    }
}

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (_receivedData1 != nil)
    {
        NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
        arr = [dataDic valueForKey:@"predictions"];
        for (int i=0; i<[arr count]; i++)
        {
            [searchdata addObject:[[[dataDic valueForKey:@"predictions"] objectAtIndex:i] valueForKey:@"description"]];
        }
        self.tbl_SearchData.hidden=NO;
        [self.tbl_SearchData reloadData];
        view_loading.hidden = YES;
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        view_loading.hidden = YES;
    }
}

#pragma mark - Button Action
//Search
- (IBAction)btn_search:(UIButton *)sender
{
    view_loading.hidden = NO;
    [self.txt_search resignFirstResponder];
    if (self.txt_search.text.length > 0)
    {
        [self getLocationFromAddressString:self.txt_search.text];
    }
    else
    {
        NSLog(@"Plase enter searchdata");
    }
}

//Show Restaurant List view
- (IBAction)btn_list:(UIButton *)sender
{
    List *list = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
//    [self presentViewController:list animated:YES completion:nil];
    [self.navigationController pushViewController:list animated:YES];
}
//Back
- (IBAction)btn_back_click:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Action
- (IBAction)textFieldChange:(id)sender
{
    if ([self.txt_search.text length]>0)
    {
        [self retriveData];
    }
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchdata count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = searchdata[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    view_loading.hidden = NO;
    self.txt_search.text = searchdata[indexPath.row];
    [self.txt_search resignFirstResponder];
    self.tbl_SearchData.hidden = YES;
    [self getLocationFromAddressString:self.txt_search.text];
}

#pragma mark - CLLocation Fetch Latitude-Lonngitude From Address
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr
{
    double latitude = 0, longitude = 0;
    NSLog(@"Search Text:::%@",addressStr);
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result)
    {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"Logitute : %f, Latitute : %f",center.latitude,center.longitude);
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:[[CLLocation alloc]initWithLatitude:center.latitude longitude:center.longitude] completionHandler:^(NSArray *placemarks, NSError *error)
     {
         CLPlacemark* placemark;
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             NSMutableDictionary *location = [[NSMutableDictionary alloc] init];
             [location setValue:[NSString stringWithFormat:@"%f",center.latitude] forKey:@"latitude"];
             [location setValue:[NSString stringWithFormat:@"%f", center.longitude] forKey:@"longitude"];
             [location setValue:addressStr forKeyPath:@"address"];
             [location setValue:placemark.postalCode forKeyPath:@"zipcode"];
             NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
             [userDefault setObject:location forKey:@"location"];
             [userDefault synchronize];
             NSLog(@"Default :: %@",[userDefault valueForKey:@"location"]);
             view_loading.hidden = YES;
         }
     }];

    return center;
}

@end
