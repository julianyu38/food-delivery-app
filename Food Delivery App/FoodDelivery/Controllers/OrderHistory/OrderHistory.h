//
//  OrderHistory.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 29/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistory : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

//UIView
@property (weak, nonatomic) IBOutlet UIView *view_loading;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@end
