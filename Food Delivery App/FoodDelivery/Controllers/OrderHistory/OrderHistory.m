//
//  OrderHistory.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 29/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "OrderHistory.h"
#import "Constants.h"
#import "Reachability.h"
#import "Snackbar.h"
#import "OrderDetail.h"
@interface OrderHistory ()
{
    NSString *language;
    NSMutableArray *ArrOrderHistory;
     NSMutableDictionary *dicDeliveryProfile;
}

@end

@implementation OrderHistory

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_Language];
    
    _tableView.delegate =self;
    _tableView.dataSource =self;
    _tableView.tableFooterView=[UIView new];
    
}
- (void)viewDidAppear:(BOOL)animated
{
   
 dicDeliveryProfile = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];
     [self retriveJSON:[NSString stringWithFormat:@"%@order_history.php?deliverboy_id=%@",SERVERURL,[[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0]]flag:@"1"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)didClickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
       
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrOrderHistory.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView  *imgOrder=(UIImageView *) [cell.contentView viewWithTag:101];
    UILabel *lblOrderNo=(UILabel *) [cell.contentView viewWithTag:102];
    UILabel *lblOrderAmt=(UILabel *) [cell.contentView viewWithTag:103];
    UILabel *lblOrderItme=(UILabel *) [cell.contentView viewWithTag:104];
    UILabel *lblDataTime=(UILabel *) [cell.contentView viewWithTag:105];
   
    NSDictionary *dic = [ArrOrderHistory objectAtIndex:indexPath.row];
    
    
    if ([[dic valueForKey:@"status"]isEqualToString:@"Order is Delivered"]) {
        imgOrder.image=[UIImage imageNamed:@"img_ordercomplete.png"];
    }else
    {
        imgOrder.image=[UIImage imageNamed:@"img_orderprocess.png"];
    }
    //set Text
    
    
    lblOrderNo.text=[NSString stringWithFormat:@"Order No %@",[dic valueForKey:@"order_no"]];
    lblOrderAmt.text=[NSString stringWithFormat:@"Order Amount %@ %@",dollar,[dic valueForKey:@"total_amount"]];
    lblOrderItme.text=[NSString stringWithFormat:@"%@ Items",[dic valueForKey:@"items"]];
    lblDataTime.text=[dic valueForKey:@"date"];
    
    //set Font
    lblOrderNo.font = [UIFont fontWithName:RFontSemibold size:lblOrderNo.font.pointSize];
    lblOrderAmt.font = [UIFont fontWithName:RFontRegular size:lblOrderAmt.font.pointSize];
    lblOrderItme.font = [UIFont fontWithName:RFontRegular size:lblOrderItme.font.pointSize];
    lblDataTime.font = [UIFont fontWithName:RFontRegular size:lblDataTime.font.pointSize];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [ArrOrderHistory objectAtIndex:indexPath.row];
    
    OrderDetail *next =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetail"];
    next.orderId = [dic valueForKey:@"order_no"];
    next.strStatus=[dic valueForKey:@"status"];
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}
#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                ArrOrderHistory =[[NSMutableArray alloc]init];
                ArrOrderHistory=[json valueForKey:@"order"];
                NSLog(@"arrAssigned %@",ArrOrderHistory);
                [_tableView reloadData];
            }
            
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:[json valueForKey:@"order"] duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    
    _view_loading.hidden = YES;
}


@end
