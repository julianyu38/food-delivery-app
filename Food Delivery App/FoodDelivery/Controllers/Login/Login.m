//
//  Login.m
//  FoodDelivery
//
//  Created by Redixbit on 27/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Login.h"
#import "Review.h"
#import "DeliveryBoyLogin.h"

@interface Login ()
{
    NSString *language;

}

@end

@implementation Login
#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    receivedData1 = [NSMutableData new];
    view_loading.hidden = YES;
    [self set_Language];
    for (ACFloatingTextField *textfield in self.view_scroll.subviews)
    {
        if ([textfield isKindOfClass:[ACFloatingTextField class]])
        {
            textfield.delegate = self;
            [textfield setPlaceHolderTextColor:[UIColor blackColor]];
            textfield.selectedPlaceHolderTextColor = [UIColor blueColor];
            textfield.btmLineColor = [UIColor blackColor];
            textfield.btmLineSelectionColor = [UIColor blueColor];
        }
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];

    [self.view endEditing:YES];
    [self.connection cancel];
    self.connection = nil;
}

#pragma mark - UIGestureRecognizer Handler Method
-(void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    for (ACFloatingTextField *textfield in self.view_scroll.subviews)
    {
        if ([textfield isKindOfClass:[ACFloatingTextField class]])
        {
            [textfield resignFirstResponder];
        }
    }
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    
    _lblLogin.text=NSLocalizedString(@"lblLogin", @"");
    txt_email.placeholder=NSLocalizedString(@"txt_email", @"");
    txt_password.placeholder=NSLocalizedString(@"txt_password", @"");
    _lblBtnLogin.text =NSLocalizedString(@"lblLogin", @"");
    _lblOr.text=NSLocalizedString(@"lblOr", @"");
    _lblFacebook.text = NSLocalizedString(@"lblFacebook", @"");
    _lblGoogle.text =NSLocalizedString(@"lblGoogle", @"");
    [_btnCreateAccount setTitle:NSLocalizedString(@"btnCreateAccount", @"") forState:UIControlStateNormal];
    [_btnDeliveryBoyLogin setTitle:NSLocalizedString(@"btnDeliveryBoyLogin", @"") forState:UIControlStateNormal];
    
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
         _Imgscooter.image=[_Imgscooter.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBAck setImage:[_btnBAck.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        txt_email.textAlignment = UITextAlignmentRight;
        txt_password.textAlignment = UITextAlignmentRight;
        _btnCreateAccount.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    }
    else
    {
        NSLog(@"en");
    }
    
    
}
#pragma mark - Retrive Data from Webservice
-(void)Login:(NSString *)url
{
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        [self handleTapGesture:nil];
        NSString *strUrl = url;
        NSLog(@"URL :: %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
 
        _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [_connection start];
    }
}

#pragma mark - NSURLConnection Delegate Method

-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (receivedData1 != nil)
    {
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",json);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"id"]objectAtIndex:0]);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"fullname"]objectAtIndex:0]);
        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"email"]objectAtIndex:0]);
        NSLog(@"json : %@, Error :: %@",json,error);
        if (!error)
        {
            if ([[[json valueForKey:@"status"] objectAtIndex:0] isEqualToString:@"Success"])
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"fullname"]objectAtIndex:0] forKey:@"Name"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"id"]objectAtIndex:0] forKey:@"User_id"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"email"]objectAtIndex:0] forKey:@"Mail"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"image"]objectAtIndex:0] forKey:@"Profile"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"phone_no"]objectAtIndex:0] forKey:@"PhoneNo"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"referal_code"]objectAtIndex:0] forKey:@"ReferalCode"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"user_detail"]valueForKey:@"login_with"]objectAtIndex:0] forKey:@"LoginType"];
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                
//
                }];
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:[NSString stringWithFormat:@"Success"] subTitle:@"Login Success." closeButtonTitle:nil duration:0.0];

            }
            else
            {
                [self showAlertView:[NSString stringWithFormat:@"Failed"] message:[NSString stringWithFormat:@"%@",[[json objectAtIndex:0]valueForKey:@"error"]]cancelButtonTitle:@"OK"];
            }
        }
        else
        {
            NSLog(@"Error :: %@",error);
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Please try again later." duration:3.0];
            [self.view addSubview:snackbar];
//            [self showAlertView:@"Info" message:@"Please try again later." cancelButtonTitle:@"OK"];
        }
        view_loading.hidden = YES;
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        view_loading.hidden = YES;
    }
}

#pragma mark - UITextfield Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [(ACFloatingTextField *)textField textFieldDidBeginEditing];
    [UIView animateWithDuration:0.75 animations:^{
        self.view_scroll.contentOffset = CGPointMake(self.view_scroll.frame.origin.x, textField.frame.origin.y - textField.frame.origin.y / 2);
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [(ACFloatingTextField *)textField textFieldDidEndEditing];
    [UIView animateWithDuration:0.75 animations:^{
        self.view_scroll.contentOffset = CGPointZero;
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextField Method
- (IBAction)textFieldChanged:(ACFloatingTextField *)textField
{
    if (textField.tag==1)
    {
        if (![self validateEmailWithString:txt_email.text])
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
    if (textField.tag == 2)
    {
        if (textField.text.length < 3)
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
}

#pragma mark - Validations
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//Showing Error when user enters incorrect data
-(void)hasError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor redColor]];
    [textfield setBtmLineColor:[UIColor redColor]];
    [textfield setBtmLineSelectionColor:[UIColor redColor]];
}

-(void)hasNoError:(ACFloatingTextField *)textfield
{
    textfield.placeHolderTextColor = [UIColor greenColor];
    textfield.btmLineColor = [UIColor greenColor];
    textfield.btmLineSelectionColor = [UIColor greenColor];
}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Button Action
- (IBAction)btn_back_click:(UIButton *)sender
{
    view_loading.hidden = YES;
    

    [self.navigationController popViewControllerAnimated:YES];
}

//Register
- (IBAction)btn_register:(UIButton *)sender
{
    view_loading.hidden = YES;
    Register *reg = [self.storyboard instantiateViewControllerWithIdentifier:@"Register"];
    reg.Rest_ID =_Res_ID;
    reg.page_name1 = _page_name;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self presentViewController:reg animated:YES completion:nil];
    [self.navigationController pushViewController:reg animated:YES];
}

//Login
- (IBAction)btn_login:(UIButton *)sender
{
    if (![self validateEmailWithString:txt_email.text] || txt_password.text.length == 0)
    {
        [self showAlertView:@"Info" message:@"Please enter email or password" cancelButtonTitle:@"OK"];
    }
    else
    {
        view_loading.hidden = NO;
   

        [self Login:[NSString stringWithFormat:@"%@userlogin.php?email=%@&password=%@",SERVERURL,txt_email.text,txt_password.text]];
//        [self Login:[NSString stringWithFormat:@"%@userlogin.php?email=%@&password=%@",SERVERURL,txt_email.text,txt_password.text]];
    }
}

//Login with Facebook
- (IBAction)btn_facebook:(UIButton *)sender
{
    view_loading.hidden = NO;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error)
         {
             NSLog(@"Process error");
             view_loading.hidden = YES;
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             view_loading.hidden = YES;
         }
         else
         {
             NSLog(@"Logged in");
             NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
             
             [parameters setValue:@"id,name,email,picture" forKey:@"fields"];
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             {
                 if (!error)
                 {
                     NSLog(@"Result :: %@",result);
                     NSString *name = [result valueForKey:@"name"];
                     NSString *email = [result valueForKey:@"email"];
                     NSString *profile = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&w‌​idth=720&height=720", [result valueForKey:@"id"]];
                     NSString *mobile;
                     NSString *refcode;
                     
                   
                     [self Login:[NSString stringWithFormat:@"%@userlogin.php?login_type=Facebook&fullname=%@&email=%@&phone_no=&referral_code=&image=%@",SERVERURL,name,email,profile]];
                 }
              }];
         }
    }];
}

//Login with Google+
- (IBAction)btn_google:(UIButton *)sender
{
    [view_loading setHidden:NO];
    [[GIDSignIn sharedInstance] signOut];
    [GIDSignInButton class];
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [[GIDSignIn sharedInstance] setAllowsSignInWithWebView:NO];
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)didClickDeliveryBoyLoginButton:(id)sender {
    DeliveryBoyLogin *reg = [self.storyboard instantiateViewControllerWithIdentifier:@"DeliveryBoyLogin"];
    [self.navigationController pushViewController:reg animated:YES];
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (error)
    {
        NSLog(@"Status: Authentication error: %@", error);
        view_loading.hidden = YES;
        return;
    }
    [self reportAuthStatus];
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (error)
    {
        NSLog(@"Status: Failed to disconnect: %@", error);
    }
    else
    {
        NSLog(@"Status: Disconnected");
    }
    view_loading.hidden = YES;
    [self reportAuthStatus];
}

- (void)presentSignInViewController:(UIViewController *)viewController
{
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark Helper methods
- (void)reportAuthStatus
{
    GIDGoogleUser *googleUser = [[GIDSignIn sharedInstance] currentUser];
    if (googleUser.authentication)
    {
        NSLog(@"Status: Authenticated");
    }
    else
    {
        // To authenticate, use Google+ sign-in button.
        NSLog(@"Status: Not authenticated");
    }
    [self refreshUserInfo];
}
// Update the interface elements containing user data to reflect the
// currently signed in user.
- (void)refreshUserInfo
{
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil)
    {
        return;
    }
    NSString *name = [GIDSignIn sharedInstance].currentUser.profile.name;
    NSString *email = [GIDSignIn sharedInstance].currentUser.profile.email;
    NSString *mobile;
    NSString *refcode;
    
    NSString *profileImage = [[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:100].absoluteString;
    NSLog(@"PRofile :: %@",profileImage);
    NSLog(@"Email :%@",[GIDSignIn sharedInstance].currentUser.profile.email);
    NSLog(@"Name :%@",[GIDSignIn sharedInstance].currentUser.profile.name);
    [self Login:[NSString stringWithFormat:@"%@userlogin.php?login_type=Google&fullname=%@&email=%@&phone_no=&referral_code=&image=%@",SERVERURL,name,email,profileImage]];
}

@end
