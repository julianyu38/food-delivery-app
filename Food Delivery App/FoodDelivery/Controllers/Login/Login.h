//
//  Login.h
//  FoodDelivery
//
//  Created by Redixbit on 27/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "Register.h"
#import "Constants.h"
#import "Reachability.h"
#import "SCLAlertView.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface Login : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate,NSURLConnectionDelegate,GIDSignInDelegate,GIDSignInUIDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet ACFloatingTextField *txt_email, *txt_password;
    NSMutableData *receivedData1;
    IBOutlet UIView *view_loading;
}
@property(nonatomic,retain)NSString *page_name;
@property(nonatomic,retain)NSString *Res_ID;
@property (weak, nonatomic) IBOutlet UIButton *btnBAck;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateAccount;

@property (strong, nonatomic) IBOutlet UIScrollView *view_scroll;
@property (retain, nonatomic) NSURLConnection *connection;
@property (weak, nonatomic) IBOutlet UIImageView *Imgscooter;
@property (weak, nonatomic) IBOutlet UILabel *lblLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UILabel *lblFacebook;
@property (weak, nonatomic) IBOutlet UILabel *lblGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnDeliveryBoyLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnLogin;

@end
