//
//  MyOrder.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 23/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "MyOrder.h"
#import "List.h"
#import "OrderStatus.h"
@interface MyOrder ()
{
    NSUserDefaults *userDefaults;
    NSMutableArray *arrOder;
    NSString *language;

  
}
@end

@implementation MyOrder

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults=[NSUserDefaults standardUserDefaults];
    arrOder=[[userDefaults objectForKey:@"MyOrder"]mutableCopy];
    
    [self set_Language];

    _tbl_myOrder.delegate =self;
    _tbl_myOrder.dataSource =self;

}


- (IBAction)didClickBackButton:(id)sender {
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }
}

#pragma mark - SET LANGUAGE
-(void)set_Language
{
    _lblTitle.text = NSLocalizedString(@"lblMyOrders", @"");

    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        ;
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
      
        
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOder.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
      NSDictionary *dic = [arrOder objectAtIndex:indexPath.row];
    
    UILabel *lblFirstWord=(UILabel *) [cell.contentView viewWithTag:101];
    UILabel *lblRestName=(UILabel *) [cell.contentView viewWithTag:103];
    UILabel *lblRestAddress=(UILabel *) [cell.contentView viewWithTag:104];
    UILabel *lblDataTime=(UILabel *) [cell.contentView viewWithTag:105];
    UILabel *lblTotalPrice=(UILabel *) [cell.contentView viewWithTag:107];
     UILabel *lblTotal=(UILabel *) [cell.contentView viewWithTag:110];
    
    lblTotal.text = NSLocalizedString(@"lbltotal1", @"");
    
    NSString *str = [dic valueForKey:@"rest_name"];
    lblFirstWord.text= [[str substringToIndex:1] uppercaseString];
    lblRestName.text=[dic valueForKey:@"rest_name"];
    lblRestAddress.text=[dic valueForKey:@"rest_address"];
    lblDataTime.text=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"orderDate", @""),[dic valueForKey:@"order_date"]];
    lblTotalPrice.text=[NSString stringWithFormat:@"%@%@",[dic valueForKey:@"foodCurrency"],[dic valueForKey:@"totle_price"]];

    
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        ;
        lblTotalPrice.textAlignment = UITextAlignmentLeft;
        lblTotal.textAlignment = UITextAlignmentLeft;
        
    }
    else
    {
        NSLog(@"en");
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [arrOder objectAtIndex:indexPath.row];
    OrderStatus *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderStatus"];
    detail.orderId=[dic valueForKey:@"oder_id"];
    detail.strScreen=@"myorder";
    [self.navigationController pushViewController:detail animated:YES];
}

@end
