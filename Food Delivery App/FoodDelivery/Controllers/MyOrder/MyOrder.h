//
//  MyOrder.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 23/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrder : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *TableView;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tbl_myOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@end
