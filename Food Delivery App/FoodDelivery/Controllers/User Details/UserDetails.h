//
//  UserDetails.h
//  FoodDelivery
//
//  Created by Redixbit on 04/10/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ACFloatingTextField.h"
#import "SCLAlertView.h"
#import "Constants.h"
#import "Reachability.h"
#import "SQLFile.h"
#import <MapKit/MapKit.h>

#import "Login.h"
#import "OrderSucess.h"

@interface UserDetails : UIViewController<UITextFieldDelegate,MKMapViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSMutableArray *arr_cart, *items, *arrTotalPrice;
    UITextField *activeField;
    NSDecimalNumber *total;
    CGFloat W,H;
    NSString *description, *userID, *zipcode, *cityName,*Email;
    UIDatePicker *DatePicker;
    UIDatePicker *TimePicker;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblUserInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectLocation;

@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceOrder;

//Radio buttons
@property (weak, nonatomic) IBOutlet UIButton *btn_paypal;
@property (weak, nonatomic) IBOutlet UIButton *btn_cashOnDelivery;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_street;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_flat;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_landmark;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_city;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_zipcode;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txt_desc;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (nonatomic, retain) NSDecimalNumber *totalPay;
@property (nonatomic, retain) NSDecimalNumber *totalPayAmount;
@property (nonatomic, retain) NSString *paymentStyle, *restDelTime;

//Paypal Properties

@property(nonatomic, retain) NSMutableArray *items;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) NSString *restName;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) CLLocationCoordinate2D coordinates;


@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;
@property (weak, nonatomic) IBOutlet UIView *viewLoading;

@property (strong, nonatomic) IBOutlet UIScrollView *view_scroll;

@end
