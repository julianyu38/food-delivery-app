//
//  UserDetails.m
//  FoodDelivery
//
//  Created by Redixbit on 04/10/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "UserDetails.h"
#import "OrderStatus.h"

@interface UserDetails ()
{
    NSString *language;
    NSString *address1,*foodCurrency1;
    double latitude1;
    double longitude1;
    CLLocation *location1;
    NSString *currentTime;
    NSString *currentDate;
    NSString *strSelectLocation;

}
@end

@implementation UserDetails
@synthesize items;

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    _viewLoading.hidden=YES;
    strSelectLocation =@" ";
    //Set up ACFloatingTextField
    for (ACFloatingTextField *textfield in self.view_scroll.subviews)
    {
        if ([textfield isKindOfClass:[ACFloatingTextField class]])
        {
            textfield.delegate = self;
            [textfield setPlaceHolderTextColor:[UIColor blackColor]];
            textfield.selectedPlaceHolderTextColor = [UIColor blueColor];
            textfield.btmLineColor = [UIColor blackColor];
            textfield.btmLineSelectionColor = [UIColor blueColor];
        }
    }
    [self set_Language];

    [self retriveData];
    [self OrderDataAndTime];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [self.view_scroll addGestureRecognizer:tap];
       [_view_scroll setContentSize:CGSizeMake(_view_scroll.frame.size.width, _btnPlaceOrder.frame.origin.y+_btnPlaceOrder.frame.size.height+10)];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
    _mapView.mapType = MKMapTypeStandard;
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    Email = [[NSUserDefaults standardUserDefaults] valueForKey:@"Mail"];
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
    
    NSMutableDictionary *location = [[NSUserDefaults standardUserDefaults]valueForKey:@"location"];
    cityName = [location valueForKey:@"address"];
}
-(void)OrderDataAndTime
{
    DatePicker = [[UIDatePicker alloc]init];
    DatePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    currentDate = [dateFormatter stringFromDate:DatePicker.date];
    
    
    TimePicker = [[UIDatePicker alloc]init];
    TimePicker.datePickerMode = UIDatePickerModeTime;
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"hh:mm a"];
    currentTime = [dateFormatter1 stringFromDate:TimePicker.date];
   
    
    
    
    
}
#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SET LANGUAGE
#pragma mark - MKMapView Delegate Methods
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    
    location1 = [[CLLocation alloc] initWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude];
    _coordinates = CLLocationCoordinate2DMake(mapView.centerCoordinate.latitude, mapView.centerCoordinate.longitude);
    latitude1=mapView.centerCoordinate.latitude;
    longitude1=mapView.centerCoordinate.longitude;
    
  
}
-(void)set_Language
{
    _lblUserInfo.text = NSLocalizedString(@"lblUserInformation", @"");
    [_btnSelectLocation setTitle:NSLocalizedString(@"btnSelectLocation", @"") forState:UIControlStateNormal];
    [_btnPlaceOrder setTitle:NSLocalizedString(@"btnPlaceOrder", @"") forState:UIControlStateNormal];
    _txt_landmark.placeholder=NSLocalizedString(@"txtAddress", @"");
    _txt_desc.placeholder = NSLocalizedString(@"txtNote", @"");
    
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
         [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        
        _txt_landmark.textAlignment = UITextAlignmentRight;
        _txt_desc.textAlignment = UITextAlignmentRight;


    }
    else
    {
        NSLog(@"en");
    }
    
    
}

#pragma mark - Button Action
//Back button
- (IBAction)btn_back_click:(UIButton *)sender
{
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    if (userID)
    {
        [navigationArray removeObjectAtIndex: navigationArray.count- (1)];  // You can pass your index here
        self.navigationController.viewControllers = navigationArray;
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}



//Place Order Button
-(IBAction)btn_placeOrder:(id)sender
{
    if (self.txt_landmark.text.length > 0 && _txt_desc.text.length > 0 && ![strSelectLocation isEqualToString:@" "])
    {
        if (userID != nil)
        {


            _viewLoading.hidden = NO;
            
            foodCurrency1 =[[arr_cart valueForKey:@"foodCurrency"] objectAtIndex:0];

            [self POST_Order];

            

        }
        else
        {
            SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
            [alert addButton:@"YES" actionBlock:^{
                Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
//                [self presentViewController:login animated:YES completion:nil];
                [self.navigationController pushViewController:login animated:YES];
            }];
            [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:@"Warning" subTitle:@"To place order you must login first. Do you want to login now?" closeButtonTitle:@"NO" duration:0.0];
        }
    }
    else
    {
        NSString *msg = @"";
       
        if (self.txt_landmark.text.length <= 0)
        {
            msg = [msg stringByAppendingString:@"Enter select address from map\n"];
        }
        
        if (self.txt_desc.text.length <= 0)
        {
            msg = [msg stringByAppendingString:@"Enter Notes\n"];
        }
        if ([strSelectLocation isEqualToString:@" "])
        {
            msg = [msg stringByAppendingString:@"Please select location\n"];
        }
       
        [self showAlertView:@"Error" message:msg cancelButtonTitle:@"OK"];
    }
}
- (IBAction)didClickSelectLocationButton:(id)sender {
    _viewLoading.hidden=NO;
    _txt_landmark.text =@"";
    strSelectLocation = @"select location";
    CLGeocoder *geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location1 completionHandler:^(NSArray *placemarks, NSError *error)
     {
         CLPlacemark* placemark;
         address1 = @"";
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             
             if (placemark.name)
             {
                 address1 = [address1 stringByAppendingString:[NSString stringWithFormat:@"%@," ,placemark.name]];
             }
             
             if (placemark.subLocality)
             {
                 address1 = [address1 stringByAppendingString:[NSString stringWithFormat:@" %@,",placemark.subLocality]];
             }
             
             if (placemark.locality)
             {
                 address1= [address1 stringByAppendingString:[NSString stringWithFormat:@" %@,",placemark.locality]];
             }
             
             if(placemark.postalCode)
             {
                 address1 = [address1 stringByAppendingString:[NSString stringWithFormat:@" %@",placemark.postalCode]];
             }
             NSLog(@"CheckState %@",[address1 stringByAppendingString:[NSString stringWithFormat:@" %@",placemark.administrativeArea]]);
              _txt_landmark.text = [NSString stringWithFormat:@"%@",address1];
             _viewLoading.hidden=YES;
         }
     }];
   

}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - UITextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
   
    activeField = textField;
    [(ACFloatingTextField *)textField textFieldDidBeginEditing];
    [UIView animateWithDuration:0.75 animations:^{
        self.view_scroll.contentOffset = CGPointMake(self.view_scroll.frame.origin.x, textField.frame.origin.y - textField.frame.origin.y / 2);
    }];
    
}


#pragma mark - TextField Action
- (IBAction)textFieldChanged:(ACFloatingTextField *)textField
{
   
    if (textField.tag==3)
    {
        if (_txt_landmark.text.length>0)
        {
            [self hasNoError:textField];
        }
        else
        {
           [self hasError:textField];
        }
    }
  
    if (textField.tag == 6)
    {
        if(_txt_desc.text.length>0)
        {
            [self hasNoError:textField];
        }
        else
        {
            [self hasError:textField];
        }
    }
  
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    [(ACFloatingTextField *)textField textFieldDidEndEditing];
    [UIView animateWithDuration:0.75 animations:^{
        self.view_scroll.contentOffset = CGPointZero;
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [activeField resignFirstResponder];
    return YES;
}

-(void)hasError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
    [textfield setBtmLineColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
    [textfield setBtmLineSelectionColor:[UIColor colorWithRed:225.0/255.0 green:9.0/255.0 blue:29.0/255.0 alpha:1.0]];
}

-(void)hasNoError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0]];
    textfield.btmLineColor = [UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0];
    textfield.btmLineSelectionColor = [UIColor colorWithRed:50.0/255.0 green:171.0/255.0 blue:58.0/255.0 alpha:1.0];
}
#pragma mark - UIGestureRecognizer Handler Method
-(void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.txt_street resignFirstResponder];
    [self.txt_flat resignFirstResponder];
    [self.txt_landmark resignFirstResponder];
    [self.txt_city resignFirstResponder];
    [self.txt_zipcode resignFirstResponder];
    [self.txt_desc resignFirstResponder];
}

#pragma mark - Retrive Data From Database
-(void)retriveData
{
    NSString *str = [NSString stringWithFormat:@"select * from CartQTY"];
    SQLFile *new =[[SQLFile alloc]init];
    arr_cart = [new select_cart:str];
    if (arr_cart.count == 0)
    {
        [self showAlertView:@"Sorry" message:@"Ther is no data in cart" cancelButtonTitle:@"OK"];
    }
    
    
    description  =[NSString stringWithFormat:@"{\"Order\": [{\"ItemId\": \"%@\", \"ItemQty\": \"%@\",\"ItemAmt\": \"%.2f\"},",[[arr_cart valueForKey:@"foodId"] objectAtIndex:0],[[arr_cart valueForKey:@"foodQty"] objectAtIndex:0],[[[arr_cart valueForKey:@"foodPrice"] objectAtIndex:0] floatValue]];
    for (int i = 0; i<[arr_cart count]; i++)
    {
        description =  [description stringByAppendingString:[NSString stringWithFormat:@"{\"ItemId\": \"%@\", \"ItemQty\": \"%@\",\"ItemAmt\": \"%.2f\"}%@",[[arr_cart valueForKey:@"foodId"] objectAtIndex:i], [[arr_cart valueForKey:@"foodQty"] objectAtIndex:i], [[[arr_cart valueForKey:@"foodPrice"] objectAtIndex:i] floatValue],(arr_cart.count-1 == i?@"":@",")]];
 
    }
    
    description =[description stringByAppendingString:@"]}"];
    NSLog(@"arrayOrder %@",description);
//    description = [description stringByReplacingOccurrencesOfString:@"\\" withString:@""];
   
    arrTotalPrice = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<arr_cart.count; i++)
    {
        float totalEach = [[[arr_cart valueForKey:@"foodQty"] objectAtIndex:i] floatValue] * [[[arr_cart valueForKey:@"foodPrice"] objectAtIndex:i] floatValue];
        [arrTotalPrice addObject:[NSString stringWithFormat:@"%.2f", totalEach]];
    }
    self.totalPayAmount = [arrTotalPrice valueForKeyPath:@"@sum.self"];
}


#pragma mark - Goto Next UIViewController
-(void)gotoNextViewcontroller
{
    OrderSucess *order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderSucess"];
    order.totalPay = self.totalPay;
    order.restDelTime = self.restDelTime;
    order.restName = self.restName;
    order.address = [NSString stringWithFormat:@"%@-%@,%@,%@-%@",self.txt_street.text, self.txt_flat.text, self.txt_landmark.text,_txt_city.text,_txt_zipcode.text];
//    [self presentViewController:order animated:YES completion:nil];
    [self.navigationController pushViewController:order animated:YES];
}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Post METHOD / Post Data from webservice
#pragma mark - POST Methods
-(void)POST_Order
{
    
       
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
  
    [parameters setValue:userID forKey:@"user_id"];
    [parameters setValue:[[arr_cart valueForKey:@"restId"] objectAtIndex:0] forKey:@"res_id"];
    [parameters setValue:_txt_landmark.text forKey:@"address"];
    [parameters setValue:[NSString stringWithFormat:@"%f",latitude1] forKey:@"lat"];
    [parameters setValue:[NSString stringWithFormat:@"%f",longitude1] forKey:@"long"];
    [parameters setValue:description forKey:@"food_desc"];
    [parameters setValue:self.txt_desc.text forKey:@"notes"];
    [parameters setValue:[NSString stringWithFormat:@"%.2f",[self.totalPayAmount floatValue]] forKey:@"total_price"];
    
    NSLog(@"PARAM : %@",parameters);
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
  
    
    //Assuming data is not nil we add this to the multipart form
    
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@bookorder.php?",SERVERURL]]];
    
    _connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection1 start];
        
    
}
//-(void)retriveJSON:(NSString *)url
//{
//    //Checks internet connection
//    if (![self connected])
//    {
//        //handle if Internet connection is not found
//        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
//        [self.view addSubview:snackbar];
//    }
//    else
//    {
//        NSString *urlString = url;
//        NSLog(@"URL ::: %@",urlString);
//        urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//        NSMutableURLRequest *request = nil;
//        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//        [request setTimeoutInterval:30.0f];
//
//        self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        [self.connection1 start];
//    }
//}

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _viewLoading.hidden = YES;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == _connection1)
    {
    NSError *error;
    if (_receivedData1 != nil)
    {

        NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"JSON Result :: %@",jsonResult);
        if ([[jsonResult valueForKey:@"status"] isEqualToString:@"Failed"])
        {
//            [self showAlertView:@"Failed" message:@"Please try after some time" cancelButtonTitle:@"OK"];
            
        }
        else
        {
            NSString *passingStr= @"Delete from CartQTY";
            SQLFile *sql = [SQLFile new];
            if ([sql operationdb:passingStr])
            {
                NSLog(@"Delete");
            }
            else
            {
                NSLog(@"Not Delete");
            }
            arr_cart = nil;

           
            
            
            NSLog(@"CheckMyOrder : %@ ",[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"order_amount"]);
          
            
            
            NSMutableDictionary *userDic =[[NSMutableDictionary alloc] init];
            [userDic setObject:[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"order_id"] forKey:@"oder_id"];
            [userDic setObject:[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"restaurant_name"] forKey:@"rest_name"];
            [userDic setObject:[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"restaurant_address"] forKey:@"rest_address"];
            [userDic setObject:[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"order_amount"] forKey:@"totle_price"];
            [userDic setObject:foodCurrency1 forKey:@"foodCurrency"] ;
            [userDic setObject:[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"order_date"] forKey:@"order_date"];
            
            NSMutableArray *arrOrder = [[[NSUserDefaults standardUserDefaults] objectForKey:@"MyOrder"] mutableCopy];
            [arrOrder addObject:userDic];
            [[NSUserDefaults standardUserDefaults] setObject:arrOrder forKey:@"MyOrder"];
            
            SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
            [alert addButton:@"OK" actionBlock:^{
                //                [self gotoNextViewcontroller];
               
                OrderStatus *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderStatus"];
                detail.orderId=[[[jsonResult valueForKey:@"order_details"]objectAtIndex:0] valueForKey:@"order_id"];
                [self.navigationController pushViewController:detail animated:YES];
            }];
            [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:@"Success" subTitle:[jsonResult valueForKey:@"success"] closeButtonTitle:nil duration:0.0];
            
        }
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
    }
    _viewLoading.hidden = YES;
    }
}

@end
