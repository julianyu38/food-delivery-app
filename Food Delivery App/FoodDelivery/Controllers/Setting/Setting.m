//
//  Setting.m
//  Hospital
//
//  Created by Redixbit on 10/08/16.
//  Copyright (c) 2016 Redixbit. All rights reserved.
//

#import "Setting.h"
#import "SeachByMap.h"
#import "MostRatedRrequsturant.h"

@interface Setting ()
{
    NSString *language;
    NSString *RowZero;
}
@end

@implementation Setting
@synthesize lat_str,long_str,speciality_id;

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(BOOL)prefersStatusBarHidden
{
    if(iPhoneVersion == 10)
        return NO;
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    language = [[NSLocale preferredLanguages] firstObject];

    language=[language substringToIndex:character];
    [self setFont];
    _view_loading.hidden=YES;
    
    
      _lblTitle.text = NSLocalizedString(@"lblTitle", @"");
      lblRadius.text = NSLocalizedString(@"lblRadius", @"");
      lblDistance.text = NSLocalizedString(@"lblDistance", @"");
      _lblkm.text = NSLocalizedString(@"lblkm", @"");
      Lbl_Radius.text = NSLocalizedString(@"Lbl_Radius", @"");
      Lbl_currentLocation.text = NSLocalizedString(@"Lbl_currentLocation", @"");
    if ([language isEqualToString:country]) {
        
        NSLog(@"CheRtl2");
        currentCity_lbl.textAlignment = NSTextAlignmentLeft;
         [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
    }
    else
    {
          NSLog(@"CheRtl");
        currentCity_lbl.textAlignment = NSTextAlignmentRight;
    }
    
    [self set_radius];
//    [self Set_Language];
    
    app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    receivedData1 = [NSMutableData new];
    
    UIImage *thumbImage;
    if(self.view.frame.size.width>=768)
    {
        thumbImage = [UIImage imageNamed:@"iPad_level_btn.png"];
    }
    else
    {
        thumbImage = [UIImage imageNamed:@"level_btn.png"];
    }
    thumbImage = [thumbImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [radius_slider setTintColor:[UIColor blackColor]];
    [radius_slider setThumbImage:thumbImage forState:UIControlStateNormal];
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"fill_scroll.png"];
    sliderLeftTrackImage = [sliderLeftTrackImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [radius_slider setTintColor:[UIColor blackColor]];
    sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    
    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"scroll_bg.png"];
   
    sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    
    [radius_slider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [radius_slider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
    
    
    
    city_arr=[[NSMutableArray alloc]init];
//    cityId_arr=[[NSMutableArray alloc]init];
 
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderBy"] isEqualToString:@"ASC"])
    {
        [order_switchbtn setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
        order_switchbtn.tag=11;
        order_lbl.text=NSLocalizedString(@"Ascending", @"");
    }
    else
    {
        [order_switchbtn setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
        order_switchbtn.tag=111;
         order_lbl.text=NSLocalizedString(@"Descending", @"");
    }

    radius_slider.value=[[[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"]floatValue];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"]);
    radiusValue_lbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"];
    
    order_imgview.layer.cornerRadius=img_radius;
    order_imgview.clipsToBounds=YES;
    
    
}
-(void)setFont
{
    _lblTitle.font = [UIFont fontWithName:RFontRegular size:_lblTitle.font.pointSize];
    lblRadius.font = [UIFont fontWithName:RFontRegular size:lblRadius.font.pointSize];
    lblDistance.font = [UIFont fontWithName:RFontRegular size:lblDistance.font.pointSize];
    radiusValue_lbl.font = [UIFont fontWithName:RFontRegular size:radiusValue_lbl.font.pointSize];
    _lblkm.font = [UIFont fontWithName:RFontRegular size:_lblkm.font.pointSize];
    Lbl_Radius.font = [UIFont fontWithName:RFontRegular size:Lbl_Radius.font.pointSize];
        Lbl_currentLocation.font = [UIFont fontWithName:RFontRegular size:Lbl_currentLocation.font.pointSize];
    currentCity_lbl.font = [UIFont fontWithName:RFontRegular size:currentCity_lbl.font.pointSize];
    
    
    lblRadius.textColor =greaySetting;
    lblDistance.textColor =greaySetting;
    radiusValue_lbl.textColor =greaySetting;
    _lblkm.textColor =greaySetting;
    Lbl_Radius.textColor =greaySetting;
    Lbl_currentLocation.textColor =greaySetting;
    currentCity_lbl.textColor =greaySetting;
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetData:@"restaurant_city.php"];
    _view_loading.hidden =NO;

    radius_slider.enabled=NO;
    [btnRedius setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
   
    NSString *strOnOff = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"OnOff"];
   
    if ([strOnOff isEqualToString:@"off"]) {
        radius_slider.enabled=NO;
        [btnRedius setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
         [self OffAlpa];
    }
    else if ([strOnOff isEqualToString:@"on"])
    {
        radius_slider.enabled=YES;
        [btnRedius setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
         [self OnAlpa];
    }
    else
    {
        radius_slider.enabled=NO;
        [btnRedius setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
        [self OffAlpa];
    }
    
    
    
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Retirve Data From Webservice
-(void)GetData:(NSString *)url
{
    if(![self connected])
    {
        
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        NSLog(@"URL :: %@",url);
        NSString *StringURL = [NSString stringWithFormat:@"%@%@",SERVERURL, url];
        StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [connection start];
    }
}

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    _view_loading.hidden = NO;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (receivedData1 != nil)
    {
        NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"json : %@, Error :: %@",resultDic,error);
        if (error == nil)
        {
            
          
                city_arr=[[resultDic objectForKey:@"city"]valueForKey:@"city_name"];
            
            
           
          
//            cityId_arr=[[[resultDic valueForKey:@"Cities"] objectAtIndex:0]valueForKey:@"id"];
            [city_table reloadData];
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No Record Available!" duration:3.0];
            [self.view addSubview:snackbar];
           
        }
    }
    else
    {
        NSLog(@"Data not found");
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server" duration:3.0];
        [self.view addSubview:snackbar];
    }
    _view_loading.hidden = YES;
}

-(void)set_radius
{
  
    if(iPhoneVersion==4 || iPhoneVersion==5)
    {
        img_radius=6;
        table_radius=8;
    }
    if(iPhoneVersion==6)
    {
        img_radius=8;
        table_radius=10;
    }
    else if(iPhoneVersion==61)
    {
        img_radius=10;
        table_radius=12;
    }
    else
    {
        img_radius=16;
        table_radius=14;
    }
}

#pragma mark - UITableView Datasource and Delegte Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [city_arr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CityCell"];
    }
   

    UILabel *city=(UILabel *)[cell viewWithTag:102];
    
    city.text=[city_arr objectAtIndex:indexPath.row];
    UIImageView *selected = (UIImageView *)[cell.contentView viewWithTag:1024];
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"City_Name"]length]>0)
    {
        currentCity_lbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"City_Name"];
        NSLog(@"Checksd currentCity_lbl123123%@",currentCity_lbl.text);
    }
    else
    {
        if ([city_arr objectAtIndex:0]) {
            currentCity_lbl.text=[city_arr objectAtIndex:0];
            RowZero = [city_arr objectAtIndex:0];
            NSLog(@"RowZero : %@",RowZero);
            [[NSUserDefaults standardUserDefaults]setObject:RowZero forKey:@"selectClick"];
        }
    }
    
    NSString *selectCity = [[NSUserDefaults standardUserDefaults] stringForKey:@"selectClick"];
    if ([city.text isEqualToString:selectCity])
    {
        selected.image = [UIImage imageNamed:@"righticon.png"];
    }
    else
    {
        selected.image = [UIImage imageNamed:@""];
    }
   
    city.font = [UIFont fontWithName:RFontRegular size:city.font.pointSize];
     city.textColor =greaySetting;
    cell.backgroundColor = [UIColor whiteColor];

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentCity_lbl.text=[city_arr objectAtIndex:indexPath.row];
    strCity = [NSString stringWithFormat:@"%@",[city_arr objectAtIndex:indexPath.row]];
    [[NSUserDefaults standardUserDefaults]setObject:strCity forKey:@"selectClick"];
//    [[NSUserDefaults standardUserDefaults]setObject:[cityId_arr objectAtIndex:indexPath.row] forKey:@"City"];
    [[NSUserDefaults standardUserDefaults]setObject:[city_arr objectAtIndex:indexPath.row] forKey:@"City_Name"];
    [[NSUserDefaults standardUserDefaults]synchronize];
     [city_table reloadData];

}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Force your tableview margins (this may be a bad idea)
    if ([city_table respondsToSelector:@selector(setSeparatorInset:)]) {
        [city_table setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([city_table respondsToSelector:@selector(setLayoutMargins:)]) {
        [city_table setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}
#pragma mark Locaton Delegate Method
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        long_str = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        lat_str = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [self getAddressFromLatLon:newLocation];
        [locationManager stopUpdatingLocation];
        locationManager = nil;
    }
}
- (void) getAddressFromLatLon:(CLLocation *)bestLocation
{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:bestLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"Checksd currentCity_lbl%@",currentCity_lbl.text);
     }];
}

#pragma mark Slider Method
- (IBAction)sliderValueChanged:(UISlider *)sender
{
    
    radius_slider.value=sender.value;
    radiusValue_lbl.text=[NSString stringWithFormat:@"%.0f",radius_slider.value];
    [[NSUserDefaults standardUserDefaults]setObject:radiusValue_lbl.text forKey:@"Radius"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
#pragma mark Switch Method

- (IBAction)RediusChangeSwitch:(UIButton *)sender {
    if([sender tag]==33)
    {
       
        sender.tag=333;
        NSLog(@"Switch is OFF");
        NSString *Redius =@"100000";
        [[NSUserDefaults standardUserDefaults]setObject:Redius forKey:@"Radius1"];
         
        radius_slider.enabled=NO;
        [btnRedius setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"OnOff"];
         [self OffAlpa];

    }
    else
    {
        sender.tag=33;
        NSLog(@"Switch is ON");
        radius_slider.enabled=YES;
        [btnRedius setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"OnOff"];
        [self OnAlpa];
    }
}

- (IBAction)changeSwitch:(UIButton *)sender
{
    if([sender tag]==111)
    {
        sender.tag=11;
        NSLog(@"Switch is ON");
        order_lbl.text=NSLocalizedString(@"Ascending", @"");
        [[NSUserDefaults standardUserDefaults]setObject:@"ASC" forKey:@"OrderBy"];
        [order_switchbtn setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
    }
    else
    {
        sender.tag=111;
        order_lbl.text=NSLocalizedString(@"Descending", @"");
        [[NSUserDefaults standardUserDefaults]setObject:@"DES" forKey:@"OrderBy"];
        [order_switchbtn setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
        NSLog(@"Switch is OFF");
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
}
#pragma mark extra metthod
-(void)OffAlpa
{
    lblDistance.alpha=0.3;
    radiusValue_lbl.alpha=0.3;
    self.lblkm.alpha=0.3;
     Lbl_Radius.alpha=0.3;
    radius_imgview.alpha = 0.3;
}
-(void)OnAlpa
{
    lblDistance.alpha=1;
    radiusValue_lbl.alpha=1;
    self.lblkm.alpha=1;
    Lbl_Radius.alpha=1;
    radius_imgview.alpha = 1;
}
-(void)Set_Language
{
    title_lbl.text=NSLocalizedString(@"Setting_title", @"");
    Lbl_currentLocation.text=NSLocalizedString(@"Setting_CurrentCity", @"");
    Lbl_Radius.text=NSLocalizedString(@"Setting_Radius", @"");
    Lbl_orderby.text=NSLocalizedString(@"Setting_Order", @"");
    lblRadius.text=NSLocalizedString(@"Setting_Redius", @"");
}
-(IBAction)Btn_Back:(id)sender
{
    if ([_strCheckScreen isEqualToString:@"home"]) {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
        }
    }
    else if ([_strCheckScreen isEqualToString:@"map"])
    {
      
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            SeachByMap *List = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachByMap"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            SeachByMap *List = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachByMap"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
        }
    }
    else if ([_strCheckScreen isEqualToString:@"mostrated"])
    {
        
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            MostRatedRrequsturant *List = [self.storyboard instantiateViewControllerWithIdentifier:@"MostRatedRrequsturant"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            MostRatedRrequsturant *List = [self.storyboard instantiateViewControllerWithIdentifier:@"MostRatedRrequsturant"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
        }
    }
}
@end
