//
//  LocateMe.m
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "LocateMe.h"
#import "List.h"
#import "Constants.h"
@interface LocateMe ()
{
        NSString *language;
}

@end

@implementation LocateMe

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    viewLoading.hidden = YES;
    [self set_Language];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
            [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
            _imageBack.image=[_imageBack.image imageFlippedForRightToLeftLayoutDirection];
            _imageLocate.image=[_imageLocate.image imageFlippedForRightToLeftLayoutDirection];
            _imageAdd.image=[_imageAdd.image imageFlippedForRightToLeftLayoutDirection];
    }
    else
    {
        NSLog(@"en");
    }
    
    
}

#pragma mark - Button Action
//Back
-(IBAction)btnBack:(id)sender
{
    List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
    CATransition* transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:List animated:YES];
}

//Locate Me
-(IBAction)btn_locate:(id)sender
{
    [self.view addSubview:viewLoading];
    viewLoading.hidden = NO;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}
//Manually Locate
- (IBAction)btn_manually:(UIButton *)sender
{
    ManuallySearch *manually = [self.storyboard instantiateViewControllerWithIdentifier:@"ManuallySearch"];
    CATransition *transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self presentViewController:manually animated:YES completion:nil];
    [self.navigationController pushViewController:manually animated:YES];
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to get your location." duration:3.0];
    [self.view addSubview:snackbar];
//    [self showAlertView:@"Error" message:@"Failed to Get Your Location" cancelButtonTitle:@"OK"];
    viewLoading.hidden = YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        NSString *longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString *latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [locationManager stopUpdatingLocation];
        
        CLGeocoder* geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             CLPlacemark* placemark;
             NSString *address = nil;
             if (error == nil && [placemarks count] > 0)
             {
//                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                 address=placemark.locality;
                 
                 placemark = [placemarks lastObject];
                 address = [NSString stringWithFormat:@"%@",placemark.locality];
//                 address = [NSString stringWithFormat:@"%@, %@", placemark.name, placemark.locality];
                 NSLog(@"Address :: %@",address);
                 NSMutableDictionary *location = [[NSMutableDictionary alloc] init];
                 [location setValue:latitude forKey:@"latitude"];
                 [location setValue:longitude forKey:@"longitude"];
                 [location setValue:address forKeyPath:@"address"];
                 [location setValue:placemark.postalCode forKeyPath:@"zipcode"];
                 NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                 [userDefault setObject:location forKey:@"location"];
                 [userDefault synchronize];
                 NSLog(@"Default :: %@",[userDefault valueForKey:@"location"]);
                 viewLoading.hidden = YES;
             }
         }];
        locationManager = nil;
    }
}

@end
