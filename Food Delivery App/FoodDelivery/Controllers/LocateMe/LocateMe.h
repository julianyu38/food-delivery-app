//
//  LocateMe.h
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ManuallySearch.h"
#import "Reachability.h"

@interface LocateMe : UIViewController<CLLocationManagerDelegate>
{
    IBOutlet UIView *viewLoading;
    CLLocationManager *locationManager;
    NSString *lat,*lon;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btn_manually:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageBack;
@property (weak, nonatomic) IBOutlet UIImageView *imageLocate;
@property (weak, nonatomic) IBOutlet UIImageView *imageAdd;

@end
