//
//  MostRatedRrequsturant.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 26/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "MostRatedRrequsturant.h"
#import "Snackbar.h"
#import "Reachability.h"
#import "Constants.h"
#import "Color.h"
#import "Setting.h"
#import "Detail.h"
#import "List.h"
@interface MostRatedRrequsturant ()<WebServiceHelperDelegate>
{
    int startIndex;
    int pageRow;
    NSString *strCity;
    NSString *NotLoad;
    NSMutableArray *city_arr;
    NSString *language;
    NSString *strRadius,*localTimeZone;
    
  
}

@end

@implementation MostRatedRrequsturant

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_Language];

    self.arrData = [[NSMutableArray alloc]init];
    [_arrData removeAllObjects];
    [self.view addSubview:_view_loading];
    _view_loading.hidden = NO;
    startIndex = listRow;
    pageRow = 1;
    
    //For Radius
    
    NSString *strOnOff = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"OnOff"];
    if ([strOnOff isEqualToString:@"off"]) {
        
        strRadius = @"100000";
    }
    else if ([strOnOff isEqualToString:@"on"]) {
        
        strRadius = [[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"];
        
    }
    else
    {
        strRadius = @"100000";
    }
    
    //For City
    strCity = [[NSUserDefaults standardUserDefaults]objectForKey:@"City_Name"];
    if (strCity.length > 0) {
        NSLog(@"CITY FOUND : %@",strCity);
//        lbl_title.text = strCity;
    }else{
        [self GetData:@"restaurant_city.php"];
    }
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"location"]);
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    localTimeZone = [timeZone name];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
   
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [locationManager requestWhenInUseAuthorization];
    }
    _tableView.dataSource =self;
    _tableView.delegate =self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
     [locationManager startUpdatingLocation];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [_arrData removeAllObjects];
    NSLog(@"Location Failed");
    [self rest];
    NSLog(@"didFailWithError: %@", error);
    [self showAlertView:@"Error" message:@"Failed to Get Your Location. Please Try Manually" cancel:@"OK"];
    _view_loading.hidden = YES;
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        _Ns_Longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _Ns_Lattitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [locationManager stopUpdatingLocation];
        
        //Get Address From Latitude and Longitude
        CLGeocoder* geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             CLPlacemark* placemark;
             NSString *address= nil;
             if (error == nil && [placemarks count] > 0)
             {
                 _view_loading.hidden = YES;
                 [self.arrData removeAllObjects];
                 [self rest];
             }
         }];
        locationManager = nil;
    
    
    }
}
#pragma mark - SET LANGUAGE
-(void)set_Language
{
    _lblTitle.text = NSLocalizedString(@"lblMRR", @"");

    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        ;
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        
        
    }
    else
    {
        NSLog(@"en");
    }
    
}
- (IBAction)didClickBackButton:(id)sender
{
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }
   
    
}
- (IBAction)didClickSettingButton:(id)sender {
    Setting *locate = [self.storyboard instantiateViewControllerWithIdentifier:@"Setting"];
    locate.strCheckScreen=@"mostrated";
    
    [self.navigationController pushViewController:locate animated:YES];
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Restaurant Fetching
-(void)rest
{
    [self.view addSubview:_view_loading];
    _view_loading.hidden = NO;
    //
    NSString *urlString;
    NSLog(@"CheckCsfh %@",strCity);
    
        strCity = [strCity stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
        urlString = [NSString stringWithFormat:@"%@restaurant_byrate.php?timezone=%@&lat=%@&lon=%@&location=%@&noofrecords=%d&pageno=%d&radius=%@&short_by=ratting",SERVERURL,localTimeZone,_Ns_Lattitude,_Ns_Longitude,strCity,startIndex,pageRow,strRadius];
    
    NSLog(@"UrlString %@",urlString);
    WebServiceHelper  *obj_add = [WebServiceHelper new];
    
    [obj_add setCurrentCall:urlString];
    [obj_add setMethodName:urlString];
    [obj_add setMethodResult:@"Restaurant_list"];
    [obj_add setDelegate:self];
    [obj_add initiateConnection];
}
-(void)WebServiceHelper:(WebServiceHelper *)editor didFinishWithResult:(BOOL)result
{
    if (result)
    {
        NSMutableArray *resultDic = [[editor ReturnStr] JSONValue];
        [self getSortedArray:resultDic];
    }
    else
    {
        _view_loading.hidden = YES;
     
    }
}
-(void)getSortedArray:(NSMutableArray *)resultDic
{
    NSString *status = [[resultDic objectAtIndex:0] valueForKey:@"status"];
    //    [[NSUserDefaults standardUserDefaults] setObject:status forKey:@"Status"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([status isEqualToString:@"Success"]) {
        isSuccess = true;
        NSMutableArray *TempArray = [[resultDic objectAtIndex:0] valueForKey:@"Restaurant_list"];
        if (![TempArray count] > 0) {
            _view_loading.hidden = YES;
            //        [self CallAlertView:NSLocalizedString(@"Error Alert Title",@"") second:NSLocalizedString(@"Error Alert SubTitle",@"") third:NSLocalizedString(@"Error Alert closeButtonTitle",@"")];
            [self.tableView reloadData];
            
        }else{
            
            NSLog(@"TempArray %lul",(unsigned long)[TempArray count]);
            for (int i=0; i<[TempArray count]; i++) {
                NSMutableDictionary *temp = [TempArray objectAtIndex:i];
                NSLog(@"temp %@",temp);
                [self.arrData addObject:temp];
            }
            NSLog(@"Check %lu",(unsigned long)[self.arrData count]);
            
            if ([TempArray count]<listRow) {
                self.tableView.tableFooterView.hidden = YES;
            }else{
                //            [self addLoadMoreButton];
                NSLog(@"CheckLoadMore");
                //            [self buttonLoadMoreTouched];
            }
            
            _view_loading.hidden = YES;
            [self.tableView reloadData];
        }
    }
    else
    {
        NotLoad  =@"NotLoad";
        _view_loading.hidden = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:[[resultDic objectAtIndex:0] valueForKey:@"error"]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
    }
}
#pragma mark - Retirve Data From Webservice
-(void)GetData:(NSString *)url
{
    if(![self connected])
    {
        
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        NSLog(@"URL :: %@",url);
        NSString *StringURL = [NSString stringWithFormat:@"%@%@",SERVERURL, url];
        StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
        
        connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [connection1 start];
    }
}


#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
  
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
   
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"json : %@, Error :: %@",resultDic,error);
            if (error == nil)
            {
                
                city_arr=[[NSMutableArray alloc]init];
                city_arr=[[[resultDic objectForKey:@"city"]valueForKey:@"city_name"]objectAtIndex:0];
                
                strCity =[[[resultDic objectForKey:@"city"]valueForKey:@"city_name"]objectAtIndex:0];
//                lbl_title.text = strCity;
                [[NSUserDefaults standardUserDefaults]setObject:strCity forKey:@"selectClick"];
                
                [[NSUserDefaults standardUserDefaults]setObject:strCity forKey:@"City_Name"];
                
                
                NSLog(@"CjeCity %@ :",city_arr);
                
                //            cityId_arr=[[[resultDic valueForKey:@"Cities"] objectAtIndex:0]valueForKey:@"id"];
                
            }
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No Record Available!" duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            NSLog(@"Data not found");
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server" duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    
    _view_loading.hidden = YES;
}
#pragma mark- LazyLoad Table View
- (void)tableView:(UITableView *)tableView lazyLoadNextCursor:(int)cursor{
    //for instance here you can execute webservice request lo load more data
    NSLog(@"NotLoad %@",NotLoad);
    if ([NotLoad isEqualToString:@"NotLoad"]) {
        
    }
    else
    {
        [self buttonLoadMoreTouched];
        [self.tableView reloadData];
    }
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *cell_image = (UIImageView *)[cell.contentView viewWithTag:101];
    if (indexPath.row % 2 == 0)
    {
        cell_image.image = [UIImage imageNamed:@"first_cell.png"];
    }
    else
    {
        cell_image.image = [UIImage imageNamed:@"secound_cell.png"];
    }
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        cell_image.image=[cell_image.image imageFlippedForRightToLeftLayoutDirection];
    }
    else
    {
        NSLog(@"en");
    }
    
    NSDictionary *dic = [_arrData objectAtIndex:indexPath.row];
    
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        // This is the last cell
        NSLog(@"Scrolling");
    }
    UIImageView *rest_image = (UIImageView *)[cell.contentView viewWithTag:102];
    NSString *Str_image_name = [NSString stringWithFormat:@"%@%@",IMAGEURL,[dic valueForKey:@"image"]];
    Str_image_name = [Str_image_name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [rest_image sd_setImageWithURL:[NSURL URLWithString:Str_image_name] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
        [rest_image updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [rest_image reveal];
    }];
    
    rest_image.layer.cornerRadius = 5.0f;
    rest_image.clipsToBounds = YES;
    UILabel *rest_name = (UILabel *)[cell.contentView viewWithTag:103];
    [rest_name setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]]];
    
    UILabel *restCat = (UILabel *)[cell.contentView viewWithTag:104];
    NSLog(@"CheckCate123 %@",[[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row]objectAtIndex:0]);
    
    NSLog(@"CheckCate %@",[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row]);
    NSString *restCategoryNames = [[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] objectAtIndex:0];
    for (int i = 1; i<[[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] count]; i++)
    {
        restCategoryNames = [restCategoryNames stringByAppendingString:[NSString stringWithFormat:@", %@",[[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] objectAtIndex:i]]];
    }
    [restCat setText:restCategoryNames];
    
    UILabel *min = (UILabel *)[cell.contentView viewWithTag:56];
    UILabel *rating = (UILabel *)[cell.contentView viewWithTag:65];

    min.text = NSLocalizedString(@"lblMinute", @"");
    rating.text = NSLocalizedString(@"lblRatings", @"");
    
    UILabel *rest_open = (UILabel *)[cell.contentView viewWithTag:105];
    NSDateFormatter *formateDate = [[NSDateFormatter alloc]init];
    formateDate.dateFormat = @"HH:mm:ss";
    NSString *Res_Status = [dic valueForKey:@"res_status"];
    if ([Res_Status isEqualToString:@"open"])
    {
        NSString *str = [dic valueForKey:@"close_time"];
        formateDate.dateFormat = @"hh:mm a";
        [rest_open setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"open", @""),str]];
        rest_open.textColor = [Color colorFromHexString:@"7CB66A"];
    }
    else
    {
        [rest_open setTextColor:[UIColor redColor]];
        NSString *str = [dic valueForKey:@"open_time"];
        formateDate.dateFormat = @"hh:mm a";
        [rest_open setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"close", @""),str]];
    }
    
    UILabel *rest_time = (UILabel *)[cell.contentView viewWithTag:106];
    [rest_time setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"delivery_time"]]];
    
    UILabel *rest_rate = (UILabel *)[cell.contentView viewWithTag:107];
    [rest_rate setText:[NSString stringWithFormat:@"%.1f",[[dic valueForKey:@"ratting"] floatValue]]];
    
  cell.backgroundColor=[UIColor clearColor];
    dic = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Detail *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    //    detail.dicRestDetail = [jsonResult objectAtIndex:indexPath.row];
    detail.restaurantID = [[_arrData objectAtIndex:indexPath.row] valueForKey:@"id"];
    detail.lat =[[_arrData objectAtIndex:indexPath.row] valueForKey:@"lat"];
    detail.lon =[[_arrData objectAtIndex:indexPath.row] valueForKey:@"lon"];

    detail.distance =[[_arrData objectAtIndex:indexPath.row] valueForKey:@"distance"];

    detail.strScreen= @"mostRated";
    CATransition* transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark - Goto Detail View
-(void)gotoDetailsView:(UIViewController *)next
{
    CATransition *transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    transition.type = kCATransitionFade;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    next.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    next.view.center = touchPoint;
    [UIView animateWithDuration:0.5f animations:^{
        next.view.transform = CGAffineTransformIdentity;
        next.view.center = self.view.center;
    } completion:nil];
    
    [self.navigationController pushViewController:next animated:NO];
}
#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancel:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}
-(void)buttonLoadMoreTouched
{
    pageRow=pageRow+1;
    [self rest];
}
@end
