//
//  MostRatedRrequsturant.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 26/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKMapView+ARHelpers.h"
#import "L3SDKLazyTableView.h"

@interface MostRatedRrequsturant : UIViewController<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,L3SDKLazyTableViewDelegate>
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
    CGPoint touchPoint;
    CLLocationManager *locationManager;
    BOOL isSuccess;

}
@property (weak, nonatomic) IBOutlet UIView *view_loading;
@property (strong, nonatomic) IBOutlet NSMutableArray *arrData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) NSString *Ns_Lattitude;
@property (strong, nonatomic) NSString *Ns_Longitude;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
