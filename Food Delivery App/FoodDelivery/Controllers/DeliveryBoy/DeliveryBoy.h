//
//  DeliveryBoy.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface DeliveryBoy : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
    NSMutableData *receivedData3;
    NSURLConnection *connection3;
    
}
@property (retain, nonatomic) NSURLConnection *connection2;
@property (retain, nonatomic) NSMutableData *receivedData2;

//UILabel
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryBoy;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryBoy1;
@property (weak, nonatomic) IBOutlet UILabel *lblSignOut;
@property (weak, nonatomic) IBOutlet UILabel *lblSetYourPresnce;

//UIButton
@property (weak, nonatomic) IBOutlet UIButton *btnOrderHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnMyProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnSwitch;

//UIView
@property (weak, nonatomic) IBOutlet UIView *view_loading;

//UITableView
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
