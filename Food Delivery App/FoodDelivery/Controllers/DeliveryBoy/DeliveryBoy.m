//
//  DeliveryBoy.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "DeliveryBoy.h"
#import "Constants.h"
#import "Snackbar.h"
#import "Reachability.h"
#import "MyProfile.h"
#import "OrderDetail.h"
#import "OrderHistory.h"
#import "List.h"
@interface DeliveryBoy ()
{
   
    NSString *language;
    NSMutableDictionary *dicDeliveryProfile;
    NSMutableArray *arrAssigned;
    NSString *InstanceID;
    NSString *userID;

}
@end

@implementation DeliveryBoy

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     dicDeliveryProfile = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];
    [self setFont];
     [self set_Language];
   
    
    _tableView.delegate =self;
    _tableView.dataSource =self;
    _tableView.tableFooterView=[UIView new];
   
    
    [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
    
    NSString *strOnOff = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"OnOff"];
    
    if ([strOnOff isEqualToString:@"off"]) {
        [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
    }
    else if ([strOnOff isEqualToString:@"on"])
    {
           [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    InstanceID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"firebaseId"];
    userID = [[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0];
    [self GetData];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self retriveJSON:[NSString stringWithFormat:@"%@deliveryboy_order.php?deliverboy_id=%@",SERVERURL,[[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0]]flag:@"1"];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setFont
{
    _lblDeliveryBoy.font = [UIFont fontWithName:RFontRegular size:_lblDeliveryBoy.font.pointSize];
     _lblUserName.font = [UIFont fontWithName:RFontSemibold size:_lblUserName.font.pointSize];
     _lblDeliveryBoy1.font = [UIFont fontWithName:RFontSemibold size:_lblDeliveryBoy1.font.pointSize];
     _lblSignOut.font = [UIFont fontWithName:RFontSemibold size:_lblSignOut.font.pointSize];
    _lblSetYourPresnce.font = [UIFont fontWithName:RFontRegular size:_lblSetYourPresnce.font.pointSize];
     _lblMsg.font = [UIFont fontWithName:RFontRegular size:_lblMsg.font.pointSize];
    
    _btnOrderHistory.titleLabel.font =[UIFont fontWithName:RFontSemibold size:_btnOrderHistory.titleLabel.font.pointSize];
    _btnMyProfile.titleLabel.font  =[UIFont fontWithName:RFontSemibold size:_btnMyProfile.titleLabel.font.pointSize];
    
    _lblUserName.text=[[dicDeliveryProfile valueForKey:@"name"]objectAtIndex:0];
    
    
}
#pragma mark - SET LANGUAGE
-(void)set_Language
{
     _lblDeliveryBoy.text = NSLocalizedString(@"lblDeliveryBoy", @"");
    _lblDeliveryBoy1.text = NSLocalizedString(@"lblDeliveryBoy1", @"");
    _lblSignOut.text = NSLocalizedString(@"lblSignOut", @"");
    _lblSetYourPresnce.text = NSLocalizedString(@"lblSetYourPresnce", @"");
   
    [_btnOrderHistory setTitle:NSLocalizedString(@"btnOrderHistory", @"") forState:UIControlStateNormal];
    [_btnMyProfile setTitle:NSLocalizedString(@"btnMyProfile", @"") forState:UIControlStateNormal];
    
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
    
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAssigned.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView  *imgOrder=(UIImageView *) [cell.contentView viewWithTag:101];
    UILabel *lblOrderNo=(UILabel *) [cell.contentView viewWithTag:102];
    UILabel *lblOrderAmt=(UILabel *) [cell.contentView viewWithTag:103];
    UILabel *lblOrderItme=(UILabel *) [cell.contentView viewWithTag:104];
    UILabel *lblDataTime=(UILabel *) [cell.contentView viewWithTag:105];
   
    NSDictionary *dic = [arrAssigned objectAtIndex:indexPath.row];
  
    
    if ([[dic valueForKey:@"status"]isEqualToString:@"Order is Delivered"]) {
        imgOrder.image=[UIImage imageNamed:@"img_ordercomplete.png"];
    }else
    {
         imgOrder.image=[UIImage imageNamed:@"img_orderprocess.png"];
    }
    //set Text
    
    lblOrderNo.text=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"DOrderNo", @""),[dic valueForKey:@"order_no"]];
    lblOrderAmt.text=[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"DOrderAmount", @""),dollar,[dic valueForKey:@"total_amount"]];
    lblOrderItme.text=[NSString stringWithFormat:@"%@ %@",[dic valueForKey:@"items"],NSLocalizedString(@"DItems", @"")];
    lblDataTime.text=[dic valueForKey:@"date"];

    //set Font
     lblOrderNo.font = [UIFont fontWithName:RFontSemibold size:lblOrderNo.font.pointSize];
     lblOrderAmt.font = [UIFont fontWithName:RFontRegular size:lblOrderAmt.font.pointSize];
     lblOrderItme.font = [UIFont fontWithName:RFontRegular size:lblOrderItme.font.pointSize];
     lblDataTime.font = [UIFont fontWithName:RFontRegular size:lblDataTime.font.pointSize];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [arrAssigned objectAtIndex:indexPath.row];

    OrderDetail *next =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetail"];
    next.orderId = [dic valueForKey:@"order_no"];
    next.strStatus=[dic valueForKey:@"status"];
    [self.navigationController pushViewController:next animated:YES];
   
}

#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - POST Methods
-(void)POST_Order:(NSMutableDictionary *)parameters webserviceName:(NSString *)serviceName flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",serviceName]];
        
        NSLog(@"URL : %@",serviceName);
        
        for (__strong NSString *string in parameters.allValues)
        {
            string = [string stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        }
        
        NSLog(@"parameters :: %@",parameters);
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPShouldHandleCookies:NO];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        for (NSString *param in parameters)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //Close off the request with the boundary
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        [request setHTTPBody:body];
        [request setTimeoutInterval:30.0];
        
        
        if ([flag isEqualToString:@"2"])
        {
            _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_connection2 start];
        }
    }
}
#pragma mark - Retirve Data From Webservice
-(void)GetData
{
    NSString *StringURL;
    if (userID != nil && InstanceID !=nil)
    {
    
      StringURL = [NSString stringWithFormat:@"%@token.php?token=%@&type=Iphone&user_id=null&delivery_boyid=%@",SERVERURL,InstanceID,userID];
        NSLog(@"URL :: %@",StringURL);
    }
    
    StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = nil;
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
    
    _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection2 start];
    
}
#pragma mark - Post Data from Server
/*-(void)POST_Order
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
    NSLog(@"TOKEN123 : %@",InstanceID);
    [parameters setValue:InstanceID forKey:@"token"];
    
    [parameters setValue:@"Iphone" forKey:@"type"];
    [parameters setValue:@"null" forKey:@"user_id"];
    [parameters setValue:userID forKey:@"delivery_boyid"];
    
    
    NSLog(@"PARAM : %@",parameters);
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //Assuming data is not nil we add this to the multipart form
    
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@token.php",SERVERURL]]];
    
    connection3 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection3 start];
    
    
}
*/

#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}
#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
    else if (connection == _connection2)
    {
        _receivedData2 = [NSMutableData new];
        [_receivedData2 setLength:0];
    }
    else if (connection == connection3)
    {
        receivedData3 = [NSMutableData new];
        [receivedData3 setLength:0];
    }

    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
    else if(connection == _connection2)
    {
        [_receivedData2 appendData:data];
    }
    else if(connection == connection3)
    {
        [receivedData3 appendData:data];
    }
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                arrAssigned =[[NSMutableArray alloc]init];
                arrAssigned=[json valueForKey:@"order"];
                NSLog(@"arrAssigned %@",arrAssigned);
                [_tableView reloadData];
            }
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:[json valueForKey:@"order"] duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    else if (connection == _connection2)
    {
        if (_receivedData2 != nil)
        {
            
            NSError *error;
            NSMutableDictionary *dictData = [NSJSONSerialization JSONObjectWithData:_receivedData2 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Response1212 : %@",[[dictData objectForKey:@"data"]valueForKey:@"presence"]);
            
            if ([[[dictData objectForKey:@"data"]valueForKey:@"presence"] isEqualToString:@"yes"]) {
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                    
                }];
                NSLog(@"Response1212 : %@",dictData);
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:nil subTitle:[NSString stringWithFormat:@"You are set as available for delivery"] closeButtonTitle:nil duration:0.0];
            }
            else if([[[dictData objectForKey:@"data"]valueForKey:@"presence"] isEqualToString:@"no"])
            {
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                    
                }];
                NSLog(@"Response1212 : %@",dictData);
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:nil subTitle:[NSString stringWithFormat:@"You have set as absent for delivery"] closeButtonTitle:nil duration:0.0];
            }
            
            
        }
    }
    else if (connection == connection3)
    {
        NSError *error;
        if (receivedData3 != nil)
        {
            
            NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:receivedData3 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"JSON Result notification:: %@",jsonResult);
            
        }
        else
        {
            
        }
        
    }
    
    _view_loading.hidden = YES;
}
#pragma mark UIButton ClickEvent
- (IBAction)didClickMyProfileButton:(id)sender {
    MyProfile *next=[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfile"];
    [self.navigationController pushViewController:next animated:YES];
}
- (IBAction)didClickOrderHistoryButton:(id)sender
{
    OrderHistory *next =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderHistory"];
    [self.navigationController pushViewController:next animated:YES];
    
}
- (IBAction)didClickSignOutButton:(id)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DeliveryBoyProfile"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"OnOff"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    List *next =[self.storyboard instantiateViewControllerWithIdentifier:@"List"];
    [self.navigationController pushViewController:next animated:YES];
}
- (IBAction)RediusChangeSwitch:(UIButton *)sender {
    if([sender tag]==33)
    {
        
        sender.tag=333;
        NSLog(@"Switch is OFF");
        NSMutableDictionary *dicStatus = [[NSMutableDictionary alloc] init];
        [dicStatus setValue:[[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0] forKey:@"deliverboy_id"];
        [dicStatus setValue:@"no" forKey:@"status"];
        
        NSString *url =[NSString stringWithFormat:@"%@deliveryboy_presence.php",SERVERURL];
        [self POST_Order:dicStatus webserviceName:url flag:@"2"];
        [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"OnOff"];

       [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"off_btn.png"] forState:UIControlStateNormal];
    }
    else
    {
        sender.tag=33;
        NSLog(@"Switch is ON");
        NSMutableDictionary *dicStatus = [[NSMutableDictionary alloc] init];
        [dicStatus setValue:[[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0] forKey:@"deliverboy_id"];
        [dicStatus setValue:@"yes" forKey:@"status"];
        
        NSString *url =[NSString stringWithFormat:@"%@/deliveryboy_presence.php",SERVERURL];
        [self POST_Order:dicStatus webserviceName:url flag:@"2"];
        
        [_btnSwitch setBackgroundImage:[UIImage imageNamed:@"on_btn.png"] forState:UIControlStateNormal];
 [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"OnOff"];
    }
}

@end
