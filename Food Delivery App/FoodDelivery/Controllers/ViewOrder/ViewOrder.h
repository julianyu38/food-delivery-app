//
//  ViewOrder.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 07/05/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewOrder : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
}
@property (weak, nonatomic) IBOutlet UIView *view_loading;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UIButton *imgBack;
@property (nonatomic, retain) NSString *orderId;
@property (weak, nonatomic) IBOutlet UILabel *lblViewOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblTTotal;

@end
