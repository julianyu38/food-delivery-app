//
//  ViewOrder.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 07/05/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "ViewOrder.h"
#import "Constants.h"
#import "Snackbar.h"
#import "Reachability.h"

@interface ViewOrder ()
{
    NSMutableArray *arrOrder;
    NSMutableArray *arrOrderItem;
    NSString *language;
}
@end

@implementation ViewOrder

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self set_Language];
    _tableView.dataSource = self;
    _tableView.delegate=self;
    _tableView.tableFooterView= [UIView new];
    [self retriveJSON:[NSString stringWithFormat:@"%@order_item_details.php?order_id=%@",SERVERURL,_orderId]flag:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblViewOrder.text = NSLocalizedString(@"lblViewOrder", @"");
    _lblTTotal.text = NSLocalizedString(@"lblTotal", @"");
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        [_imgBack setImage:[_imgBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _lblTotal.textAlignment = UITextAlignmentLeft;

    }
    else
    {
        NSLog(@"en");
    }
    
    
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Get Data from Server

-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}

#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
    
    
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
   
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                arrOrder=[[NSMutableArray alloc]init];
                arrOrderItem=[[NSMutableArray alloc]init];

                arrOrder = [json valueForKey:@"Order"];
               
                _lblTotal.text =[NSString stringWithFormat:@"%@ %@",dollar,[arrOrder valueForKey:@"order_amount"]];
                arrOrderItem =[arrOrder valueForKey:@"item_name"];
                [_tableView reloadData];
            }
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:[json valueForKey:@"order"] duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    
    
    _view_loading.hidden = YES;
}
#pragma mark TableView Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOrderItem.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *lblCateName=(UILabel *) [cell.contentView viewWithTag:102];
    UILabel *lblQty=(UILabel *) [cell.contentView viewWithTag:103];
    UILabel *lblQtyNo=(UILabel *) [cell.contentView viewWithTag:104];
    UILabel *lblBasic=(UILabel *) [cell.contentView viewWithTag:105];
    UILabel *lblBasicPrice=(UILabel *) [cell.contentView viewWithTag:106];
    
    NSDictionary *dic = [arrOrderItem objectAtIndex:indexPath.row];
    
    
    //set Font
    lblCateName.font = [UIFont fontWithName:RFontRegular size:lblCateName.font.pointSize];
    lblQty.font = [UIFont fontWithName:RFontRegular size:lblQty.font.pointSize];
    lblQtyNo.font = [UIFont fontWithName:RFontRegular size:lblQtyNo.font.pointSize];
    lblBasic.font = [UIFont fontWithName:RFontRegular size:lblBasic.font.pointSize];
    lblBasicPrice.font = [UIFont fontWithName:RFontRegular size:lblBasicPrice.font.pointSize];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //setText
    
    lblCateName.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]];
    lblQtyNo.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"qty"]];
    lblBasicPrice.text = [NSString stringWithFormat:@"%@%@",dollar,[dic valueForKey:@"amount"]];
    
    UILabel *lblQtyT = (UILabel *)[cell.contentView viewWithTag:98];
    UILabel *lblItemPrice = (UILabel *)[cell.contentView viewWithTag:89];
    lblQtyT.text = NSLocalizedString(@"lblQty", @"");
    lblItemPrice.text = NSLocalizedString(@"lblItemPrice", @"");
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    OrderDetail *next =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetail"];
    //    next.orderId = [arrOrderNo objectAtIndex:indexPath.row];
    //    [self.navigationController pushViewController:next animated:YES];
    
}

@end
