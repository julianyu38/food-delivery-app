//
//  OrderSucess.h
//  FoodDelivery
//
//  Created by Redixbit on 08/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Constants.h"

@interface OrderSucess : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lbl_username;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UILabel *lbl_delivery;

@property (weak, nonatomic) IBOutlet UIButton *btn_payRupees;

@property (nonatomic, retain) NSDecimalNumber *totalPay;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *restDelTime;
@property (nonatomic, retain) NSString *restName;

@property (weak, nonatomic) IBOutlet UIButton *btnWhatsApp;
@property (weak, nonatomic) IBOutlet UIButton *btnFaceBook;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@end
