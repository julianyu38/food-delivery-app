//
//  OrderSucess.m
//  FoodDelivery
//
//  Created by Redixbit on 08/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "OrderSucess.h"

@interface OrderSucess ()
{
      NSString *language;
}

@end

@implementation OrderSucess

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.btn_payRupees setTitle:[NSString stringWithFormat:@"Pay %@ at time of delivery",self.totalPay] forState:UIControlStateNormal];
    
  NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"Name"];
    NSString *userContact = [[NSUserDefaults standardUserDefaults] valueForKey:@"PhoneNo"];
    
    NSString *phone = userContact;
    if ([phone isEqualToString:@"(null)"])
    {
        phone = @"";
    }
  
    
    [self set_Language];
    
    

    NSMutableDictionary *location = [[NSUserDefaults standardUserDefaults]valueForKey:@"location"];
    
    self.lbl_username.text = [NSString stringWithFormat:@"%@",userName];
    self.lbl_address.text = [NSString stringWithFormat:@"%@ \n%@",_address,phone];//[NSString stringWithFormat:@"%@, %@ - %@ \n%@", self.address,[location valueForKey:@"address"],[location valueForKey:@"zipcode"],phone];
    self.lbl_delivery.text = [NSString stringWithFormat:@"Your order will be delivered after %@",self.restDelTime];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        //        _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"en");
    }
    
    
}

#pragma mark - Back
- (IBAction)btn_back:(UIButton *)sender
{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didClickWhatsAppButton:(id)sender
{
    NSString * msg = [NSString stringWithFormat:@"I m ordered food from %@ using food delivery, and they will be delivered within %@, %@",_restName,self.restDelTime,APP_URL];
    
    msg = [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    msg = [msg stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    msg = [msg stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    msg = [msg stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    msg = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSLog(@"%@",urlWhats);
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WhatsApp Alert Title",@"") message:NSLocalizedString(@"WhatsApp Alert SubTitle",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"WhatsApp Alert closeButtonTitle",@"") otherButtonTitles:nil];
        [alert show];
    }
}
- (IBAction)didClickFaceBookButton:(id)sender
{
    
    NSString *str =[NSString stringWithFormat:@"I m ordered food from %@ using food delivery, and they will be delivered within %@",_restName,self.restDelTime];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL=[NSURL URLWithString:APP_URL];
    content.quote =str;
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}


@end
