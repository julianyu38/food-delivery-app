//
//  List.m
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "List.h"
#import "Color.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Setting.h"
#import "SeachByMap.h"
#import "MostRatedRrequsturant.h"

@interface List ()
{
     int startIndex;
    int pageRow;
    NSString *language;
    NSString *NotLoad;
    NSMutableArray *city_arr;
    NSString *strCity;
    NSString *strRadius;
    NSString *InstanceID;
    NSString *userID;
}


@end

@implementation List
@synthesize locationManager = locationManager_;

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   

    isMenuOpen = NO;
    self.arrData = [[NSMutableArray alloc]init];
    [self set_Language];
     [_arrData removeAllObjects];
    [self.view addSubview:view_loading];
    view_loading.hidden = NO;
    startIndex = listRow;
     pageRow = 1;
    
    //For Radius
    
    NSString *strOnOff = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"OnOff"];
   
    if ([strOnOff isEqualToString:@"off"]) {
        
        strRadius = @"100000";
    }
    else if ([strOnOff isEqualToString:@"on"]) {

            strRadius = [[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"];

    }
    else
    {
        strRadius = @"100000";
    }
    
    //For City
    strCity = [[NSUserDefaults standardUserDefaults]objectForKey:@"City_Name"];
    if (strCity.length > 0) {
        NSLog(@"CITY FOUND : %@",strCity);
        lbl_title.text = strCity;
    }else{
        [self GetData:@"restaurant_city.php"];
    }
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"location"]);
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    localTimeZone = [timeZone name];
    self.view_menu.frame = self.view.frame;
    
    locationManager_ = [[CLLocationManager alloc] init];
    locationManager_.delegate = self;
    locationManager_.distanceFilter = kCLDistanceFilterNone;
    locationManager_.desiredAccuracy = kCLLocationAccuracyBest;
    
    view_profile.hidden = YES;
   
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [locationManager_ requestWhenInUseAuthorization];
    }
    _tbl_list.dataSource =self;
    _tbl_list.delegate =self;
    self.tbl_list.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [View_UIGestureRecognizer addGestureRecognizer:tap];
    [View_UIGestureRecognizer setHidden:YES];
    [self.view_menu addGestureRecognizer:tap];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleRightSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [viewMain addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleLeftSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view_menu addGestureRecognizer:swipeLeft];
//    [self addLoadMoreButton];
//    [self buttonLoadMoreTouched];

}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblUserProfile.text = NSLocalizedString(@"lblUserProfile", @"");
    _lblSeachResturant.text = NSLocalizedString(@"lblSeachResturant", @"");
    _lblSearchByMap.text = NSLocalizedString(@"lblSearchByMap", @"");
    _lblMRR.text = NSLocalizedString(@"lblMRR", @"");
    _lblCuisine.text = NSLocalizedString(@"lblCuisine", @"");
    _lblFavourites.text = NSLocalizedString(@"lblFavourites", @"");
    _lblMyOrders.text = NSLocalizedString(@"lblMyOrders", @"");
    _lblShare.text = NSLocalizedString(@"lblShare", @"");
    _lblTerm.text = NSLocalizedString(@"lblTerm", @"");
    _lblAbout.text = NSLocalizedString(@"lblAbout", @"");
    _lblSignout.text = NSLocalizedString(@"lblSignout", @"");
    [_signin_btn setTitle:NSLocalizedString(@"btnSignin", @"") forState:UIControlStateNormal];
    _txt_search.placeholder=NSLocalizedString(@"txt_search", @"");
    
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        _imgAddLocation.image=[_imgAddLocation.image imageFlippedForRightToLeftLayoutDirection];
        _imgSeach.image=[_imgSeach.image imageFlippedForRightToLeftLayoutDirection];
         [_btnAddManually setImage:[_btnAddManually.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
          _imgFiltter.image=[_imgFiltter.image imageFlippedForRightToLeftLayoutDirection];
        _txt_search.textAlignment = UITextAlignmentRight;
         [_search_rest setImage:[_search_rest.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_search_by_map setImage:[_search_by_map.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_most_rest setImage:[_most_rest.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_cuisin setImage:[_cuisin.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_favourite setImage:[_favourite.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [_btnMyOrder setImage:[_btnMyOrder.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_share setImage:[_share.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_tc setImage:[_tc.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
         [_about setImage:[_about.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _signout_btn.image=[_signout_btn.image imageFlippedForRightToLeftLayoutDirection];
        _imgProfile1.image=[_imgProfile1.image imageFlippedForRightToLeftLayoutDirection];
        _signin_btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

    }
    else
    {
        NSLog(@"en");
    }
    
    
}
//Execute before displaying UIViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    InstanceID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"firebaseId"];
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
   
        [self GetData];
    
    [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *location = [[NSUserDefaults standardUserDefaults]valueForKey:@"location"];
    if (location)
    {
        latitude = [location valueForKey:@"latitude"];
        longitude = [location valueForKey:@"longitude"];
        
    }
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
    

    NSLog(@"User : %@",userID);
    if (userID)
    {
        view_profile.hidden = NO;
        view_login.hidden = YES;
        NSString *userEmail = [[NSUserDefaults standardUserDefaults] valueForKey:@"Mail"];
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"Name"];
        NSString *userUsl = [[NSUserDefaults standardUserDefaults] valueForKey:@"Profile"];
        
        
        NSString *LoginType = [[NSUserDefaults standardUserDefaults] valueForKey:@"LoginType"];
        NSString *userImage;
        if ([LoginType isEqualToString:@"appuser"]) {
            userImage = [NSString stringWithFormat:@"%@%@",IMAGEURL,userUsl];
        }
        else if ([LoginType isEqualToString:@"Facebook"])
        {
            userImage = [NSString stringWithFormat:@"%@",userUsl];
        }
        else if([LoginType isEqualToString:@"Google"])
        {
            userImage = [NSString stringWithFormat:@"%@",userUsl];
        }
        lbl_username.text = userName;
//        NSArray *array = [userEmail componentsSeparatedByString:@"@"];
        lbl_userid.text = userEmail;//[NSString stringWithFormat:@"@%@",[array objectAtIndex:0]];
        [imgProfile.layer setMasksToBounds:YES];
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
        imgProfile.clipsToBounds = YES;
        //        [imgProfile.layer setCornerRadius:15.0];
       
        [imgProfile sd_setImageWithURL:[NSURL URLWithString:userImage] placeholderImage:nil options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
            [imgProfile updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [imgProfile reveal];
        }];
    }
    else
    {
        view_login.hidden = NO;
        view_profile.hidden = YES;
        lbl_username.text = @"USERNAME";
        lbl_userid.text = [NSString stringWithFormat:@"@userid"];
        imgProfile.image = nil;
    }
}

//Execute after displaying UIViewController
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [NSUserDefaults standardUserDefaults];
    view_loading.hidden = NO;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"location"] == nil)
    {
        [locationManager_ startUpdatingLocation];
    }
    else
    {
        
         [_arrData removeAllObjects];
        if (isSuccess == true) {
            NSLog(@"ok");
        }
        else
        {
            [self rest];
        }
        
    }
    [View_UIGestureRecognizer setHidden:YES];
    
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
}

//Execute on leaving UIViewController
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    //After leaving UIViewController it will hide Menu
    [self handleTapGesture:nil];
    
    
}

#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Retirve Data From Webservice
-(void)GetData
{
    NSString *StringURL ;
//    40.75921100
//    -73.98463800
     if (userID != nil && InstanceID !=nil)
     {
            StringURL= [NSString stringWithFormat:@"%@token.php?token=%@&type=Iphone&user_id=%@&delivery_boyid=null",SERVERURL,InstanceID,userID];
            NSLog(@"URL :: %@",StringURL);
     }
     else
     {
         StringURL= [NSString stringWithFormat:@"%@token.php?token=%@&type=Iphone&user_id=null&delivery_boyid=null",SERVERURL,InstanceID];
         NSLog(@"URL :: %@",StringURL);
     }
   
    StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = nil;
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
    
    _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection2 start];
    
}
#pragma mark - Post Data From Webservice
/*-(void)POST_Order
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    NSMutableDictionary*parameters=[[NSMutableDictionary alloc] init];
    NSLog(@"TOKEN123 : %@",InstanceID);
    [parameters setValue:InstanceID forKey:@"token"];
    
    [parameters setValue:@"Iphone" forKey:@"type"];
    [parameters setValue:userID forKey:@"user_id"];
    [parameters setValue:@"null" forKey:@"delivery_boyid"];
    
    
    NSLog(@"PARAM123 : %@",parameters);
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //Assuming data is not nil we add this to the multipart form
    
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@token.php",SERVERURL]]];
    
    _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection2 start];
    
    
}*/
#pragma mark - Retirve Data From Webservice
-(void)GetData:(NSString *)url
{
    if(![self isConnected])
    {
        
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        NSLog(@"URL :: %@",url);
        NSString *StringURL = [NSString stringWithFormat:@"%@%@",SERVERURL, url];
        StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
        
        _connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [_connection1 start];
    }
}

#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == _connection1)
    {
        _receivedData1 = [[NSMutableData alloc]init];
        [_receivedData1 setLength:0];
    }
    else if (connection == _connection2)
    {
        _receivedData2 = [[NSMutableData alloc]init];
        [_receivedData2 setLength:0];
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == _connection1)
    {
        [_receivedData1 appendData:data];
    }
    else if (connection == _connection2)
    {
        [_receivedData2 appendData:data];
    }
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    view_loading.hidden = NO;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (connection == _connection1)
    {
        if (_receivedData1 != nil)
        {
            NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"json : %@, Error :: %@",resultDic,error);
            if (error == nil)
            {
                
                city_arr=[[NSMutableArray alloc]init];
                city_arr=[[[resultDic objectForKey:@"city"]valueForKey:@"city_name"]objectAtIndex:0];
                
                strCity =[[[resultDic objectForKey:@"city"]valueForKey:@"city_name"]objectAtIndex:0];
                lbl_title.text = strCity;
                [[NSUserDefaults standardUserDefaults]setObject:strCity forKey:@"selectClick"];
                
                [[NSUserDefaults standardUserDefaults]setObject:strCity forKey:@"City_Name"];
                

                NSLog(@"CjeCity %@ :",city_arr);
                 [self rest];
                //            cityId_arr=[[[resultDic valueForKey:@"Cities"] objectAtIndex:0]valueForKey:@"id"];
               
            }
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No Record Available!" duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            NSLog(@"Data not found");
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server" duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    else if (connection == _connection2)
    {
        if (_receivedData2 != nil)
        {
            
            NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:_receivedData2 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"JSON Result notification:: %@",jsonResult);
            
        }
    }
    
    view_loading.hidden = YES;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancel:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - UIGestureRecognizer Handler Method
-(void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (isMenuOpen)
    {
        //Hidding Side Menu
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
         {
             CATransition *transition = [CATransition animation];
             transition.duration = 0.3;
             transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
             transition.subtype = kCATransitionFromRight;
             [self.view.window.layer addAnimation:transition forKey:nil];
             viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, 0), CGAffineTransformMakeScale(1.0, 1.0));
             [self.view addSubview:viewMain];
         } completion:^(BOOL finished){
         }];
        isMenuOpen = NO;
        }
        else
        {
            NSLog(@"en");
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
             {
                 CATransition *transition = [CATransition animation];
                 transition.duration = 0.3;
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                 transition.subtype = kCATransitionFromRight;
                 [self.view.window.layer addAnimation:transition forKey:nil];
                 viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, 0), CGAffineTransformMakeScale(1.0, 1.0));
                 [self.view addSubview:viewMain];
             } completion:^(BOOL finished){
             }];
            isMenuOpen = NO;
        }
    }
}

-(void)handleRightSwipe:(UISwipeGestureRecognizer *)SwipeGestureRecognizer
{
    if (SwipeGestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if (!isMenuOpen)
        {
            [self btn_menu:nil];
        }
    }
}
-(void)handleLeftSwipe:(UISwipeGestureRecognizer *)SwipeGestureRecognizer
{
    [self handleTapGesture:nil];
}
#pragma mark - CLLocationManagerDelegate Method
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [_arrData removeAllObjects];
    NSLog(@"Location Failed");
   [self rest];
    NSLog(@"didFailWithError: %@", error);
    [self showAlertView:@"Error" message:@"Failed to Get Your Location. Please Try Manually" cancel:@"OK"];
    view_loading.hidden = YES;
    [locationManager_ stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    view_login.hidden = NO;

    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [locationManager_ stopUpdatingLocation];
        
        //Get Address From Latitude and Longitude
        CLGeocoder* geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             CLPlacemark* placemark;
            NSString *address= nil;
             if (error == nil && [placemarks count] > 0)
             {
                 placemark = [placemarks lastObject];
                 address = [NSString stringWithFormat:@"%@, %@", placemark.name, placemark.locality];
                 NSLog(@"Current Address :: %@",address);
                 
                 
                 [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"Address"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                
                 NSMutableDictionary *location = [[NSMutableDictionary alloc] init];
                 [location setValue:latitude forKey:@"latitude"];
                 [location setValue:longitude forKey:@"longitude"];
                 [location setValue:address forKeyPath:@"address"];
                 [location setValue:placemark.postalCode forKey:@"zipcode"];
                 NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                 [userDefault setObject:location forKey:@"location"];
                 [userDefault synchronize];
                 view_loading.hidden = YES;
                  [self.arrData removeAllObjects];
                 [self rest];
             }
         }];
        locationManager_ = nil;
    }
}

#pragma mark - Restaurant Fetching
-(void)rest
{
    [self.view addSubview:view_loading];
    view_loading.hidden = NO;
//
    NSString *urlString;
    NSLog(@"CheckCsfh %@",strCity);
    if (self.categoryName != nil)
    {
        if ([_strSearch isEqualToString:@"search"])
        {
                self.str_search = [self.str_search stringByReplacingOccurrencesOfString:@" "
                                                                             withString:@"%20"];
                self.tbl_list.tableFooterView.hidden = YES;
                urlString = [NSString stringWithFormat:@"%@restaurantlist.php?timezone=%@&lat=%@&lon=%@&search=%@&noofrecords=%d&pageno=%d&radius=%@",SERVERURL,localTimeZone,latitude,longitude,_str_search,startIndex,pageRow,strRadius];
        }
        else if([_strSearch isEqualToString:@"category"])
        {
             self.categoryName = [self.categoryName stringByReplacingOccurrencesOfString:@" "
                                                                           withString:@"%20"];
           
            urlString = [NSString stringWithFormat:@"%@restaurantlist.php?timezone=%@&lat=%@&lon=%@&search=%@&noofrecords=%d&pageno=%d&radius=%@",SERVERURL,localTimeZone,latitude,longitude,_categoryName,startIndex,pageRow,strRadius];
        }
    }
    else if ([self.str_search length]>0)
    {
            self.str_search = [self.str_search stringByReplacingOccurrencesOfString:@" "
                                                           withString:@"%20"];
            self.tbl_list.tableFooterView.hidden = YES;
           urlString = [NSString stringWithFormat:@"%@restaurantlist.php?timezone=%@&lat=%@&lon=%@&search=%@&noofrecords=%d&pageno=%d&radius=%@",SERVERURL,localTimeZone,latitude,longitude,_str_search,startIndex,pageRow,strRadius];
    }
    else
    {
    
        strCity = [strCity stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@"%20"];
            urlString = [NSString stringWithFormat:@"%@restaurantlist.php?timezone=%@&lat=%@&lon=%@&location=%@&noofrecords=%d&pageno=%d&radius=%@",SERVERURL,localTimeZone,  latitude,longitude,strCity,startIndex,pageRow,strRadius];
    //        @"21.22858296",@"72.89721796",@"Varachha%20Road,Surat",
    }
       NSLog(@"UrlString %@",urlString);
    WebServiceHelper  *obj_add = [WebServiceHelper new];
    
    [obj_add setCurrentCall:urlString];
    [obj_add setMethodName:urlString];
    [obj_add setMethodResult:@"Restaurant_list"];
    [obj_add setDelegate:self];
    [obj_add initiateConnection];
}




#pragma mark - UITextField Delegate Method
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
  
        [self.arrData removeAllObjects];
        pageRow = 1;
        _strSearch=@"search";
        startIndex=listRow;
        self.str_search = self.txt_search.text;
        NSLog(@"%@",self.txt_search.text);
        [self rest];
   
    
    [self.txt_search resignFirstResponder];
    return YES;
}

#pragma mark- LazyLoad Table View
- (void)tableView:(UITableView *)tableView lazyLoadNextCursor:(int)cursor{
    //for instance here you can execute webservice request lo load more data
    NSLog(@"NotLoad %@",NotLoad);
    if ([NotLoad isEqualToString:@"NotLoad"]) {
       
    }
    else
    {
        [self buttonLoadMoreTouched];
        [self.tbl_list reloadData];
    }
    
}
#pragma mark - Tableview Datasource and Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_arrData == nil)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *cell_image = (UIImageView *)[cell.contentView viewWithTag:101];
    if (indexPath.row % 2 == 0)
    {
        cell_image.image = [UIImage imageNamed:@"first_cell.png"];
    }
    else
    {
        cell_image.image = [UIImage imageNamed:@"secound_cell.png"];
    }
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
        cell_image.image=[cell_image.image imageFlippedForRightToLeftLayoutDirection];
    }
    else
    {
        NSLog(@"en");
    }
    
    NSDictionary *dic = [_arrData objectAtIndex:indexPath.row];
    
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        // This is the last cell
        NSLog(@"Scrolling");
    }
    UIImageView *rest_image = (UIImageView *)[cell.contentView viewWithTag:102];
    NSString *Str_image_name = [NSString stringWithFormat:@"%@%@",IMAGEURL,[dic valueForKey:@"image"]];
    Str_image_name = [Str_image_name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [rest_image sd_setImageWithURL:[NSURL URLWithString:Str_image_name] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
        [rest_image updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [rest_image reveal];
    }];
    
    rest_image.layer.cornerRadius = 5.0f;
    rest_image.clipsToBounds = YES;
    UILabel *rest_name = (UILabel *)[cell.contentView viewWithTag:103];
    [rest_name setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]]];
    
    UILabel *restCat = (UILabel *)[cell.contentView viewWithTag:104];
   
        NSString *restCategoryNames = [[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] objectAtIndex:0];
        for (int i = 1; i<[[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] count]; i++)
        {
            restCategoryNames = [restCategoryNames stringByAppendingString:[NSString stringWithFormat:@", %@",[[[_arrData valueForKey:@"Category"] objectAtIndex:indexPath.row] objectAtIndex:i]]];
        }
        [restCat setText:restCategoryNames];
    
    
    UILabel *min = (UILabel *)[cell.contentView viewWithTag:56];
    UILabel *rating = (UILabel *)[cell.contentView viewWithTag:65];
    
    min.text = NSLocalizedString(@"lblMinute", @"");
    rating.text = NSLocalizedString(@"lblRatings", @"");
    
    UILabel *rest_open = (UILabel *)[cell.contentView viewWithTag:105];
    NSDateFormatter *formateDate = [[NSDateFormatter alloc]init];
    formateDate.dateFormat = @"HH:mm:ss";
    NSString *Res_Status = [dic valueForKey:@"res_status"];
    if ([Res_Status isEqualToString:@"open"])
    {
        NSString *str = [dic valueForKey:@"close_time"];
        NSDate *date = [formateDate dateFromString:str];
        formateDate.dateFormat = @"hh:mm a";
        [rest_open setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"open", @""),str]];
        rest_open.textColor = [Color colorFromHexString:@"7CB66A"];
    }
    else
    {
        [rest_open setTextColor:[UIColor redColor]];
        NSString *str = [dic valueForKey:@"open_time"];
        NSDate *date = [formateDate dateFromString:str];
        formateDate.dateFormat = @"hh:mm a";
        [rest_open setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"close", @""),str]];
    }

    UILabel *rest_time = (UILabel *)[cell.contentView viewWithTag:106];
    [rest_time setText:[NSString stringWithFormat:@"%@",[dic valueForKey:@"delivery_time"]]];
    
    UILabel *rest_rate = (UILabel *)[cell.contentView viewWithTag:107];
    [rest_rate setText:[NSString stringWithFormat:@"%.1f",[[dic valueForKey:@"ratting"] floatValue]]];
    
    UILabel *rest_distance = (UILabel *)[cell.contentView viewWithTag:110];
    [rest_distance setText:[NSString stringWithFormat:@"%.1f km away",[[dic valueForKey:@"distance"] floatValue]]];
    cell.backgroundColor=[UIColor clearColor];
    dic = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     Detail *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    detail.restaurantID = [[_arrData objectAtIndex:indexPath.row] valueForKey:@"id"];
    detail.lat =[[_arrData objectAtIndex:indexPath.row] valueForKey:@"lat"];
    detail.lon =[[_arrData objectAtIndex:indexPath.row] valueForKey:@"lon"];
    detail.distance=[[_arrData objectAtIndex:indexPath.row] valueForKey:@"distance"];
    detail.strScreen= @"home";
   
    [self.navigationController pushViewController:detail animated:YES];
   
//    detail.dicRestDetail = [jsonResult objectAtIndex:indexPath.row];
   
   
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark - Float Animation for button
-(void)floatButton:(UIButton *)button
{
    CGRect Frame = button.frame;
    [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        button.frame = CGRectMake(button.frame.origin.x+5, button.frame.origin.y+5, button.frame.size.width-10, button.frame.size.height-10);
        button.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }completion:^(BOOL finished){
        if (finished)
        {
            [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                button.frame = CGRectMake(button.frame.origin.x-10, button.frame.origin.y-10, button.frame.size.width+20, button.frame.size.height+20);
                button.alpha = 0.0;
            }completion:^(BOOL finished){
                if (finished)
                {
                    [UIView animateWithDuration:0.1f animations:^{
                        button.alpha = 1.0;
                        button.frame = Frame;
                    }];
                }
            }];
        }
    }];
}



#pragma mark - Button Click Events
//Edit Location
- (IBAction)btn_SeachResturant:(id)sender {
    if (isMenuOpen)
    {
        //Hidding Side Menu
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
             {
                 CATransition *transition = [CATransition animation];
                 transition.duration = 0.3;
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                 transition.subtype = kCATransitionFromRight;
                 [self.view.window.layer addAnimation:transition forKey:nil];
                 viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, 0), CGAffineTransformMakeScale(1.0, 1.0));
                 [self.view addSubview:viewMain];
             } completion:^(BOOL finished){
             }];
            isMenuOpen = NO;
        }
        else
        {
            NSLog(@"en");
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
             {
                 CATransition *transition = [CATransition animation];
                 transition.duration = 0.3;
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                 transition.subtype = kCATransitionFromRight;
                 [self.view.window.layer addAnimation:transition forKey:nil];
                 viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, 0), CGAffineTransformMakeScale(1.0, 1.0));
                 [self.view addSubview:viewMain];
             } completion:^(BOOL finished){
             }];
            isMenuOpen = NO;
        }
    }
}
- (IBAction)btn_SeachByMap:(id)sender {

    SeachByMap *food = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachByMap"];
  
    [self.navigationController pushViewController:food animated:YES];
    
}
- (IBAction)btn_Most_Rated_Reasturant:(id)sender {
    MostRatedRrequsturant *food = [self.storyboard instantiateViewControllerWithIdentifier:@"MostRatedRrequsturant"];
    
    [self.navigationController pushViewController:food animated:YES];
}

- (IBAction)btn_editLocation:(UIButton *)sender
{


    Setting *locate = [self.storyboard instantiateViewControllerWithIdentifier:@"Setting"];
    locate.strCheckScreen=@"home";
    [self.navigationController pushViewController:locate animated:YES];
}
-(void)rippleEffectForView:(UIButton *)button
{
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.3f];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    [animation setType:@"rippleEffect"];
    [button.layer addAnimation:animation forKey:NULL];
}
// Menu button
- (IBAction)btn_menu:(UIButton *)sender
{
    if (!isMenuOpen)
    {
        if ([language isEqualToString:country])
        {
//            (240 85.2; 224 397.6)
            NSLog(@"ar");
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
             {
                 CATransition *transition = [CATransition animation];
                 transition.duration = 0.3;
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                 transition.subtype = kCATransitionFromRight;
                 [self.view.window.layer addAnimation:transition forKey:nil];
                 [self.view addSubview:self.view_menu];
                 
                 viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(-(self.view_menu.frame.size.width - (self.view_menu.frame.size.width/7)), 0), CGAffineTransformMakeScale(0.7, 0.7));
                 [self.view_menu addSubview:viewMain];
             } completion:^(BOOL finished){
             }];
            isMenuOpen = YES;
        }
        else
        {
            NSLog(@"en");

            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
             {
                 CATransition *transition = [CATransition animation];
                 transition.duration = 0.3;
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                 transition.subtype = kCATransitionFromRight;
                 [self.view.window.layer addAnimation:transition forKey:nil];
                 [self.view addSubview:self.view_menu];
                 
                 viewMain.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(self.view_menu.frame.size.width - self.view_menu.frame.size.width/7, 0), CGAffineTransformMakeScale(0.7, 0.7));
                 [self.view_menu addSubview:viewMain];
             } completion:^(BOOL finished){
             }];
            isMenuOpen = YES;
        }
    }
    else
    {
        [self handleTapGesture:nil];
    }
}

- (IBAction)btn_category:(UIButton *)sender
{
    FoodCategory *food = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodCategory"];
   
    [self.navigationController pushViewController:food animated:YES];
}

- (IBAction)btn_login:(UIButton *)sender
{
    Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
  
    [self.navigationController pushViewController:login animated:YES];
}

-(IBAction)btn_fav:(UIButton *)sender
{
    Favourite *fav = [self.storyboard instantiateViewControllerWithIdentifier:@"Favourite"];
   
    [self.navigationController pushViewController:fav animated:YES];
}

- (IBAction)btn_about:(UIButton *)sender
{
    AboutUs *about = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];

    [self.navigationController pushViewController:about animated:YES];
}
- (IBAction)btn_share:(id)sender {
    
    
    SCLAlertView *alert=[[SCLAlertView alloc]init];
    
    [alert addButton:@"Text" target:self selector:@selector(Message)];
    [alert addButton:@"Whatapp" target:self selector:@selector(Whatsapp)];
    [alert addButton:@"Facebook" target:self selector:@selector(Facebook)];
    
    UIColor *color = [UIColor colorWithRed:13.0/255.0 green:116.0/255.0 blue:196.0/255.0 alpha:1.0];
    [alert setTitleFontFamily:@"Superclarendon" withSize:12.0f];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:NSLocalizedString(@"Social_Title", @"") subTitle:nil closeButtonTitle:NSLocalizedString(@"Social_Close", @"") duration:0.0f];
}

- (IBAction)btn_terms:(UIButton *)sender
{
    Terms *Terms = [self.storyboard instantiateViewControllerWithIdentifier:@"Terms"];

    [self.navigationController pushViewController:Terms animated:YES];
}

- (IBAction)btn_SignOut:(UIButton *)sender
{
   userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"User_id"];
    if (userID)
    {
        SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
        [alert addButton:@"YES" actionBlock:^{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            view_login.hidden = NO;
            view_profile.hidden = YES;
            lbl_username.text = @"USERNAME";
            lbl_userid.text = [NSString stringWithFormat:@"@userid"];
            imgProfile.image = nil;
        }];
        [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:@"Confirm logout" subTitle:@"Do you really want to logout ?" closeButtonTitle:@"NO" duration:0.0];
    }
}
- (IBAction)btn_MyOrder:(id)sender {
    MyOrder *Terms = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrder"];
   
    [self.navigationController pushViewController:Terms animated:YES];
}

//Set Sort Order Data

-(void)getSortedArray:(NSMutableArray *)resultDic
{
    NSString *status = [[resultDic objectAtIndex:0] valueForKey:@"status"];
//    [[NSUserDefaults standardUserDefaults] setObject:status forKey:@"Status"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([status isEqualToString:@"Success"]) {
        isSuccess = true;
        NSMutableArray *TempArray = [[resultDic objectAtIndex:0] valueForKey:@"Restaurant_list"];
        if (![TempArray count] > 0) {
            view_loading.hidden = YES;
            //        [self CallAlertView:NSLocalizedString(@"Error Alert Title",@"") second:NSLocalizedString(@"Error Alert SubTitle",@"") third:NSLocalizedString(@"Error Alert closeButtonTitle",@"")];
            [self.tbl_list reloadData];
            
        }else{
            
            NSLog(@"TempArray %lul",(unsigned long)[TempArray count]);
            for (int i=0; i<[TempArray count]; i++) {
                NSMutableDictionary *temp = [TempArray objectAtIndex:i];
                NSLog(@"temp %@",temp);
                [self.arrData addObject:temp];
            }
            NSLog(@"Check %lu",(unsigned long)[self.arrData count]);
            
            if ([TempArray count]<listRow) {
                self.tbl_list.tableFooterView.hidden = YES;
            }else{
                //            [self addLoadMoreButton];
                NSLog(@"CheckLoadMore");
                //            [self buttonLoadMoreTouched];
            }
            
            view_loading.hidden = YES;
            [self.tbl_list reloadData];
        }
    }
    else
    {
        NotLoad  =@"NotLoad";
         view_loading.hidden = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:[[resultDic objectAtIndex:0] valueForKey:@"error"]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
    }
}
#pragma mark - Sharing Methods
- (void)Facebook
{
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL=[NSURL URLWithString:APP_URL];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}

//Share With WhatsApp

- (void)Whatsapp
{
    NSString * msg = [NSString stringWithFormat:@"%@  %@",NSLocalizedString(@"AppDesc", @""),APP_URL];
    
    msg = [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    msg = [msg stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    msg = [msg stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    msg = [msg stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    msg = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSLog(@"%@",urlWhats);
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WhatsApp Alert Title",@"") message:NSLocalizedString(@"WhatsApp Alert SubTitle",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"WhatsApp Alert closeButtonTitle",@"") otherButtonTitles:nil];
        [alert show];
    }
}

// Share With SMS
// send Details By Message

- (void)Message
{
    if(![MFMessageComposeViewController canSendText])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        UIColor *color = [UIColor colorWithRed:13.0/255.0 green:116.0/255.0 blue:196.0/255.0 alpha:1.0];
        
        [alert showCustom:self image:nil color:color title:@"Failed" subTitle:@"Your Device does not support SMS service" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        NSArray *recipents = @[@"9999999999"];
        NSString *message = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"AppDesc", @""),APP_URL];
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setSubject:NSLocalizedString(@"Subject", @"")];
        [messageController setBody:message];
        
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

#pragma mark - MessageComposser Delegate Method
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MessageComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Mail sent failure");
            break;
        default:
            break;
    }
    
    // Close the Message Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Load More Data In TableView

-(void)addLoadMoreButton{
    
    if (iPhoneVersion == 4 || iPhoneVersion == 5)
        FooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 60)];
    else if (iPhoneVersion == 10)
        FooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,375, 65)];
   
    else
        FooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,768, 110)];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    if (iPhoneVersion == 4 || iPhoneVersion == 5)
        button.frame = CGRectMake(35, 5, 250, 50);
    else if (iPhoneVersion == 10)
        button.frame = CGRectMake(38, 5, 300, 60);
    else
        button.frame = CGRectMake(134, 10, 500, 100);
    
    [button setBackgroundImage:[UIImage imageNamed:@"load_more"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonLoadMoreTouched) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lbl1 = [[UILabel alloc] init];
    if (iPhoneVersion == 4 || iPhoneVersion == 5){
        lbl1.frame = CGRectMake(35, 5, 250, 50);
        lbl1.font = [UIFont boldSystemFontOfSize:15];
    }else if (iPhoneVersion == 10){
        lbl1.frame = CGRectMake(38, 5, 280, 60);
        lbl1.font = [UIFont boldSystemFontOfSize:20];
    }else{
        lbl1.frame = CGRectMake(134, 10, 500, 100);
        lbl1.font = [UIFont boldSystemFontOfSize:25];
    }
    lbl1.backgroundColor=[UIColor clearColor];
    lbl1.textColor=[UIColor blackColor];
    lbl1.userInteractionEnabled=NO;
    
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.text=@"Load More";
    [FooterView addSubview:button];
    [FooterView addSubview:lbl1];
    self.tbl_list.tableFooterView = FooterView;
}

-(void)buttonLoadMoreTouched
{
    pageRow=pageRow+1;
    [self rest];
}
-(void)WebServiceHelper:(WebServiceHelper *)editor didFinishWithResult:(BOOL)result
{
    if (result)
    {
        NSMutableArray *resultDic = [[editor ReturnStr] JSONValue];
        [self getSortedArray:resultDic];
        [self.txt_search setDelegate:self];
        //        [self.txt_search setReturnKeyType:UIReturnKeyDone];
        //        self.txt_search.text = @"";
      
    }
    else
    {
        view_loading.hidden = YES;
//        [self CallAlertView:NSLocalizedString(@"Error Alert Title",@"") second:NSLocalizedString(@"Error Alert SubTitle",@"") third:NSLocalizedString(@"Error Alert closeButtonTitle",@"")];
    }
}


-(UIColor *)colorFromHexString:(NSString *)hexString {
    
    // declare an unsigned int rgbValue, scan the hexString, convert it to an int, create the RGB colors values, and return a UIColor
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    
}
@end
