//
//  List.h
//  FoodDelivery
//
//  Created by R on 22/08/2016.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "WebServiceHelper.h"

#import "LocateMe.h"
#import "Detail.h"
#import "FoodCategory.h"
#import "Favourite.h"
#import "Login.h"
#import "AboutUs.h"
#import "Terms.h"

#import "UIImageView+RJLoader.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "Reachability.h"
#import "SQLFile.h"
#import "SCLAlertView.h"
#import "SBJson.h"
#import "MyOrder.h"
#import "Snackbar.h"
#import "L3SDKLazyTableView.h"

@interface List : UIViewController<UITableViewDataSource,UITableViewDelegate,WebServiceHelperDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UIScrollViewDelegate,L3SDKLazyTableViewDelegate>
{
    NSString *latitude, *longitude, *localTimeZone;
    NSMutableArray *jsonResult,*arrCategory,*ArrList;
    BOOL isMenuOpen;
    BOOL isSuccess;
    UIView *FooterView;
    IBOutlet UILabel *lbl_title;
    IBOutlet UILabel *lbl_username;
    IBOutlet UILabel *lbl_userid;
    
    IBOutlet UIImageView *imgProfile;
    
    IBOutlet UIView *viewTitle;
    IBOutlet UIView *viewMain;
    IBOutlet UIView *view_profile;
    IBOutlet UIView *view_login;
    IBOutlet UIView *view_loading;
    IBOutlet UIView *View_UIGestureRecognizer;
    
    CGPoint touchPoint;
}
@property (strong, nonatomic) IBOutlet NSMutableArray *arrData;
@property(strong,nonatomic)NSString *str_search;
@property(strong,nonatomic)NSString *strSearch;


@property (nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic,retain) NSString *categoryName;

@property (weak, nonatomic) IBOutlet UIImageView *imgAddLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgFiltter;
@property (weak, nonatomic) IBOutlet UIImageView *imgSeach;

@property (strong, nonatomic) IBOutlet UITableView *tbl_list;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIView *view_menu;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;

@property (retain, nonatomic) NSURLConnection *connection2;
@property (retain, nonatomic) NSMutableData *receivedData2;


@property (weak, nonatomic) IBOutlet UIImageView *imgProfile1;
@property (weak, nonatomic) IBOutlet UIImageView *signout_btn;
@property (weak, nonatomic) IBOutlet UIButton *signin_btn;
@property (weak, nonatomic) IBOutlet UIButton *search_rest;
@property (weak, nonatomic) IBOutlet UIButton *search_by_map;
@property (weak, nonatomic) IBOutlet UIButton *most_rest;
@property (weak, nonatomic) IBOutlet UIButton *cuisin;
@property (weak, nonatomic) IBOutlet UIButton *favourite;
@property (weak, nonatomic) IBOutlet UIButton *share;
@property (weak, nonatomic) IBOutlet UIButton *tc;
@property (weak, nonatomic) IBOutlet UIButton *about;
@property (weak, nonatomic) IBOutlet UIButton *btnAddManually;
@property (weak, nonatomic) IBOutlet UIButton *btnMyOrder;

@property (weak, nonatomic) IBOutlet UILabel *lblUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblSeachResturant;
@property (weak, nonatomic) IBOutlet UILabel *lblSearchByMap;
@property (weak, nonatomic) IBOutlet UILabel *lblMRR;
@property (weak, nonatomic) IBOutlet UILabel *lblCuisine;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblMyOrders;
@property (weak, nonatomic) IBOutlet UILabel *lblShare;
@property (weak, nonatomic) IBOutlet UILabel *lblTerm;
@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblSignout;




@end
