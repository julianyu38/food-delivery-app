//
//  MyProfile.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfile : UIViewController
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
}
//UILable
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicalNo;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicalType;

@property (weak, nonatomic) IBOutlet UILabel *lblNameDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNoDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicalNoDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicalTypeDesc;
@property (weak, nonatomic) IBOutlet UIView *view_loading;


@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@end
