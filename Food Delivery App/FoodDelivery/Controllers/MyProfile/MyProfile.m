//
//  MyProfile.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "MyProfile.h"
#import "Constants.h"
#import "Reachability.h"
#import "Snackbar.h"
#import "SCLAlertView.h"

@interface MyProfile ()
{
    NSString *language;
    NSMutableArray *arrUserDetail;
     NSMutableDictionary *dicDeliveryProfile;
}

@end

@implementation MyProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setFont];
    [self set_Language];
   
    dicDeliveryProfile = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeliveryBoyProfile"] mutableCopy];
    [self retriveJSON:[NSString stringWithFormat:@"%@deliveryboy_profile.php?deliverboy_id=%@",SERVERURL,[[dicDeliveryProfile valueForKey:@"id"]objectAtIndex:0]]flag:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setFont
{
    _lblName.font =[UIFont fontWithName:RFontRegular size:_lblName.font.pointSize];
    _lblContactNo.font =[UIFont fontWithName:RFontRegular size:_lblContactNo.font.pointSize];
    _lblEmail.font =[UIFont fontWithName:RFontRegular size:_lblEmail.font.pointSize];
    _lblVehicalNo.font =[UIFont fontWithName:RFontRegular size:_lblVehicalNo.font.pointSize];
    _lblVehicalType.font =[UIFont fontWithName:RFontRegular size:_lblVehicalType.font.pointSize];
    
    _lblNameDesc.font =[UIFont fontWithName:RFontRegular size:_lblNameDesc.font.pointSize];
    _lblContactNoDesc.font =[UIFont fontWithName:RFontRegular size:_lblContactNoDesc.font.pointSize];
    _lblEmailDesc.font =[UIFont fontWithName:RFontRegular size:_lblEmailDesc.font.pointSize];
    _lblVehicalNoDesc.font =[UIFont fontWithName:RFontRegular size:_lblVehicalNoDesc.font.pointSize];
    _lblVehicalTypeDesc.font =[UIFont fontWithName:RFontRegular size:_lblVehicalTypeDesc.font.pointSize];
}
-(void)setText
{
    NSLog(@"Check %@",[[arrUserDetail valueForKey:@"name"]objectAtIndex:0]);
    _lblNameDesc.text=[NSString stringWithFormat:@"%@",[[arrUserDetail valueForKey:@"name"]objectAtIndex:0]];
    _lblContactNoDesc.text=[NSString stringWithFormat:@"%@",[[arrUserDetail valueForKey:@"phone"]objectAtIndex:0]];
    _lblEmailDesc.text=[NSString stringWithFormat:@"%@",[[arrUserDetail valueForKey:@"email"]objectAtIndex:0]];
    _lblVehicalNoDesc.text=[NSString stringWithFormat:@"%@",[[arrUserDetail valueForKey:@"vehicle_no"]objectAtIndex:0]];
    _lblVehicalTypeDesc.text=[NSString stringWithFormat:@"%@",[[arrUserDetail valueForKey:@"vehicle_type"]objectAtIndex:0]];
    
}
#pragma mark - SET LANGUAGE
-(void)set_Language
{
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
      
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _lblNameDesc.textAlignment = UITextAlignmentLeft;
       _lblContactNoDesc.textAlignment = UITextAlignmentLeft;
        _lblEmailDesc.textAlignment = UITextAlignmentLeft;
        _lblVehicalNoDesc.textAlignment = UITextAlignmentLeft;
        _lblVehicalTypeDesc.textAlignment = UITextAlignmentLeft;
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
- (IBAction)didClickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}
#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                arrUserDetail =[[NSMutableArray alloc]init];
                arrUserDetail=[json valueForKey:@"order"];
                NSLog(@"arrAssigned %@",arrUserDetail);
                
                [self setText];
            }
            
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No order details" duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    
    _view_loading.hidden = YES;
}
@end
