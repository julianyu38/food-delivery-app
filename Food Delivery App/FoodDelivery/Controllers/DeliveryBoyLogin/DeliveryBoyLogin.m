//
//  DeliveryBoyLogin.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "DeliveryBoyLogin.h"
#import "DeliveryBoy.h"
#import "Constants.h"
#import "Reachability.h"
#import "Snackbar.h"
#import "SCLAlertView.h"
@interface DeliveryBoyLogin ()
{
     NSString *language;
}

@end

@implementation DeliveryBoyLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    txt_email.delegate =self;
    txt_password.delegate =self;
    
    _view_loading.hidden = YES;
     [self set_Language];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didClickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didClickDeliveryBoyLoginButton:(id)sender {
    if (![self validateEmailWithString:txt_email.text] || txt_password.text.length == 0)
    {
        [self showAlertView:@"Info" message:@"Please enter email or password" cancelButtonTitle:@"OK"];
    }
    else
    {
        _view_loading.hidden = NO;
        
        
        [self Login:[NSString stringWithFormat:@"%@deliveryboy_login.php?email=%@&password=%@",SERVERURL,txt_email.text,txt_password.text]];
      
    }
//
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    _lblDeliveryBoy.text = NSLocalizedString(@"lblDeliveryBoy", @"");
    _lblBtnLogin.text = NSLocalizedString(@"lblLogin", @"");
    txt_email.placeholder = NSLocalizedString(@"txt_email", @"");
    txt_password.placeholder = NSLocalizedString(@"txt_password", @"");
    
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        _Imgscooter.image=[_Imgscooter.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBAck setImage:[_btnBAck.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        txt_email.textAlignment = UITextAlignmentRight;
        txt_password.textAlignment = UITextAlignmentRight;
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
#pragma mark - UITextfield Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [(ACFloatingTextField *)textField textFieldDidBeginEditing];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [(ACFloatingTextField *)textField textFieldDidEndEditing];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextField Method
- (IBAction)textFieldChanged:(ACFloatingTextField *)textField
{
    if (textField.tag==1)
    {
        if (![self validateEmailWithString:txt_email.text])
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
    if (textField.tag == 2)
    {
        if (textField.text.length < 3)
        {
            [self hasError:textField];
        }
        else
        {
            [self hasNoError:textField];
        }
    }
}

#pragma mark - Validations
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//Showing Error when user enters incorrect data
-(void)hasError:(ACFloatingTextField *)textfield
{
    [textfield setPlaceHolderTextColor:[UIColor redColor]];
    [textfield setBtmLineColor:[UIColor redColor]];
    [textfield setBtmLineSelectionColor:[UIColor redColor]];
}

-(void)hasNoError:(ACFloatingTextField *)textfield
{
    textfield.placeHolderTextColor = [UIColor greenColor];
    textfield.btmLineColor = [UIColor greenColor];
    textfield.btmLineSelectionColor = [UIColor greenColor];
}


#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Retrive Data from Webservice
-(void)Login:(NSString *)url
{
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"URL :: %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [_connection start];
    }
}

#pragma mark - NSURLConnection Delegate Method

-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == _connection)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == _connection)
    {
        [receivedData1 appendData:data];
    }
    
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (connection == _connection)
    {
    if (receivedData1 != nil)
    {
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",json);
        NSLog(@"success%@",[[json valueForKey:@"data"]valueForKey:@"success"]);
//        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"fullname"]objectAtIndex:0]);
//        NSLog(@"%@",[[[json valueForKey:@"user_detail"]valueForKey:@"email"]objectAtIndex:0]);
        NSLog(@"json : %@, Error :: %@",json,error);
        NSMutableDictionary *profileDictionary = [[NSMutableDictionary  alloc] init];
       profileDictionary=[[json valueForKey:@"data"]valueForKey:@"login"];
       
        if (!error)
        {
            if ([[[json valueForKey:@"data"]valueForKey:@"success"] isEqualToString:@"1"])
            {
               
                [[NSUserDefaults standardUserDefaults] setObject:profileDictionary forKey:@"DeliveryBoyProfile"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                [alert addButton:@"OK" actionBlock:^{
                   DeliveryBoy *next = [self.storyboard instantiateViewControllerWithIdentifier:@"DeliveryBoy"];
                [self.navigationController pushViewController:next animated:YES];
                    //
                }];
                [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:[NSString stringWithFormat:@"Success"] subTitle:@"Login Success." closeButtonTitle:nil duration:0.0];
                
            }
            else
            {
                [self showAlertView:[NSString stringWithFormat:@"Failed"] message:[NSString stringWithFormat:@"%@",[[json valueForKey:@"data"]valueForKey:@"login"]]cancelButtonTitle:@"OK"];
            }
        }
        else
        {
            NSLog(@"Error :: %@",error);
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Please try again later." duration:3.0];
            [self.view addSubview:snackbar];
            //            [self showAlertView:@"Info" message:@"Please try again later." cancelButtonTitle:@"OK"];
        }
        _view_loading.hidden = YES;
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
        _view_loading.hidden = YES;
    }
    }
}
#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}
- (IBAction)btnSignOut:(id)sender {
}
@end
