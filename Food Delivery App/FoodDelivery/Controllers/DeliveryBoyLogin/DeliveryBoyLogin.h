//
//  DeliveryBoyLogin.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface DeliveryBoyLogin : UIViewController<UITextFieldDelegate>
{
    IBOutlet ACFloatingTextField *txt_email;
    IBOutlet ACFloatingTextField *txt_password;
    NSMutableData *receivedData1;

}
@property (weak, nonatomic) IBOutlet UIImageView *Imgscooter;
@property (weak, nonatomic) IBOutlet UIView *view_loading;
@property (retain, nonatomic) NSURLConnection *connection;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryBoy;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnLogin;


@property (weak, nonatomic) IBOutlet UIButton *btnBAck;
@end
