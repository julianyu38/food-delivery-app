//
//  OrderDetail.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "OrderDetail.h"
#import "Constants.h"
@interface OrderDetail ()
{
    NSString *language;
    NSMutableArray *arrOrderDetail;
    NSMutableArray *arrItemName;
    NSString *strComNO;
   
}

@end

@implementation OrderDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self  setFont];
    [self set_Language];

    _tableView.dataSource=self;
    _tableView.delegate=self;
    _tableView.tableFooterView=[UIView new];
 
     [self retriveJSON:[NSString stringWithFormat:@"%@deliveryboy_order_details.php?order_id=%@",SERVERURL,_orderId]flag:@"1"];
    
    if ([_strStatus isEqualToString:@"Order is processing"]) {
        [_btnPick setTitle:@"PICKED" forState:UIControlStateNormal];
        [_btnPick setBackgroundColor:pickedcolor];
       
    }
    else if ([_strStatus isEqualToString:@"Order is out for delivery"])
    {
        [_btnPick setTitle:@"COMPLETE" forState:UIControlStateNormal];
        [_btnPick setBackgroundColor:[UIColor blackColor]];
    }
    else if([_strStatus isEqualToString:@"Order is Delivered"])
    {
        _btnPick.hidden=YES;
    }
 
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setFont
{
    
        _lblTitle.font=[UIFont fontWithName:RFontRegular size:_lblTitle.font.pointSize];
        _lblOderAmount.font = [UIFont fontWithName:RFontRegular size:_lblOderAmount.font.pointSize];
        _lblOrderItem.font = [UIFont fontWithName:RFontRegular size:_lblOrderItem.font.pointSize];
        _lblOrderDate.font = [UIFont fontWithName:RFontRegular size:_lblOrderDate.font.pointSize];
        _lblPayment.font = [UIFont fontWithName:RFontRegular size:_lblPayment.font.pointSize];
        _lblUserName.font = [UIFont fontWithName:RFontRegular size:_lblUserName.font.pointSize];
        _lblMobileNumber.font = [UIFont fontWithName:RFontRegular size:_lblMobileNumber.font.pointSize];
        _lblAddress.font = [UIFont fontWithName:RFontRegular size:_lblAddress.font.pointSize];
      _lblCall.font = [UIFont fontWithName:RFontRegular size:_lblCall.font.pointSize];
      _lblViewMap.font = [UIFont fontWithName:RFontRegular size:_lblViewMap.font.pointSize];
    
    
}
- (IBAction)didClickCallCustomerButton:(id)sender {
    NSLog(@"Call Customer");
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"phone"]]];
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]])
    {
        //        [self showAlertView:@"Warning" message:@"Can not make call" cancelButtonTitle:@"OK"];
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Can't make call from this device." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}
- (IBAction)didClickViewMapButton:(id)sender {
    NSLog(@"View Map");
    NSLog(@"Latitude :: %f, Longitude :: %f",[[arrOrderDetail valueForKey:@"lat"] doubleValue],[[arrOrderDetail valueForKey:@"long"] doubleValue]);
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake([[arrOrderDetail valueForKey:@"lat"] floatValue], [[arrOrderDetail valueForKey:@"long"] floatValue]);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    endingItem.name = [arrOrderDetail valueForKey:@"customer_name"];
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
}
- (IBAction)didClickPickedButton:(id)sender {
    
    if ([_strStatus isEqualToString:@"Order is processing"]) {
        NSMutableDictionary *dicStatus = [[NSMutableDictionary alloc] init];
        [dicStatus setValue:_orderId forKey:@"order_id"];
        
        
        NSString *url =[NSString stringWithFormat:@"%@order_pick.php",SERVERURL];
        [self POST_Order:dicStatus webserviceName:url flag:@"2"];
        
    }
    else if ([_strStatus isEqualToString:@"Order is out for delivery"])
    {
        [_btnPick setTitle:@"COMPLETE" forState:UIControlStateNormal];
        [_btnPick setBackgroundColor:[UIColor blackColor]];
        
        NSMutableDictionary *dicStatus = [[NSMutableDictionary alloc] init];
        [dicStatus setValue:_orderId forKey:@"order_id"];
        
        
        NSString *url =[NSString stringWithFormat:@"%@order_complete.php",SERVERURL];
        [self POST_Order:dicStatus webserviceName:url flag:@"2"];
    }
    else if([_strStatus isEqualToString:@"Order is Delivered"])
    {
        _btnPick.hidden=YES;
    }

}

-(void)setText
{
    
    _lblTitle.text =[NSString stringWithFormat:@"Order No %@",_orderId];
    _lblOderAmount.text=[NSString stringWithFormat:@"Order Amount %@%@",dollar,[arrOrderDetail valueForKey:@"order_amount"]];
    _lblOrderItem.text=[NSString stringWithFormat:@"%@ Items",[arrOrderDetail valueForKey:@"items"]];
    _lblPaymentDesc.text=[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"payment"]];
    _lblOrderDate.text=[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"date"]];
    if ([_strStatus isEqualToString:@"Order is Delivered"]) {
        _imgOrder.image=[UIImage imageNamed:@"img_ordercomplete.png"];
    }else
    {
        _imgOrder.image=[UIImage imageNamed:@"img_orderprocess.png"];
    }
    _lblUserName.text=[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"customer_name"]];
    _lblAddress.text=[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"address"]];
    _lblMobileNumber.text=[NSString stringWithFormat:@"%@",[arrOrderDetail valueForKey:@"phone"]];

    

    
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        _imgCall.image=[_imgCall.image imageFlippedForRightToLeftLayoutDirection];
        _imgViewMap.image=[_imgViewMap.image imageFlippedForRightToLeftLayoutDirection];
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        _lblOrderDate.textAlignment = UITextAlignmentRight;
  
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
- (IBAction)didClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrItemName.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *lblCateName=(UILabel *) [cell.contentView viewWithTag:102];
    UILabel *lblQty=(UILabel *) [cell.contentView viewWithTag:103];
    UILabel *lblQtyNo=(UILabel *) [cell.contentView viewWithTag:104];
    UILabel *lblBasic=(UILabel *) [cell.contentView viewWithTag:105];
    UILabel *lblBasicPrice=(UILabel *) [cell.contentView viewWithTag:106];

    NSDictionary *dic = [arrItemName objectAtIndex:indexPath.row];
    
  
    //set Font
    lblCateName.font = [UIFont fontWithName:RFontRegular size:lblCateName.font.pointSize];
    lblQty.font = [UIFont fontWithName:RFontRegular size:lblQty.font.pointSize];
    lblQtyNo.font = [UIFont fontWithName:RFontRegular size:lblQtyNo.font.pointSize];
    lblBasic.font = [UIFont fontWithName:RFontRegular size:lblBasic.font.pointSize];
    lblBasicPrice.font = [UIFont fontWithName:RFontRegular size:lblBasicPrice.font.pointSize];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //setText
    
    lblCateName.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]];
    lblQtyNo.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"qty"]];
    lblBasicPrice.text = [NSString stringWithFormat:@"%@%@",dollar,[dic valueForKey:@"amount"]];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    OrderDetail *next =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetail"];
//    next.orderId = [arrOrderNo objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:next animated:YES];
    
}
#pragma mark - POST Methods
-(void)POST_Order:(NSMutableDictionary *)parameters webserviceName:(NSString *)serviceName flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",serviceName]];
        
        NSLog(@"URL : %@",serviceName);
        
        for (__strong NSString *string in parameters.allValues)
        {
            string = [string stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        }
        
        NSLog(@"parameters :: %@",parameters);
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPShouldHandleCookies:NO];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        for (NSString *param in parameters)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //Close off the request with the boundary
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        [request setHTTPBody:body];
        [request setTimeoutInterval:30.0];
        
        
        if ([flag isEqualToString:@"2"])
        {
            _connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_connection2 start];
        }
    }
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
#pragma mark - Get Data from Server
-(void)retriveJSON:(NSString *)url flag:(NSString *)flag
{
    _view_loading.hidden = NO;
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
        //        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"OK"];
    }
    else
    {
        NSString *strUrl = url;
        NSLog(@"url : %@",strUrl);
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        
        if ([flag isEqualToString:@"1"])
        {
            connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        
    }
}
#pragma mark - NSURLConnection Delegate Methods
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error : %@",error);
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
    _view_loading.hidden = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == connection1)
    {
        receivedData1 = [[NSMutableData alloc]init];
        [receivedData1 setLength:0];
    }
   
    else if (connection == _connection2)
    {
        _receivedData2 = [NSMutableData new];
        [_receivedData2 setLength:0];
    }
    
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == connection1)
    {
        [receivedData1 appendData:data];
    }
    else
    {
        [_receivedData2 appendData:data];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Get JSON data here
    NSError *error;
    if (connection == connection1)
    {
        if (receivedData1 != nil)
        {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Json :: %@, Error :: %@",json,error);
            
            if ([[json valueForKey:@"success"] isEqualToString:@"1"]) {
                arrOrderDetail =[[NSMutableArray alloc]init];
                arrOrderDetail=[json valueForKey:@"Order"];
                
                [self setText];
                arrItemName = [[NSMutableArray alloc]init];
                arrItemName=[arrOrderDetail valueForKey:@"item_name"];
                NSLog(@"arrAssigned %@",arrItemName);
                [_tableView reloadData];
            }
            
            else
            {
                Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No order details" duration:3.0];
                [self.view addSubview:snackbar];
                
            }
        }
        else
        {
            Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
            [self.view addSubview:snackbar];
        }
    }
    else if (connection == _connection2)
    {
        if (_receivedData2 != nil)
        {
            
            NSError *error;
            NSMutableDictionary *dictData = [NSJSONSerialization JSONObjectWithData:_receivedData2 options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"CheckJson %@",dictData);
            if ([[dictData valueForKey:@"success"]isEqualToString:@"1"]) {
                
                if ([_strStatus isEqualToString:@"Order is processing"]) {
                    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                    [alert addButton:@"OK" actionBlock:^{
                        
                        
                        [_btnPick setTitle:@"COMPLETE" forState:UIControlStateNormal];
                        [_btnPick setBackgroundColor:[UIColor blackColor]];
                    }];
                    NSLog(@"Response1212 : %@",dictData);
                    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:nil subTitle:[NSString stringWithFormat:@"Your assigned order is Picked for delivery."] closeButtonTitle:nil duration:0.0];
                    
                    
                }
                else if ([_strStatus isEqualToString:@"Order is out for delivery"])
                {
                    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
                    [alert addButton:@"OK" actionBlock:^{
                        _btnPick.hidden=YES;
                    }];
                    NSLog(@"Response1212 : %@",dictData);
                    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:nil subTitle:[NSString stringWithFormat:@"Your assigned order is completed."] closeButtonTitle:nil duration:0.0];
                }
                else if([_strStatus isEqualToString:@"Order is Delivered"])
                {
                    _btnPick.hidden=YES;
                }
                
            }
            
        }
    }
  
    
    _view_loading.hidden = YES;
}

@end
