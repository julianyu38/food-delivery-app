//
//  OrderDetail.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
#import "Snackbar.h"
#import "Reachability.h"
#import <MapKit/MapKit.h>

@interface OrderDetail : UIViewController<UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSMutableData *receivedData1;
    NSURLConnection *connection1;
    
}

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *btnPick;
@property (nonatomic, retain) NSString *strStatus;
@property (retain, nonatomic) NSURLConnection *connection2;
@property (retain, nonatomic) NSMutableData *receivedData2;

@property (nonatomic, retain) NSString *orderId;


@property (weak, nonatomic) IBOutlet UIView *view_loading;

//UIImage
@property (weak, nonatomic) IBOutlet UIImageView *imgOrder;
@property (weak, nonatomic) IBOutlet UIImageView *imgCall;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMap;


//UILabel
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOderAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderItem;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCall;
@property (weak, nonatomic) IBOutlet UILabel *lblViewMap;

//UITabelView
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//UIButton
@property (weak, nonatomic) IBOutlet UIButton *btnBack;




@end
