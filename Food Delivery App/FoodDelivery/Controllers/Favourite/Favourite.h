//
//  Favourite.h
//  FoodDelivery
//
//  Created by Redixbit on 31/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLFile.h"
#import "Detail.h"
#import <CoreLocation/CoreLocation.h>
#import "SCLAlertView.h"

@interface Favourite : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>
{
    IBOutlet UIView *viewLoading;
    IBOutlet UITableView *tblFav;
    NSMutableArray *arrFav;
    CLLocationManager *locationManager;
    
    CGPoint touchPoint;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
