//
//  Favourite.m
//  FoodDelivery
//
//  Created by Redixbit on 31/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Favourite.h"

@interface Favourite ()
{
    NSString *language;
    

}

@end

@implementation Favourite

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self set_Language];

    viewLoading.hidden = NO;
    tblFav.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}
#pragma mark - SET LANGUAGE

-(void)set_Language
{
    
    _lblTitle.text=NSLocalizedString(@"lblFavourites", @"");
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
       [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"en");
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
    [self retriveDataFromDatabase];
}

#pragma mark - CLLocationManage Delegate Method
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [self showAlertView:@"Error" message:@"Failed to Get Your Location" cancelButtonTitle:@"OK"];
    locationManager=nil;
    viewLoading.hidden = YES;
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;

    if (currentLocation != nil)
    {
        [locationManager stopUpdatingLocation];
        locationManager = nil;
        viewLoading.hidden = YES;
//        [tblFav reloadData];
//        [self Add_Distance:arrFav Location:newLocation];
//        [tblFav reloadData];
//        NSLog(@"%@",arrFav);
    }
}

#pragma mark - Calculate Distance
-(void)Add_Distance:(NSMutableArray *)arr_fav Location:(CLLocation *)newLocation
{
    for (int i=0; i<[arr_fav count]; i++)
    {
        NSMutableDictionary *temp = [arr_fav objectAtIndex:i];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:[[temp objectForKey:@"Latitude"] doubleValue] longitude:[[temp objectForKey:@"Longitude"]doubleValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:newLocation];
        
        NSLog(@"Calculated Miles %@", [NSString stringWithFormat:@"%.1f mi",distance]);
        
        [temp setValue:[NSString stringWithFormat:@"%0.02f",distance] forKey:@"restDist"];
        
        [arrFav replaceObjectAtIndex:i withObject:temp];
    }
    
}

#pragma mark - Button action
//Back Button
- (IBAction)btn_back:(UIButton *)sender
{
    if ([language isEqualToString:country])
    {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Retrive Data From Database
-(void)retriveDataFromDatabase
{
    SQLFile *new =[[SQLFile alloc]init];
    
    NSString *querynew =[NSString stringWithFormat:@"select * from Fav"];
    
    arrFav =[new select_favou:querynew];
    NSLog(@"%@",arrFav);
    if ([arrFav count]<1)
    {
        [self showAlertView:@"Info" message:@"No Favourites" cancelButtonTitle:@"OK"];
    }
    [tblFav reloadData];
}

#pragma mark - UITableView Datasource and Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrFav count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *image_fav = (UIImageView *)[cell.contentView viewWithTag:456];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
                image_fav.image=[image_fav.image imageFlippedForRightToLeftLayoutDirection];

    }
    else
    {
        NSLog(@"en");
    }
    UILabel *rest_name=(UILabel *)[cell.contentView viewWithTag:101];
    rest_name.text=[[arrFav valueForKey:@"restName"] objectAtIndex:indexPath.row];
    
    UILabel *rest_Addres=(UILabel *)[cell.contentView viewWithTag:102];
    rest_Addres.text=[[arrFav valueForKey:@"restAddress"] objectAtIndex:indexPath.row];
    
    UILabel *rest_name_first = (UILabel *)[cell.contentView viewWithTag:103];
    rest_name_first.text = [rest_name.text substringToIndex:1];
    
    UILabel *distance = (UILabel *)[cell.contentView viewWithTag:104];
    distance.text = [NSString stringWithFormat:@"%@ %@",[[arrFav valueForKey:@"restDetails"] objectAtIndex:indexPath.row],NSLocalizedString(@"lblkm", @"")];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.5f];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    [animation setType:@"rippleEffect"];
    [cell.layer addAnimation:animation forKey:nil];
    
    Detail *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    detail.restaurantID = [[arrFav valueForKey:@"restId"] objectAtIndex:indexPath.row];
    detail.strScreen= @"fav";
    detail.distance =[[arrFav valueForKey:@"restDetails"] objectAtIndex:indexPath.row];

    touchPoint = [cell convertPoint:CGPointZero toView:self.view];
    touchPoint.x = tblFav.center.x / 2;
    touchPoint.y = touchPoint.y + cell.frame.size.height /2;
    
    [self performSelector:@selector(gotoDetailsView:) withObject:detail afterDelay:0.5];
}

#pragma mark - Goto Detail View
-(void)gotoDetailsView:(UIViewController *)next
{
    CATransition *transition = [CATransition animation];
    transition.duration = transitionDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    transition.type = kCATransitionFade;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    next.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    next.view.center = touchPoint;
    [UIView animateWithDuration:0.5f animations:^{
        next.view.transform = CGAffineTransformIdentity;
        next.view.center = self.view.center;
    } completion:nil];
    
    [self.navigationController pushViewController:next animated:NO];
}

@end
