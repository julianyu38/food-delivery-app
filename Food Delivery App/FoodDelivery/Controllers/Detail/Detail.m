//
//  Detail.m
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Detail.h"
#import "Snackbar.h"
#import "SeachByMap.h"
#import "MostRatedRrequsturant.h"

@interface Detail ()
{
    NSString *language;
    CLLocation *currentLocation;
}

@end

@implementation Detail
@synthesize locationManager = locationManager_;

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    viewLoading.hidden = NO;
    viewBottom.hidden = YES;
    viewScroll.hidden = YES;
    btnFav.hidden = YES;
    favImage.hidden = YES;
    
   
   
    language = [[NSLocale preferredLanguages] firstObject];
    
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        
//                _imaLogout.image=[_imaLogout.image imageFlippedForRightToLeftLayoutDirection];
         [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [_btnShare setImage:[_btnShare.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [btnMap setImage:[btnMap.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [btnContact setImage:[btnContact.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [_btnMenu setImage:[_btnMenu.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [_btnReview setImage:[_btnReview.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"en");
    }
    

    if (iPhoneVersion == 4)
    {
        space1=0;
        space=10;
        labelSpace = 5;
        vrtclHeight=20;
        fontSize=9.0f;
    }
    else if (iPhoneVersion == 5)
    {
        space1=0;
        space=10;
        labelSpace = 5;
        vrtclHeight=20;
        fontSize = 10.0f;
    }
    else if (iPhoneVersion==10)
    {
        space = 12;
        space1=0;
        labelSpace = 5;
        vrtclHeight=-57;
        fontSize=13.0;
    }
    else
    {
        space1=-50;
        space=25;
        labelSpace = 10;
        vrtclHeight=-120;
        fontSize=21.0;
    }
    [self CheckFavourite];
    viewScroll.delegate=self;
    _Mymap.delegate =self;
    _Mymap.mapType = MKMapTypeStandard;
    _Mymap.showsUserLocation = NO;
    
//    NSString *strUrl = [NSString stringWithFormat:@"%@getrestaurantdetail.php?res_id=%@&lat=0.0&lon=0.0",SERVERURL,self.restaurantID];
//    [self retriveJSON:strUrl];
//    http://192.168.1.112/freak/FoodDeliverySystem/api/getrestaurantdetail.php?res_id={}&lat={}&lon={}
    NSString *strUrl = [NSString stringWithFormat:@"%@getrestaurantdetail.php?res_id=%@&lat=%@&lon=%@",SERVERURL,_restaurantID,_lat,_lon];
    [self retriveJSON:strUrl];

    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleLeftSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
//    locationManager_ = [[CLLocationManager alloc] init];
//    locationManager_.delegate = self;
//    locationManager_.distanceFilter = kCLDistanceFilterNone;
//    locationManager_.desiredAccuracy = kCLLocationAccuracyBest;
//    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//    {
//        [locationManager_ requestWhenInUseAuthorization];
//    }
//    [locationManager_ startUpdatingLocation];
    
//    [self displayData:_dicRestDetail];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:NO];
    
    [self.connection1 cancel];
    self.connection1 = nil;
}

#pragma mark - Map delegate Method
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != _Mymap.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[_Mymap dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        //pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.canShowCallout = YES;
        //pinView.animatesDrop = YES;
        pinView.image = [UIImage imageNamed:@"map_pin.png"];    //as suggested by Squatch
    }
    else
    {
        [_Mymap.userLocation setTitle:@"I am here"];
    }
    return pinView;
}



#pragma mark - UIGestureRecognizer Handler Method
-(void)handleLeftSwipe:(UISwipeGestureRecognizer *)SwipeGestureRecognizer
{
    [self btn_back_click:nil];
}

#pragma mark - Set UILable Text
-(void)setLanguage
{
    lblAddress.text = NSLocalizedString(@"lblAddress", @"");
    lblPhone.text = NSLocalizedString(@"lblPhone", @"");
    lblTiming.text = NSLocalizedString(@"lblTiming", @"");
    lblFoodType.text = NSLocalizedString(@"lblFoodType", @"");
    lblDelTime.text = NSLocalizedString(@"lblDeliveryTime", @"");
    lblDelType.text = NSLocalizedString(@"lblDeliveryType", @"");
    _lblMenu.text = NSLocalizedString(@"lblMenu", @"");
    _lblReview.text = NSLocalizedString(@"lblReview", @"");

}

#pragma mark - Reachability
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}

#pragma mark - Retrive Data from webservice
-(void)retriveJSON:(NSString *)url
{
    //checks internet connection
    if (![self connected])
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"No internet, please check your network connection and try again." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Connection Problem" message:@"Internet connection is not found" cancelButtonTitle:@"Ok"];
    }
    else
    {
        viewLoading.hidden = NO;
        NSString *strUrl = url;
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        [request setTimeoutInterval:30.0f];
        self.connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [self.connection1 start];
    }
}
#pragma mark - NSURLConnection Delegate Method
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData1 = [NSMutableData new];
    [_receivedData1 setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData1 appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error);
    viewLoading.hidden = YES;
    Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
    [self.view addSubview:snackbar];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    if (_receivedData1 != nil)
    {
        json = [NSJSONSerialization JSONObjectWithData:_receivedData1 options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"Json :: %@, Error : %@",json, error);
        if (error)
        {
            viewScroll.hidden = YES;
            viewBottom.hidden = YES;
            btnFav.hidden = YES;
            return;
        }
        [self displayData:[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] ];
    }
    else
    {
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
//        [self showAlertView:@"Failed" message:@"Failed to retrive data from server" cancelButtonTitle:@"OK"];
    }
}

#pragma mark - Displaying Detail UIViewController
-(void)displayData:(NSMutableDictionary *)jsonData
{
    
    UILabel *restName = (UILabel *)[self.view viewWithTag:11];
    [restName setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"name"]]];
    
    
    TitleName = restName.text;
    restCurrency = [NSString stringWithFormat:@"%@",[jsonData valueForKey:@"currency"]];
    latitude = [NSString stringWithFormat:@"%@",[jsonData valueForKey:@"lat"]];
    longitude = [NSString stringWithFormat:@"%@",[jsonData valueForKey:@"lon"]];
    
    UILabel *restName1 = (UILabel *)[self.view viewWithTag:61];
    [restName1 setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"Category_Name"]]];
//    [restName1 setText:[NSString stringWithFormat:@"%@",[[jsonData valueForKey:@"category"] componentsJoinedByString:@", "]]];
    
    UILabel *restRate = (UILabel *)[self.view viewWithTag:62];
    [restRate setText:[NSString stringWithFormat:@"%0.1f",[[jsonData valueForKey:@"ratting"] floatValue]]];
    
    HCSStarRatingView *rate = (HCSStarRatingView *)[self.view viewWithTag:103];
    rate.value = [[jsonData valueForKey:@"ratting"]floatValue];
    

    UILabel *restDist = (UILabel *)[self.view viewWithTag:63];
    [restDist setText:[NSString stringWithFormat:@"%.2f %@",[_distance floatValue],NSLocalizedString(@"dOpen", @"")]];
   
   
    UILabel *restAddress = (UILabel *)[self.view viewWithTag:64];
    [restAddress setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"address"]]];
    [self set_height:restAddress];

    [self setBars:restAddress Image:imgAddBar];
    
    lblPhone.frame = CGRectMake(lblPhone.frame.origin.x, imgAddBar.frame.origin.y + imgAddBar.frame.size.height + 10, lblPhone.frame.size.width, lblPhone.frame.size.height);
    
    UILabel *restPhone = (UILabel *)[self.view viewWithTag:65];
    restPhone.frame = CGRectMake(restPhone.frame.origin.x, lblPhone.frame.origin.y + lblPhone.frame.size.height, restPhone.frame.size.width, restPhone.frame.size.height);
    [restPhone setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"phone"]]];

    [self setBars:restPhone Image:imgPhoneBar];
    
     UILabel *restDesc = (UILabel *)[self.view viewWithTag:643];
    restDesc.text = [jsonData valueForKey:@"desc"];
    
    lblTiming.frame = CGRectMake(lblTiming.frame.origin.x, imgPhoneBar.frame.origin.y + imgPhoneBar.frame.size.height + 10, lblTiming.frame.size.width, lblTiming.frame.size.height);
    
    UILabel *restTiming = (UILabel *)[self.view viewWithTag:66];
    restTiming.frame = CGRectMake(restTiming.frame.origin.x, lblTiming.frame.origin.y + lblTiming.frame.size.height, restTiming.frame.size.width, restTiming.frame.size.height);
//    "17:00:00 To 15:15:15 :فتح"
    [restTiming setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"dOpen", @""),[jsonData valueForKey:@"time"]]];

    [self setBars:restTiming Image:imgTimingBar];
    
    lblFoodType.frame = CGRectMake(lblFoodType.frame.origin.x, imgTimingBar.frame.origin.y + imgTimingBar.frame.size.height + 10, lblFoodType.frame.size.width, lblFoodType.frame.size.height);
    
    UILabel *restFoodType = (UILabel *)[self.view viewWithTag:67];
    restFoodType.frame = CGRectMake(restFoodType.frame.origin.x, lblFoodType.frame.origin.y + lblFoodType.frame.size.height, restFoodType.frame.size.width, restFoodType.frame.size.height);
    [restFoodType setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"Category"]]];
    [restFoodType setText:[NSString stringWithFormat:@"%@",[[jsonData valueForKey:@"Category"] componentsJoinedByString:@", "]]];
    NSString *restCategoryNames = [[jsonData objectForKey:@"Category"] objectAtIndex:0] ;
    for (int i = 1; i<[[jsonData valueForKey:@"Category"] count]; i++)
    {
        restCategoryNames = [restCategoryNames stringByAppendingString:[NSString stringWithFormat:@", %@",[[jsonData valueForKey:@"Category"] objectAtIndex:i]]];
    }
    [restFoodType setText:restCategoryNames];
    [self setBars:restFoodType Image:imgFoodTypeBar];
    
    lblDelTime.frame = CGRectMake(lblDelTime.frame.origin.x, imgFoodTypeBar.frame.origin.y + imgFoodTypeBar.frame.size.height + 10, lblDelTime.frame.size.width, lblDelTime.frame.size.height);
    
    UILabel *restDelTime = (UILabel *)[self.view viewWithTag:68];
    restDelTime.frame = CGRectMake(restDelTime.frame.origin.x, lblDelTime.frame.origin.y + lblDelTime.frame.size.height, restDelTime.frame.size.width, restDelTime.frame.size.height);
    [restDelTime setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"delivery_time"]]];
    restDeliveryTime = restDelTime.text;
    [self setBars:restDelTime Image:imgDelTimeBar];
    
    lblDelType.frame = CGRectMake(lblDelType.frame.origin.x, imgDelTimeBar.frame.origin.y + imgDelTimeBar.frame.size.height + 10, lblDelType.frame.size.width, lblDelType.frame.size.height);
    lblDelType.text = @"Delivery Charges";
    UILabel *restDelType = (UILabel *)[self.view viewWithTag:69];
    restDelType.frame = CGRectMake(restDelType.frame.origin.x, lblDelType.frame.origin.y + lblDelType.frame.size.height, restDelType.frame.size.width, restDelType.frame.size.height);
    [restDelType setText:[NSString stringWithFormat:@"%@",[jsonData valueForKey:@"delivery_charg"]]];
    
    [self setBars:restDelType Image:imgDelTypeBar];
    
    UIImageView *restImage = (UIImageView *)[self.view viewWithTag:60];
    NSString *Str_image_name = [NSString stringWithFormat:@"%@%@",IMAGEURL,[jsonData valueForKey:@"photo"]];
    Str_image_name = [Str_image_name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [restImage sd_setImageWithURL:[NSURL URLWithString:Str_image_name] placeholderImage:[UIImage imageNamed:@"Rest"] options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize){
        [restImage updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [restImage reveal];
    }];
    CLLocationCoordinate2D loc;
    loc.longitude = [[jsonData valueForKey:@"lon"] floatValue];
    loc.latitude = [[jsonData valueForKey:@"lat"] floatValue];
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = loc;
    annotationPoint.title =@"";
    [_Mymap addAnnotation:annotationPoint];
    
    // Set Default Zomm to Resturant Location
    
    if (iPhoneVersion == 5) {
        W = 10000.0f;
        H = 10000.0f;
    }
    else if (iPhoneVersion == 10)
    {
        W = 10000.0f;
        H = 10000.0f;
    }
    else
    {
        W = 10000.0f;
        H = 10000.0f;
    }
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in _Mymap.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, W, H);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
        
    }
    [_Mymap setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(40, 10, 10, 10) animated:YES];
  

    
    
    _InfoViewHieght.constant=imgDelTypeBar.frame.origin.y+space;
    _VerticalLine.constant=imgDelTypeBar.frame.origin.y-vrtclHeight;

    _topView.constant = 0;
    
    [viewScroll setContentSize:CGSizeMake(viewScroll.frame.size.width, restDesc.frame.origin.y+restDesc.frame.size.height+10)];
    
    [self setPoints];
     [self setLanguage];
    viewBottom.hidden = NO;
    viewScroll.hidden = NO;
    btnFav.hidden = NO;
    favImage.hidden = NO;
    viewLoading.hidden = YES;
}

#pragma mark - Scrollview Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender{
    //executes when you scroll the scrollView
     _topView.constant = -44;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // execute when you drag the scrollView
     _topView.constant = -44;
}
#pragma mark - Set Frame of views
-(void)setBars:(UILabel *)lable Image:(UIImageView *)image
{
    image.frame = CGRectMake(image.frame.origin.x, lable.frame.origin.y + lable.frame.size.height + 10, image.frame.size.width, image.frame.size.height);
}

-(void)setPoints
{
    [self setPoint:lblAddress Image:imgAddP];
    [self setPoint:lblPhone Image:imgPhoneP];
    [self setPoint:lblTiming Image:imgTimingP];
    [self setPoint:lblFoodType Image:imgFoodTypeP];
    [self setPoint:lblDelTime Image:imgDelTimeP];
    [self setPoint:lblDelType Image:imgDelTypeP];
    
    imgVertical.frame = CGRectMake(imgVertical.frame.origin.x, imgVertical.frame.origin.y, imgVertical.frame.size.width, imgDelTypeBar.frame.origin.y - imgVertical.frame.origin.y + 1);
    
    btnMap.frame = CGRectMake(btnMap.frame.origin.x, lblAddress.frame.origin.y + 10, btnMap.frame.size.width, btnMap.frame.size.height);
    btnContact.frame = CGRectMake(btnContact.frame.origin.x, lblPhone.frame.origin.y, btnContact.frame.size.width, btnContact.frame.size.height);
    viewLoading.hidden = YES;
}

-(void)setPoint:(UILabel *)label Image:(UIImageView *)imageView
{
    
    imageView.frame = CGRectMake(imageView.frame.origin.x, label.frame.origin.y, imageView.frame.size.width, imageView.frame.size.height);
    imageView.center = CGPointMake(imageView.center.x, label.center.y);
}

-(void)set_height:(UILabel *)lbl
{
    CGSize maximumLabelSize = CGSizeMake(lbl.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize:maximumLabelSize lineBreakMode:lbl.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.height = expectedLabelSize.height + 5;
    lbl.frame = newFrame;
}

#pragma mark - Button Actions
//Favourite
-(IBAction)btn_fav:(id)sender
{
//    NSLog(@"%@",[json objectAtIndex:0]);
    NSString *passingstr;
    if (isFavourite == NO)
    {
        SQLFile *new=[[SQLFile alloc]init];
        
        passingstr = [NSString stringWithFormat:@"insert into Fav values('%@','%@','%@','%@','%@','%.2f')",self.restaurantID,[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"name"],[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"address"],[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"lat"],[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"lon"],[_distance floatValue]];
        if ([new operationdb:passingstr]==YES)
        {
            NSLog(@"Added to favourite");
            [self initWithTitle:@"Added to favourite" duration:0.5f];
        }
        else
        {
            NSLog(@"Not added to favourite");
        }
        isFavourite = YES;
        favImage.image = [UIImage imageNamed:@"fav.png"];
    }
    else
    {
        SQLFile *new=[[SQLFile alloc]init];
        passingstr = [NSString stringWithFormat:@"DELETE FROM Fav WHERE restId ='%@'",self.restaurantID];
        if ([new operationdb:passingstr]==YES)
        {
            NSLog(@"Remove From Favourite");
        }
        isFavourite = NO;
        favImage.image = [UIImage imageNamed:@"unfav.png"];
    }
}

//Back
- (IBAction)btn_back_click:(UIButton *)sender
{
    if ([_strScreen isEqualToString:@"home"]) {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
    }
    else if ([_strScreen isEqualToString:@"map"])
    {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            SeachByMap *List = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachByMap"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            SeachByMap *List = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachByMap"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
    }
    else if([_strScreen isEqualToString:@"mostRated"])
    {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            MostRatedRrequsturant *List = [self.storyboard instantiateViewControllerWithIdentifier:@"MostRatedRrequsturant"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            MostRatedRrequsturant *List = [self.storyboard instantiateViewControllerWithIdentifier:@"MostRatedRrequsturant"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
    }
    else if([_strScreen isEqualToString:@"fav"])
    {
        if ([language isEqualToString:country]) {
            NSLog(@"ar");
            Favourite *List = [self.storyboard instantiateViewControllerWithIdentifier:@"Favourite"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
        else
        {
            NSLog(@"en");
            Favourite *List = [self.storyboard instantiateViewControllerWithIdentifier:@"Favourite"];
            CATransition* transition = [CATransition animation];
            transition.duration = transitionDuration;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:List animated:YES];
            
        }
    }
   
}

//Review Button
- (IBAction)btn_review:(UIButton *)sender
{
    Review *review = [self.storyboard instantiateViewControllerWithIdentifier:@"Review"];
    review.restId = self.restaurantID;
    review.lat=_lat;
    review.lon = _lon;
    
    [self.navigationController pushViewController:review animated:YES];
}

//Menu Button
-(IBAction)btn_menu:(UIButton *)sender
{
  
    
    Menu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    menu.restID = _restaurantID;
    menu.restName = TitleName;
    menu.restCurrency = restCurrency;
    menu.restDelTime = restDeliveryTime;
    menu.currency = [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"currency"];
 
    [self.navigationController pushViewController:menu animated:YES];
}

//View Location on map
//Draw Rout from current location to Restaurant
-(IBAction)btnLocation:(id)sender
{
    NSLog(@"Latitude :: %f, Longitude :: %f",[latitude floatValue],[longitude floatValue]);
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake([latitude floatValue], [longitude floatValue]);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    endingItem.name = TitleName;
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem openInMapsWithLaunchOptions:launchOptions];
    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/maps?q=%f,%f",[latitude floatValue], [longitude floatValue]]];
//    [[UIApplication sharedApplication] openURL:url];
}

//Call Option
- (IBAction)Btn_Call:(UIButton *)sender
{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"phone"]]];
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]])
    {
//        [self showAlertView:@"Warning" message:@"Can not make call" cancelButtonTitle:@"OK"];
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Can't make call from this device." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}
- (IBAction)didClickShareAppButton:(id)sender
{
    if ([UIAlertController class])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Share this app on !" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *facebook = [UIAlertAction actionWithTitle:NSLocalizedString(@"Facebook",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self Facebook];}];
        [alertController addAction:facebook];
        
        UIAlertAction *twitter = [UIAlertAction actionWithTitle:NSLocalizedString(@"Twitter",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self Twitter];}];
        [alertController addAction:twitter];
        
        UIAlertAction *whatsApp = [UIAlertAction actionWithTitle:NSLocalizedString(@"WhatsApp",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self Whatsapp];}];
        [alertController addAction:whatsApp];
        
        UIAlertAction *mail = [UIAlertAction actionWithTitle:NSLocalizedString(@"Mail",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self sendMail];}];
        [alertController addAction:mail];
        
        UIAlertAction *text = [UIAlertAction actionWithTitle:NSLocalizedString(@"Text Message",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self sendMessage];}];
        [alertController addAction:text];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:cancel];
        
        [alertController setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
        popPresenter.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share this app on !" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Facebook",@""),NSLocalizedString(@"Twitter",@""),NSLocalizedString(@"WhatsApp",@""),NSLocalizedString(@"Mail",@""),@"Text Message", nil];
        [actionSheet showInView:self.view];
    }
}
#pragma mark - Check Favourite Or Not [From Database]
-(void)CheckFavourite
{
    SQLFile *new =[[SQLFile alloc]init];
    NSString *querynew =[NSString stringWithFormat:@"select * from Fav where restId = %@",self.restaurantID];
    NSMutableArray *arr_fav = [new select_favou:querynew];
    if (arr_fav.count > 0)
    {
        isFavourite = YES;
        favImage.image = [UIImage imageNamed:@"fav.png"];
    }
    else
    {
        isFavourite = NO;
        favImage.image = [UIImage imageNamed:@"unfav.png"];
    }
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Favourite Alert[Snackbar]
-(void)initWithTitle:(NSString *)title duration:(CGFloat)duration
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(30, [UIScreen mainScreen].bounds.size.height - 150, [UIScreen mainScreen].bounds.size.width-60, 30)];
    view.alpha = 0.0;
    [view.layer setMasksToBounds:YES];
    view.layer.cornerRadius = view.frame.size.height / 2;
    if (self)
    {
        view.backgroundColor = [UIColor orangeColor];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, view.frame.size.width-20, view.frame.size.height)];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.text = title;
        [label setFont:[UIFont fontWithName:@"WorkSans-Regular" size:12.0f]];
//        [label setFont:[UIFont boldSystemFontOfSize:12.0]];
        [view addSubview:label];
    }
    [self.view addSubview:view];
    [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        view.alpha = 1.0;
    } completion:^(BOOL finished){
        if (finished)
        {
            [self performSelector:@selector(dismissSnackbar:) withObject:view afterDelay:1.0f];
        }
    }];
}

-(void)dismissSnackbar:(UIView *)view
{
    view.alpha = 1.0;
    [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished){
        if (finished)
        {
            [view removeFromSuperview];
        }
    }];
}
#pragma mark - Social Sharing
// Share With Whatsapp
- (void)Whatsapp
{
    
    NSString * msg = [NSString stringWithFormat:@"*%@*\n%@\n%@\nShare From : \n%@\n%@",[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"name"],
        [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"address"],
        [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"phone"],APP_NAME,APP_URL];
    
    msg = [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    msg = [msg stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    msg = [msg stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    msg = [msg stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    msg = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL:whatsappURL options:@{UIApplicationOpenURLOptionUniversalLinksOnly : @NO} completionHandler:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WhatsApp not installed.",@"") message:NSLocalizedString(@"Your device has no WhatsApp installed.", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
        [alert show];
    }
}

// Share With FaceBook
- (void)Facebook
{
    NSString * msg = [NSString stringWithFormat:@"%@\n%@\n%@\nShare From : \n%@",[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"name"],
                      [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"address"],
                      [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"phone"],APP_NAME];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL=[NSURL URLWithString:APP_URL];
    content.quote =msg;
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
//    SLComposeViewController *mySLcomposerSheet;
//    mySLcomposerSheet = [[SLComposeViewController alloc]init];
//    mySLcomposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//    [mySLcomposerSheet setInitialText:[NSString stringWithFormat:@"%@",msg]];
//    [mySLcomposerSheet addURL:[NSURL URLWithString:[[json objectAtIndex:0] valueForKey:@"Resturent_URL"]]];
//    [mySLcomposerSheet setAccessibilityElementsHidden:YES];
//    [mySLcomposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
//
//        switch (result)
//        {
//            case SLComposeViewControllerResultCancelled:
//                NSLog(@"Post Canceled");
//                break;
//            case SLComposeViewControllerResultDone:
//                NSLog(@"Post Sucessful");
//                break;
//
//            default:
//                break;
//        }
//    }];
//    [self presentViewController:mySLcomposerSheet animated:YES completion:NULL];
}

// Share With Twitter
- (void)Twitter
{
    SLComposeViewController *mySLcomposerSheet;
    mySLcomposerSheet = [[SLComposeViewController alloc]init];
    mySLcomposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [mySLcomposerSheet setInitialText:[NSString stringWithFormat:@"%@",[[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"name"]]];
    [mySLcomposerSheet addURL:[NSURL URLWithString:APP_URL]];
    [mySLcomposerSheet setAccessibilityElementsHidden:YES];
    [mySLcomposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result)
        {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                NSLog(@"Post Sucessful");
                break;
                
            default:
                break;
        }
    }];
    [self presentViewController:mySLcomposerSheet animated:YES completion:NULL];
}
// Send Mail
-(void)sendMail
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        [mailCont setSubject:APP_NAME];
        
//        NSString * msg = [NSString stringWithFormat:@"*%@*\n%@\n%@\n%@\nShare From : \n%@\n%@",[[json objectAtIndex:0] valueForKey:@"Resturant_Name"],[[json objectAtIndex:0] valueForKey:@"Resturent_Address"],[[json objectAtIndex:0] valueForKey:@"Resturant_Phone"],[[json objectAtIndex:0] valueForKey:@"Resturent_URL"],APP_NAME,APP_URL];
        NSString * msg = [NSString stringWithFormat:@"<b>%@</b></br>%@</br>%@</br></br></br>Share From : <b>%@</b></br>Get this app at : %@",
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"name"],
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"address"],
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"phone"],
                          APP_NAME,APP_URL];
        
        [mailCont setMessageBody:msg isHTML:YES];
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}
//Send text message
-(void)sendMessage
{
    if([MFMessageComposeViewController canSendText])
    {
        NSString * msg = [NSString stringWithFormat:@"%@\n%@\n%@\nShare From : %@\nGet this app at : %@",
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"]valueForKey:@"name"],
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"address"],
                          [[[json objectAtIndex:0] valueForKey:@"Restaurant_Detail"] valueForKey:@"phone"],APP_NAME,APP_URL];
        MFMessageComposeViewController *messageCont = [[MFMessageComposeViewController alloc] init];
        messageCont.messageComposeDelegate = self;
        [messageCont setBody:[NSString stringWithFormat:@"%@",msg]];
        [self presentViewController:messageCont animated:YES completion:nil];
    }
}
#pragma mark - MFMessageComposeViewController Delegate Method
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - mail compose delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
