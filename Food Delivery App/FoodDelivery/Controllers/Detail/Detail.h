//
//  Detail.h
//  FoodDelivery
//
//  Created by Redixbit on 26/08/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import "SCLAlertView.h"
#import "Constants.h"
#import "Review.h"
#import "Reachability.h"
#import "UIImageView+RJLoader.h"
#import "UIImageView+WebCache.h"
#import "SQLFile.h"
#import "Menu.h"
#import <MapKit/MapKit.h>

@interface Detail : UIViewController<UIScrollViewDelegate,CLLocationManagerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,MKMapViewDelegate,UIScrollViewDelegate>
{
    BOOL isFavourite;
    float fontSize;
    NSMutableArray *json;
    NSString *TitleName;
    NSString *restCurrency;
    NSString *latitude;
    NSString *longitude;
    NSString *restDeliveryTime;
    CLLocationManager *locationManager;

    __weak IBOutlet UIView *viewLoading;
    __weak IBOutlet UIView *viewBottom;
    __weak IBOutlet UIScrollView *viewScroll;
    __weak IBOutlet UIImageView *favImage;
    __weak IBOutlet UIButton *btnFav;
    __weak IBOutlet UIButton *btnMap;
    __weak IBOutlet UIButton *btnContact;
    __weak IBOutlet UILabel *lblAddress;
    __weak IBOutlet UILabel *lblPhone;
    __weak IBOutlet UILabel *lblTiming;
    __weak IBOutlet UILabel *lblFoodType;
    __weak IBOutlet UILabel *lblDelTime;
    __weak IBOutlet UILabel *lblDelType;
    __weak IBOutlet UIImageView *imgAddBar;
    __weak IBOutlet UIImageView *imgPhoneBar;
    __weak IBOutlet UIImageView *imgTimingBar;
    __weak IBOutlet UIImageView *imgFoodTypeBar;
    __weak IBOutlet UIImageView *imgDelTimeBar;
    __weak IBOutlet UIImageView *imgDelTypeBar;
    __weak IBOutlet UIImageView *imgAddP;
    __weak IBOutlet UIImageView *imgPhoneP;
    __weak IBOutlet UIImageView *imgTimingP;
    __weak IBOutlet UIImageView *imgFoodTypeP;
    __weak IBOutlet UIImageView *imgDelTimeP;
    __weak IBOutlet UIImageView *imgDelTypeP;
    __weak IBOutlet UIImageView *imgVertical;
     CGFloat space,space1, labelSpace,vrtclHeight;
    CGFloat W;
    CGFloat H,detailAddX;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;


@property (nonatomic,retain) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIView *InfoView;

@property (weak, nonatomic) IBOutlet MKMapView *Mymap;
@property (nonatomic,retain) NSString *restaurantID;
//@property (nonatomic, retain) NSMutableDictionary *dicRestDetail;
@property (nonatomic,retain) NSString *lon;
@property (nonatomic,retain) NSString *lat;
@property (nonatomic,retain) NSString *distance;

@property (nonatomic,retain) NSString *strScreen;

@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *InfoViewHieght;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *VerticalLine;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;

@end
