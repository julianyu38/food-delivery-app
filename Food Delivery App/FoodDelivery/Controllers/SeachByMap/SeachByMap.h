//
//  SeachByMap.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 26/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKMapView+ARHelpers.h"
#import "WebServiceHelper.h"
#import "SBJson.h"
#import "Snackbar.h"
#import "List.h"
#import "Reachability.h"



@interface SeachByMap : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate>
{
    NSString *latitude, *longitude, *localTimeZone;
    BOOL isFlagMap;
    int annotationTag;
    CGPoint touchPoint;

    NSString *ann_Name;
    NSString *ann_Address;
    BOOL _isUserLocation;
    BOOL _isNewLocation;
    CLLocationManager *locationManager;

}
@property (retain, nonatomic) NSURLConnection *connection1;
@property (retain, nonatomic) NSMutableData *receivedData1;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) NSString *Ns_Lattitude;
@property (strong, nonatomic) NSString *Ns_Longitude;
@property (strong, nonatomic) IBOutlet MKMapView *myMap;
@property (weak, nonatomic) IBOutlet UIView *view_loading;
@property (strong, nonatomic) IBOutlet NSMutableArray *arrData;
@property (weak, nonatomic) IBOutlet UIButton *btnLoadMore;

@property (nonatomic,retain) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@end
