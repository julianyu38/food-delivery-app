//
//  callOutView.h
//  Coupan
//
//  Created by Redixbit IPhone on 09/11/16.
//  Copyright © 2016 freaktemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface callOutView : UIView
@property (strong, nonatomic) IBOutlet UILabel *lbl_HotelName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_HotelCate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_OpenOrNot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Minute;
@property (weak, nonatomic) IBOutlet UILabel *lblRatting;
@property (weak, nonatomic) IBOutlet UIImageView *imgRest;

@end
