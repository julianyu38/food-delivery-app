//
//  MyAnnotation.h
//  Coupan
//
//  Created by Redixbit IPhone on 09/11/16.
//  Copyright © 2016 freaktemplate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject<MKAnnotation>
{
    NSString *title,*cate,*min,*rat,*openornot,*restID,*restOpen,*restClose,*restLat,*restlong,*restDistance;
    CLLocationCoordinate2D coordinate;
   
}
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *cate;
@property (nonatomic, copy) NSString *min;
@property (nonatomic, copy) NSString *rat;
@property (nonatomic, copy) NSString *openornot;
@property (nonatomic, copy) NSString *restID;
@property (nonatomic, copy) NSString *restOpen;
@property (nonatomic, copy) NSString *restLat;
@property (nonatomic, copy) NSString *restlong;
@property (nonatomic, copy) NSString *restDistance;
@property (nonatomic, copy) NSString *restClose;




@property (nonatomic, copy) NSMutableDictionary *imageDic;

@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString*)strTitle andAddress:(NSString *)strCate andDistance:(NSString *)strMin  andType:(NSString *)strRate andBId:(NSString *)strRestId andRstatus:(NSString *)strStatus andROpen:(NSString *)strOpen andRClose:(NSString *)strClose andRLat:(NSString *)strLat andRLong:(NSString *)strLong andRDistance:(NSString *)strDistance andCoordinate:(CLLocationCoordinate2D)coord;
@end
