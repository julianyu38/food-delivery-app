//
//  MyAnnotation.m
//  Coupan
//
//  Created by Redixbit IPhone on 09/11/16.
//  Copyright © 2016 freaktemplate. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
@synthesize title,cate,min,rat,openornot,restOpen,restClose,coordinate,restID,restLat,restlong,restDistance;
- (id)initWithTitle:(NSString*)strTitle andAddress:(NSString *)strCate andDistance:(NSString *)strMin andType:(NSString *)strRate andBId:(NSString *)strRestId andRstatus:(NSString *)strStatus andROpen:(NSString *)strOpen andRClose:(NSString *)strClose andRLat:(NSString *)strLat andRLong:(NSString *)strLong andRDistance:(NSString *)strDistance andCoordinate:(CLLocationCoordinate2D)coord
{
    if (self = [super init])
    {
        self.title = strTitle;//[strTitle copy];
        self.cate=strCate;
        self.min=strMin;
        self.rat = strRate;
        self.restID =strRestId;
        self.openornot = strStatus;
        self.restOpen= strOpen;
        self.restClose= strClose;
        self.restLat=strLat;
        self.restlong=strLong;
        self.restDistance=strDistance;
         self.coordinate = coord;
      }
    return self;
}
-(CLLocationCoordinate2D)coord
{
    return coordinate;
}
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    coordinate = newCoordinate;
    
}
@end
