//
//  SeachByMap.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 26/03/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "SeachByMap.h"
#import "Setting.h"
#import "Color.h"
#import "Detail.h"
#import "MyAnnotation.h"
#import "NSString+MD5.h"
#import "FTWCache.h"
#import "MyAnnotationView.h"
#import "callOutView.h"

@interface SeachByMap ()
{
    int startIndex;
    int pageRow;
    BOOL isSuccess;
    NSMutableArray *city_arr;
    NSString *strCity;
    NSString *strRadius;
    NSMutableDictionary *location;
    NSMutableDictionary *dict_data;
    NSString *language;
    NSString *strLatitude,*strlongitude,*strDis,*strRestId;
     int scale;

        
        
}

@end

@implementation SeachByMap
@synthesize locationManager = locationManager_;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self set_Language];

    self.arrData = [[NSMutableArray alloc]init];
    [_arrData removeAllObjects];
    [self.view addSubview:_view_loading];
    _view_loading.hidden = NO;
    startIndex = listRow;
    pageRow = 1;
    //For Radius

    isFlagMap=YES;
    
    
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    localTimeZone = [timeZone name];
  
    
    self.myMap.delegate =self;
    self.myMap.mapType = MKMapTypeStandard;
    
    self.myMap.showsUserLocation = YES;
_isUserLocation = NO;
    _isNewLocation = NO;
    [self SEtscale];
}
-(void)SEtscale
{
    if (iPhoneVersion == 4)
    {
        scale = 5;
    }
    else if (iPhoneVersion == 5)
    {
        scale = 3;
    }
    else if (iPhoneVersion == 6 || iPhoneVersion == 10)
    {
        scale = 3;
    }
    else if (iPhoneVersion == 61)
    {
        scale = 3;
    }
    else
    {
        scale =2;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [NSUserDefaults standardUserDefaults];
    _view_loading.hidden = NO;

    
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
        {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"])
            {
                [locationManager requestAlwaysAuthorization];
            }
            else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locationManager  requestWhenInUseAuthorization];
            }
            else
            {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManager Delegate Method
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil)
    {
        _isNewLocation = YES;
        _Ns_Longitude =[NSString stringWithFormat:@"%.14f", currentLocation.coordinate.longitude];
        _Ns_Lattitude = [NSString stringWithFormat:@"%.14f", currentLocation.coordinate.latitude];
        NSString *strOnOff = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"OnOff"];
        if ([strOnOff isEqualToString:@"off"]) {
            
            strRadius = @"100000";
        }
        else if ([strOnOff isEqualToString:@"on"]) {
            
            strRadius = [[NSUserDefaults standardUserDefaults]objectForKey:@"Radius"];
            
        }
        else
        {
            strRadius = @"100000";
        }
        
        //For City
        strCity = [[NSUserDefaults standardUserDefaults]objectForKey:@"City_Name"];
        if (strCity.length > 0) {
            NSLog(@"CITY FOUND : %@",strCity);
        }else{
            [self GetData:@"restaurant_city.php"];
        }
        [self rest];
        if (!_isUserLocation)// || _isNewLocation)
        {
            double miles = 8;
            double scalingFactor = ABS( (cos(2 * M_PI * newLocation.coordinate.latitude / 360.0) ));
            
            MKCoordinateSpan span;
            
            span.latitudeDelta = miles/69.0;
            span.longitudeDelta = miles/(scalingFactor * 69.0);
            
            MKCoordinateRegion region;
            region.span = span;
            region.center = newLocation.coordinate;
            
            [_myMap setRegion:region animated:YES];
        }
    }
    [locationManager stopUpdatingLocation];
    locationManager = nil;
}
- (IBAction)didClickBackButton:(id)sender {
    
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
        
    }
    else
    {
        NSLog(@"en");
        List *List = [self.storyboard instantiateViewControllerWithIdentifier:@"List"];
        CATransition* transition = [CATransition animation];
        transition.duration = transitionDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:List animated:YES];
    }
    
}
- (IBAction)didClickLoadMoreButton:(id)sender {
    pageRow=pageRow+1;
    [self rest];
}
- (IBAction)didClickSettingButton:(id)sender {
    Setting *locate = [self.storyboard instantiateViewControllerWithIdentifier:@"Setting"];
    locate.strCheckScreen=@"map";

    [self.navigationController pushViewController:locate animated:YES];
}
- (IBAction)didClickGotoNextButton:(id)sender {
    Detail *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    detail.restaurantID = [dict_data valueForKey:@"id"];
    detail.lat =[dict_data  valueForKey:@"lat"];
    detail.lon =[dict_data  valueForKey:@"lon"];
    detail.distance=[dict_data valueForKey:@"distance"];

    detail.strScreen= @"map";
 
    [self.navigationController pushViewController:detail animated:YES];

}
#pragma mark - SET LANGUAGE
-(void)set_Language
{
    
    _lblTitle.text = NSLocalizedString(@"lblSearchByMap", @"");

    language = [[NSLocale preferredLanguages] firstObject];
    language=[language substringToIndex:character];
    if ([language isEqualToString:country]) {
        NSLog(@"ar");
        ;
        [_btnBack setImage:[_btnBack.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        [_btnLoadMore setImage:[_btnLoadMore.currentImage imageFlippedForRightToLeftLayoutDirection] forState:UIControlStateNormal];
        
    }
    else
    {
        NSLog(@"en");
    }
    
}
#pragma mark - Restaurant Fetching
-(void)rest
{
    [self.view addSubview:_view_loading];
    _view_loading.hidden = NO;
    //
    NSString *urlString;
    
    strCity = [strCity stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
        urlString = [NSString stringWithFormat:@"%@restaurantlist.php?timezone=%@&lat=%@&lon=%@&location=%@&noofrecords=%d&pageno=%d&radius=%@",SERVERURL,localTimeZone,  _Ns_Lattitude,_Ns_Longitude,strCity,startIndex,pageRow,strRadius];
        
   
    NSLog(@"UrlString %@",urlString);
    WebServiceHelper  *obj_add = [WebServiceHelper new];
    
    [obj_add setCurrentCall:urlString];
    [obj_add setMethodName:urlString];
    [obj_add setMethodResult:@"Restaurant_list"];
    [obj_add setDelegate:self];
    [obj_add initiateConnection];
}


-(void)WebServiceHelper:(WebServiceHelper *)editor didFinishWithResult:(BOOL)result
{
    if (result)
    {
        NSMutableArray *resultDic = [[editor ReturnStr] JSONValue];
        [self getSortedArray:resultDic];
    
        
    }
    else
    {
        _view_loading.hidden = YES;
   
    }
}
-(void)getSortedArray:(NSMutableArray *)resultDic
{
    NSString *status = [[resultDic objectAtIndex:0] valueForKey:@"status"];
    MyAnnotation *pinAnnotation;
    CLLocationCoordinate2D coordinate;
    NSMutableDictionary *imagesDictionary = [[NSMutableDictionary alloc] init];

    if ([status isEqualToString:@"Success"]) {
        isSuccess = true;
        NSMutableArray *TempArray = [[resultDic objectAtIndex:0] valueForKey:@"Restaurant_list"];
        if (![TempArray count] > 0) {
            _view_loading.hidden = YES;
           
            
        }else{
            
            
            for (int i=0; i<[TempArray count]; i++) {
                NSMutableDictionary *temp = [TempArray objectAtIndex:i];
               
                [self.arrData addObject:temp];
            }
           
            
            if ([TempArray count]<listRow) {

            }else{
                //            [self addLoadMoreButton];
                NSLog(@"CheckLoadMore");
                //            [self buttonLoadMoreTouched];
            }
            
            _view_loading.hidden = YES;
            for (int i=0 ; i< self.arrData.count; i++)
            {
                dict_data = [self.arrData objectAtIndex:i];
                coordinate.latitude = [[dict_data objectForKey:@"lat"] doubleValue];
                coordinate.longitude = [[dict_data objectForKey:@"lon"] doubleValue];
                
              
                NSString *restCategoryNames = [[dict_data objectForKey:@"Category"]objectAtIndex:0];
                for (int i = 1; i<[[dict_data objectForKey:@"Category"] count]; i++)
                {
                    restCategoryNames = [restCategoryNames stringByAppendingString:[NSString stringWithFormat:@", %@",[[dict_data objectForKey:@"Category"] objectAtIndex:i]]];
                }

                pinAnnotation = [[MyAnnotation alloc] init];
                pinAnnotation.title = [dict_data objectForKey:@"name"];
                pinAnnotation.cate=restCategoryNames;
                pinAnnotation.openornot=[dict_data objectForKey:@"res_status"];
                pinAnnotation.min = [dict_data objectForKey:@"delivery_time"];
                pinAnnotation.rat = [dict_data objectForKey:@"ratting"];
                pinAnnotation.restID=[dict_data objectForKey:@"id"];
                pinAnnotation.restClose=[dict_data objectForKey:@"close_time"];
                pinAnnotation.restOpen=[dict_data objectForKey:@"open_time"];
                pinAnnotation.restLat=[dict_data objectForKey:@"lat"];
                pinAnnotation.restlong=[dict_data objectForKey:@"lon"];
                pinAnnotation.restDistance=[dict_data objectForKey:@"distance"];
                pinAnnotation.coordinate = coordinate;
                NSString *Str_image_name = [NSString stringWithFormat:@"%@%@",IMAGEURL,[dict_data valueForKey:@"image"]];

                UIImageView *imageView = [[UIImageView alloc] init];
                [self loadImageFromURL:Str_image_name imgview:imageView];
                
                NSData *imageData = UIImagePNGRepresentation(imageView.image);
                [imagesDictionary setObject:imageData forKey:[dict_data objectForKey:@"name"]];
                pinAnnotation.imageDic = imagesDictionary;
                [self.myMap addAnnotation:pinAnnotation];
                
            }
            
            // Set Default Zomm to Resturant Location
            MKMapRect zoomRect = MKMapRectNull;
            for (id <MKAnnotation> annotation in self.myMap.annotations)
            {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.0, 0.0);
                zoomRect = MKMapRectUnion(zoomRect, pointRect);
            }
            if (iPhoneVersion == 5) {
                [self.myMap setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(70, 30, 30, 30) animated:YES];
            }
            else if (iPhoneVersion == 10)
            {
                [self.myMap setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(70, 30, 30, 30) animated:YES];
            }
            else
            {
                [self.myMap setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(100, 30, 30, 30) animated:YES];
            }
            
        }
    }
    else
    {
        _view_loading.hidden = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:[[resultDic objectAtIndex:0] valueForKey:@"error"]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
    }
}

#pragma mark - MKMapView Delegate Method
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    static NSString *identifier = @"MyAnnotation";
    MyAnnotationView * annotationView = (MyAnnotationView *)[_myMap dequeueReusableAnnotationViewWithIdentifier:identifier];
    
  
    annotationView = [[MyAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"mapPin.png"]];
    
    UIImage *screw = [UIImage imageWithData:UIImagePNGRepresentation(img) scale:scale];
    
    annotationView.image = screw;
    
    annotationView.centerOffset = CGPointMake(0, -annotationView.frame.size.height/2);
   
  
    return annotationView;
}



-(void)showDetailView:(id)sender{
    NSLog(@"inside the stupid method");
    
}
// View Custom Anntation [Display View ]
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    callOutView *calloutview = (callOutView *)[view viewWithTag:999];
    
    if(!calloutview){
        
        if (iPhoneVersion == 5)
        {
            calloutview = [[[NSBundle mainBundle] loadNibNamed:@"callOutView" owner:self options:nil] objectAtIndex:0];
        }
        else if (iPhoneVersion == 10)
        {
            calloutview = [[[NSBundle mainBundle] loadNibNamed:@"callOutView_X" owner:self options:nil] objectAtIndex:0];
        }
        else
        {
            calloutview = [[[NSBundle mainBundle] loadNibNamed:@"callOutView_iPad" owner:self options:nil] objectAtIndex:0];
        }
        
        calloutview.tag = 999;
        
        
        CGRect calloutViewFrame = calloutview.frame;
        calloutViewFrame.origin = CGPointMake(-calloutViewFrame.size.width/2 + 25, -calloutViewFrame.size.height);
        calloutview.frame = calloutViewFrame;
        
        MyAnnotation *cpa = view.annotation;
        
       calloutview.imgRest.layer.cornerRadius = 5.0f;
        calloutview.imgRest.clipsToBounds = YES;
        [calloutview.lbl_HotelName setText:cpa.title];
        [calloutview.lbl_Minute setText:cpa.min];
        [calloutview.lblRatting setText:cpa.rat];
         [calloutview.lbl_HotelCate setText:cpa.cate];
        strLatitude = cpa.restLat;
        strLatitude=cpa.restlong;
        strDis=cpa.restDistance;
        strRestId =cpa.restID;
        if ([cpa.openornot isEqualToString:@"open"])
        {
         
            [calloutview.lbl_OpenOrNot setText:[NSString stringWithFormat:@"OPEN : Open till %@",cpa.restClose]];
            calloutview.lbl_OpenOrNot.textColor = [Color colorFromHexString:@"7CB66A"];
        }
        else
        {
            [calloutview.lbl_OpenOrNot setTextColor:[UIColor redColor]];
            
            [calloutview.lbl_OpenOrNot setText:[NSString stringWithFormat:@"CLOSED : Open at %@",cpa.restOpen]];
        }
        
        UIImage *store_image = [[UIImage alloc] initWithData:[cpa.imageDic objectForKey:[NSString stringWithFormat:@"%@",cpa.title]]];
        
        [calloutview.imgRest setImage:store_image];
        
        UIButton *btnDelete = (UIButton *)[calloutview viewWithTag:200];
        [btnDelete setTitle:cpa.restID forState:UIControlStateNormal];
        [btnDelete setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [btnDelete addTarget:self action:@selector(DeleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:calloutview];
        [UIView animateWithDuration:.5 animations:^(void)
         {
             self.myMap.centerCoordinate = cpa.coordinate;
         }];
    }

}


-(void)DeleteButtonTapped:(UIButton *)sender
{
    Detail *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    NSLog(@"IdRest %@",strRestId);
    detail.restaurantID = strRestId;
    detail.lat =strLatitude;
    detail.lon =strlongitude;
    detail.distance=strDis;
    
    detail.strScreen= @"map";
    
    [self.navigationController pushViewController:detail animated:YES];

}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    callOutView *calloutview = (callOutView *)[view viewWithTag:999];
    [calloutview removeFromSuperview];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    
    if (!_isUserLocation)// || _isNewLocation)
    {
        _isUserLocation = YES;
        double miles = 8;
        double scalingFactor = ABS( (cos(2 * M_PI * userLocation.coordinate.latitude / 360.0) ));
        
        MKCoordinateSpan span;
        
        span.latitudeDelta = miles/69.0;
        span.longitudeDelta = miles/(scalingFactor * 69.0);
        
        MKCoordinateRegion region;
        region.span = span;
        region.center = userLocation.coordinate;
        
        [mapView setRegion:region animated:YES];
    }
    else
    {
        NSLog(@"Called");
    }
    
    
}
#pragma mark - Reachability
//Checks internet connectivity and returns BOOL value
- (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Retirve Data From Webservice
-(void)GetData:(NSString *)url
{
    if(![self isConnected])
    {
        
        Snackbar *snackbar = [[Snackbar alloc] initWithTitle:@"Failed to retrive data from server. Please try again after some time." duration:3.0];
        [self.view addSubview:snackbar];
    }
    else
    {
        NSLog(@"URL :: %@",url);
        NSString *StringURL = [NSString stringWithFormat:@"%@%@",SERVERURL, url];
        StringURL = [StringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSMutableURLRequest *request = nil;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:StringURL]];
        
        _connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [_connection1 start];
    }
}
#pragma mark - AlertView
-(void)showAlertView:(NSString *)title message:(NSString *)message cancel:(NSString *)cancelTitle
{
    SCLAlertView *alert = [[SCLAlertView alloc]initWithNewWindow];
    [alert showCustom:self image:nil color:[UIColor darkGrayColor] title:title subTitle:message closeButtonTitle:cancelTitle duration:0.0];
}
#pragma mark - Image Loader
- (void) loadImageFromURL:(NSString*)URL imgview:(UIImageView *)imageView
{
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data)
    {
        UIImage *image = [UIImage imageWithData:data];
        imageView.image = image;
    }
    else
    {
        imageView.image = [UIImage imageNamed:@"categories_load.png"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                imageView.image = image;
            });
        });
    }
}

@end
