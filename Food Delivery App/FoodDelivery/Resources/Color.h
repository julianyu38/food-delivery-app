//
//  Color.h
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/02/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Color : UIView
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end
