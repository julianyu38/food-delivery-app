 //
//  SQLFile.m
//  MEDICINES
//
//  Created by R on 11/06/2015.
//  Copyright (c) 2015 Redixbit. All rights reserved.
//

#import "SQLFile.h"
#import "AppDelegate.h"


@implementation SQLFile
{
    AppDelegate *appd;
}

-(id)init
{
    if (self=[super init])
    {
        appd = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        strdbname  =[[NSString alloc]initWithString:appd.strdbpath];
    }
    return self;
}

#pragma mark - Insert and Delete
-(BOOL)operationdb:(NSString *)str
{
    BOOL y=NO;
    
    if (sqlite3_open([strdbname UTF8String], &database)==SQLITE_OK)
    {
        NSString *strq =[NSString stringWithString:str];
        sqlite3_stmt *cstmt;
        NSLog(@"%@",strdbname);
        if (sqlite3_prepare_v2(database, [strq UTF8String], -1, &cstmt, NULL)==SQLITE_OK)
        {
            sqlite3_step(cstmt);
            y=YES;
            long long lastRowId1 = sqlite3_last_insert_rowid(database);
            NSString *rowId = [NSString stringWithFormat:@"%d", (int)lastRowId1];
            NSLog(@"%@",rowId);
        }
        sqlite3_finalize(cstmt);
        sqlite3_close(database);
    }
    return y;
}

#pragma mark - Update

-(BOOL)updatedb:(NSString *)strupdate
{
    BOOL y=NO;
    if (sqlite3_open([strdbname UTF8String], &database) == SQLITE_OK)
    {
        NSString *strq =[NSString stringWithString:strupdate];
        
        sqlite3_stmt *cstmt;
        // sqlite3_bind_int(cstmt, 1,[NSString stringWithFormat:@"%d",med_id]);
        if (sqlite3_prepare_v2(database, [strq UTF8String], -1, &cstmt, NULL)==SQLITE_OK)
        {
            sqlite3_step(cstmt);
            y=YES;
        }
        sqlite3_finalize(cstmt);
        sqlite3_close(database);
    }
    return y;
}

#pragma mark - Select
-(NSMutableArray *)select_favou:(NSString *)strqr
{
    NSMutableArray *marr = [[NSMutableArray alloc]init];

    if (sqlite3_open([strdbname UTF8String], &database)==SQLITE_OK)
    {
        NSString *userstr =[NSString stringWithString:strqr];
        sqlite3_stmt *cstmt;
        if(sqlite3_prepare_v2(database, [userstr UTF8String], -1, &cstmt, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(cstmt)==SQLITE_ROW)
            {
                NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
                
                NSString *n0=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,0)];
                [dic setValue:n0 forKey:@"restId"];
                
                NSString *n1=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,1)];
                [dic setValue:n1 forKey:@"restName"];
                
                NSString *n2=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,2)];
                [dic setValue:n2 forKey:@"restAddress"];
                
                NSString *n3=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,3)];
                [dic setValue:n3 forKey:@"restLat"];
                
                NSString *n4=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,4)];
                [dic setValue:n4 forKey:@"restLong"];
                
                NSString *n5=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,5)];
                [dic setValue:n5 forKey:@"restDetails"];

                [marr addObject:dic];
            }
        }
        sqlite3_finalize(cstmt);
        sqlite3_close(database);
    }
    return marr;
}

-(NSMutableArray *)select_codes:(NSString *)strqr
{
    NSMutableArray *marr = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([strdbname UTF8String], &database)==SQLITE_OK)
    {
        
        NSString *userstr =[NSString stringWithString:strqr];
        sqlite3_stmt *cstmt;
        if(sqlite3_prepare_v2(database, [userstr UTF8String], -1, &cstmt, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(cstmt)==SQLITE_ROW)
            {
                NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
                
                NSString *n0=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,0)];
                [dic setValue:n0 forKey:@"id"];
                
                NSString *n1=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,1)];
                [dic setValue:n1 forKey:@"iso"];
                
                NSString *n2=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,2)];
                [dic setValue:n2 forKey:@"name"];
                
                NSString *n3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 3)];
                [dic setValue:n3 forKey:@"nicename"];
                
                NSString *n4 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 4)];
                [dic setValue:n4 forKey:@"iso3"];
                
                NSString *n5 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 5)];
                [dic setValue:n5 forKey:@"numcode"];
                
                NSString *n6 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 6)];
                [dic setValue:n6 forKey:@"phonecode"];
                
                [marr addObject:dic];
            }
        }
        sqlite3_finalize(cstmt);
        sqlite3_close(database);
    }
    return marr;
}

-(NSMutableArray *)select_cart:(NSString *)strqr
{
    NSMutableArray *marr = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([strdbname UTF8String], &database)==SQLITE_OK)
    {
        
        NSString *userstr =[NSString stringWithString:strqr];
        sqlite3_stmt *cstmt;
        if(sqlite3_prepare_v2(database, [userstr UTF8String], -1, &cstmt, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(cstmt)==SQLITE_ROW)
            {
                NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
                
                NSString *n0=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,0)];
                [dic setValue:n0 forKey:@"restId"];
                
                NSString *n1=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,1)];
                [dic setValue:n1 forKey:@"menuId"];
                
                NSString *n2=[NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt,2)];
                [dic setValue:n2 forKey:@"foodId"];
                
                NSString *n3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 3)];
                [dic setValue:n3 forKey:@"foodName"];
                
                NSString *n4 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 4)];
                [dic setValue:n4 forKey:@"foodPrice"];
                
                NSString *n5 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 5)];
                [dic setValue:n5 forKey:@"foodQty"];
                
                NSString *n6 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 6)];
                [dic setValue:n6 forKey:@"foodDesc"];
                
                NSString *n7 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cstmt, 7)];
                [dic setValue:n7 forKey:@"foodCurrency"];
                
                [marr addObject:dic];
            }
        }
        sqlite3_finalize(cstmt);
        sqlite3_close(database);
    }
    return marr;
}
                
@end
