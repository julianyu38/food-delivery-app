//
//  SQLFile.h
//  MEDICINES
//
//  Created by R on 11/06/2015.
//  Copyright (c) 2015 Redixbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "AppDelegate.h"


@interface SQLFile : NSObject
{
    sqlite3 *database;
    NSString *strdbname;
}

-(BOOL)operationdb:(NSString *)str;
-(BOOL)updatedb:(NSString *)strupdate;
-(NSMutableArray *)select_favou:(NSString *)strqr;
-(NSMutableArray *)select_cart:(NSString *)strqr;
-(NSMutableArray *)select_codes:(NSString *)strqr;
@end
