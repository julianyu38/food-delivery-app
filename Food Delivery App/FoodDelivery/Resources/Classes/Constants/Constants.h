//
//  Header.h
//  FoodDelivery
//
//  Created by Redixbit on 29/09/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#ifndef FoodDelivery_Header_h
#define FoodDelivery_Header_h

#define DEFAULTS_KEY_LANGUAGE_CODE @"Language"
#define CustomLocalisedString(key, comment) \
[[LanguageManager sharedLanguageManager] getTranslationForKey:key]

#define country @"ar"
#define character 2
#define dollar @"$"

//Google API Key for autocomplete address(places)

#define API_KEY @"AIzaSyDdeDfXhSxQ0S3CYtvyt1oC5ZwgEUfndmE"

//Google SignIn

#define GClientID @"80227019868-mjgpdfua1k6ot2vgqvg8hu4q1baip6n4.apps.googleusercontent.com"

//Server

#define SERVERURL @"http://192.168.1.116/freak/FoodDeliverySystem/api/"
#define IMAGEURL @"http://192.168.1.116/freak/FoodDeliverySystem/uploads/restaurant/"

#define listRow 7

//Iphone Model
#define iPhoneVersion ([[UIScreen mainScreen] bounds].size.height == 568 ? 5 : ([[UIScreen mainScreen] bounds].size.height == 480 ? 4 : ([[UIScreen mainScreen] bounds].size.height == 667 ? 6 : ([[UIScreen mainScreen] bounds].size.height == 736 ? 61 : ([[UIScreen mainScreen] bounds].size.height == 812 ? 10 : 999)))))

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

#define NUMBERS @"0123456789"
#define ALPHABETS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopwrstuvwxyz"

#define transitionDuration 0.5f


#define APP_URL @"https://itunes.apple.com/in/app/itunes-u/id490217893?mt=8"//"https://itunes.apple.com/in/app/XXXXXXX/idXXXXXXXXX?mt=8"

#define APP_NAME @"Food Delivery"

// FONT FAMILY

//OderStatus page Color
#define Color_light_grey  [UIColor colorWithRed:0.53 green:0.53 blue:0.53 alpha:1.0]
#define pickedcolor       [UIColor colorWithRed:0.95 green:0.38 blue:0.11 alpha:1.0]
#define greaySetting        [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0]

#define RFontBoldItalic     @"OpenSans-BoldItalic"
#define RFontSemibold       @"OpenSans-Semibold"
#define RFontRegular        @"OpenSans"


#endif
