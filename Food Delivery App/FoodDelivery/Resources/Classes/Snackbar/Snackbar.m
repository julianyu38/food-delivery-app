//
//  Snackbar.m
//  FoodDelivery
//
//  Created by Redixbit on 11/11/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import "Snackbar.h"
#import "Constants.h"
@implementation Snackbar
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithTitle:(NSString *)title duration:(CGFloat)duration
{
    
    self = [super initWithFrame:CGRectMake(0, -60, [UIScreen mainScreen].bounds.size.width, 60)];
    if (self)
    {
        self.backgroundColor = [UIColor orangeColor];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, self.frame.size.height)];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.text = title;
        [label setFont:[UIFont fontWithName:@"WorkSans-Regular" size:12.0f]];
//        [label setFont:[UIFont boldSystemFontOfSize:12.0]];
        [self addSubview:label];
    }
    [self showSnackbar];
    [self performSelector:@selector(dismissSnackbar) withObject:nil afterDelay:duration];
    return self;
}

-(void)showSnackbar
{
  
   if (iPhoneVersion==10)
    {
        [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.frame = CGRectMake(self.frame.origin.x, 34, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished){}];
    }
    else
    {
        [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.frame = CGRectMake(self.frame.origin.x, 0, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished){}];
    }
    
}

-(void)dismissSnackbar
{
    [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.frame = CGRectMake(self.frame.origin.x, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}
@end
