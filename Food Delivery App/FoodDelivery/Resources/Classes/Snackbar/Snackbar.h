//
//  Snackbar.h
//  FoodDelivery
//
//  Created by Redixbit on 11/11/16.
//  Copyright (c) 2016 ExpressTemplate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Snackbar : UIView

-(id)initWithTitle:(NSString *)title duration:(CGFloat)duration;
-(void)showSnackbar;
-(void)dismissSnackbar;

@end
