//
//  Color.m
//  FoodDelivery
//
//  Created by Kiran Padhiyar on 27/02/18.
//  Copyright © 2018 ExpressTemplate. All rights reserved.
//

#import "Color.h"

@implementation Color
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    // declare an unsigned int rgbValue, scan the hexString, convert it to an int, create the RGB colors values, and return a UIColor
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
